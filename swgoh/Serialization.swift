//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

/// top level structure of the guild file, e.g. output of https://swgoh.gg/api/guild/46657/
struct GuildFile : Decodable {
	let guild: GuildInfo
	var players: [Player]
	
	enum CodingKeys: String, CodingKey {
		case guild = "data"
		case players
	}
		
	func playersSortedByGP() -> [Player] {
		return players.sorted(by: { (p1, p2) -> Bool in
			return p1.stats.gp < p2.stats.gp
		})
	}
		
	func allUnits() -> [String:Unit] {
		var units = [String:Unit]()
		for player in players {
			units.merge(player.units, uniquingKeysWith: { (u1, u2) -> Unit in
				return u1.power > u2.power ? u1 : u2
			})
		}
		return units
	}
	
	func unitProviderFor(player: Player) -> UnitProvider {
		
		struct PlayerUnitsAndFallback : UnitProvider {
			
			let playerUnits: [String:Unit]
			let fallback: [String:Unit]
			
			func lookup(id: String) -> Unit? {
				if let unit = playerUnits[id] {
					return unit
				}
				if let unit = fallback[id] {
					return unit.erase()
				}
				return nil
			}

			subscript(id: String) -> Unit? {
				if let result = lookup(id: id) {
					return result
				}
				
				// didn't match, try it as a name
				if let id2 = NameCache.get(id) {
					if let result = lookup(id: id2) {
						return result
					}
					return nil
				}
				
				// try and resolve a new name
				let character = Character.find(id)
				return lookup(id: character.id)
			}
		}

		return PlayerUnitsAndFallback(playerUnits: player.units, fallback: allUnits())
	}
}

struct GuildInfo : Decodable {
	let gp: Int
	let id: Int
	let memberCount: Int
	let name: String
	let profileCount: Int
	let rank: Int
	
	enum CodingKeys: String, CodingKey {
		case gp = "galactic_power"
		case id
		case memberCount = "member_count"
		case name
		case profileCount = "profile_count"
		case rank
	}
}

struct Unit : Codable, Hashable {
	
	struct Ability : Codable {
		let tier: Int
		let maxTier: Int
		let id: String
		let name: String
		let isOmega: Bool
		let isZeta: Bool
		let hasOmegaZeta: Bool
		
		enum CodingKeys: String, CodingKey {
			case tier = "ability_tier"
			case maxTier = "tier_max"
			case id
			case name
			case isOmega = "is_omega"
			case isZeta = "is_zeta"
		}
		
		init(from decoder: Decoder) throws {
			let values = try decoder.container(keyedBy: CodingKeys.self)
			
			let tier = try values.decode(Int.self, forKey: .tier)
			let maxTier = try values.decode(Int.self, forKey: .maxTier)
			id = try values.decode(String.self, forKey: .id)
			name = try values.decode(String.self, forKey: .name)
			let isZeta = try values.decode(Bool.self, forKey: .isZeta)
			let isOmega = try values.decode(Bool.self, forKey: .isOmega)
			
			self.tier = tier
			self.maxTier = maxTier
			self.isZeta = isZeta
			self.isOmega = isOmega
			hasOmegaZeta = (isZeta || isOmega) && tier == maxTier
		}
		
		init(id: String, name: String, maxTier: Int, isOmega: Bool, isZeta: Bool, hasOmegaZeta: Bool = false) {
			self.tier = 0
			self.maxTier = maxTier
			self.id = id
			self.name = name
			self.isOmega = isOmega
			self.isZeta = isZeta
			self.hasOmegaZeta = hasOmegaZeta
		}
		
		func erase() -> Ability {
			return Ability(id: id, name: name, maxTier: maxTier, isOmega: isOmega, isZeta: isZeta)
		}
		
		func structure() -> String {
			return "\(id): \(name)\(isOmega ? " (omega) " : "")\(isZeta ? " (zeta) " : "")[\(maxTier)]"
		}
	}
	
	struct Stats : Codable {
		
		let health: Int
		let protection: Int
		let speed: Int
		let physicalDamage: Int
		let specialDamage: Int
		let armor: Int
		let resistance: Int
		
		let armorPenetration: Int
		
		let physicalCritChance: Int
		let specialCritChance: Int
		let criticalDamage: Int
		let potency: Int
		let tenacity: Int
		
		enum CodingKeys: String, CodingKey {
			case health = "1"
			case protection = "28"
			case speed = "5"
			case physicalDamage = "6"
			case specialDamage = "7"
			case armor = "8"
			case resistance = "9"
			case armorPenetration = "10"
			case physicalCritChance = "14"
			case specialCritChance = "15"
			case criticalDamage = "16"
			case potency = "17"
			case tenacity = "18"
			
			var name: String {
				switch self {
				case .health: return "health"
				case .protection: return "protection"
				case .speed: return "speed"
				case .physicalDamage: return "physical damage"
				case .specialDamage: return "special damage"
				case .armor: return "armor"
				case .resistance: return "resistance"
				case .armorPenetration: return "armor penetration"
				case .physicalCritChance: return "physical crit chance"
				case .specialCritChance: return "special crit chance"
				case .criticalDamage: return "critical damage"
				case .potency: return "potency"
				case .tenacity: return "tenacity"
				}
			}
		}
		
		init(from decoder: Decoder) throws {
			let values = try decoder.container(keyedBy: CodingKeys.self)
			
			health = try values.decode(Int.self, forKey: .health)
			protection = try values.decode(Int.self, forKey: .protection)
			speed = try values.decode(Int.self, forKey: .speed)
			physicalDamage = try values.decode(Int.self, forKey: .physicalDamage)
			specialDamage = try values.decode(Int.self, forKey: .specialDamage)
			armor = Int(try values.decode(Double.self, forKey: .armor))
			resistance = Int(try values.decode(Double.self, forKey: .resistance))
			armorPenetration = try values.decode(Int.self, forKey: .armorPenetration)
			physicalCritChance = Int(try values.decode(Double.self, forKey: .physicalCritChance) * 100.0)
			specialCritChance = Int(try values.decode(Double.self, forKey: .specialCritChance) * 100.0)
			criticalDamage = Int(try values.decode(Double.self, forKey: .criticalDamage) * 100.0)
			potency = Int(try values.decode(Double.self, forKey: .potency) * 100.0)
			tenacity = Int(try values.decode(Double.self, forKey: .tenacity) * 100.0)
		}
		
		init() {
			health = 0
			protection = 0
			speed = 0
			physicalDamage = 0
			specialDamage = 0
			armor = 0
			resistance = 0
			armorPenetration = 0
			physicalCritChance = 0
			specialCritChance = 0
			criticalDamage = 0
			potency = 0
			tenacity = 0
		}
		
		func value(_ key: CodingKeys) -> Int {
			switch key {
			case .health: return health
			case .protection: return protection
			case .speed: return speed
			case .physicalDamage: return physicalDamage
			case .specialDamage: return specialDamage
			case .armor: return armor
			case .resistance: return resistance
			case .armorPenetration: return armorPenetration
			case .physicalCritChance: return physicalCritChance
			case .specialCritChance: return specialCritChance
			case .criticalDamage: return criticalDamage
			case .potency: return potency
			case .tenacity: return tenacity
			}
		}
	}
	
	let abilities: [Ability]
	let gearLevel: Int
	let level: Int
	let name: String
	let id: String
	let power: Int
	let stats: Stats
	let zetas: [String]
	let stars: Int
	
	let hasStats: Bool
	let hasOmegas: Bool
	
	enum CodingKeys: String, CodingKey {
		case abilities = "ability_data"
		case gearLevel = "gear_level"
		case level
		case name
		case id = "base_id"
		case power
		case stats
		case zetas = "zeta_abilities"
		case stars = "rarity"
		
		var name: String {
			switch self {
			case .abilities: return "abilities"
			case .gearLevel: return "gear level"
			case .level: return "level"
			case .name: return "name"
			case .id: return "id"
			case .power: return "power"
			case .stats: return "stats"
			case .zetas: return "zetas"
			case .stars: return "stars"
			}
		}
	}
	
	enum NestedCodingKeys: String, CodingKey {
		case data
	}
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: NestedCodingKeys.self)
		let playerValues = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
		
		abilities = try playerValues.decode([Ability].self, forKey: .abilities)
		gearLevel = try playerValues.decode(Int.self, forKey: .gearLevel)
		level = try playerValues.decode(Int.self, forKey: .level)
		name = try playerValues.decode(String.self, forKey: .name)
		id = try playerValues.decode(String.self, forKey: .id)
		power = try playerValues.decode(Int.self, forKey: .power)
		stats = try playerValues.decode(Stats.self, forKey: .stats)
		zetas = try playerValues.decode([String].self, forKey: .zetas)
		stars = try playerValues.decode(Int.self, forKey: .stars)
		
		hasStats = true
		hasOmegas = true
	}
	
	init(id: String, name: String, abilities: [Ability]) {
		self.id = id
		self.abilities = abilities
		gearLevel = 0
		level = 0
		self.name = name
		power = 0
		stats = Stats()
		zetas = []
		stars = 0
		
		hasStats = false
		hasOmegas = false
	}
	
	init(id: String, name: String, gearLevel: Int, level: Int, power: Int, stars: Int, abilities: [Ability]) {
		self.id = id
		self.abilities = abilities
		self.gearLevel = gearLevel
		self.level = level
		self.name = name
		self.power = power
		stats = Stats()
		zetas = abilities.filter { $0.isZeta }.map { $0.name }
		self.stars = stars
		
		hasStats = false
		hasOmegas = false
	}
	
	func encode(to encoder: Encoder) throws {
		var outerContainer = encoder.container(keyedBy: NestedCodingKeys.self)
		var container = outerContainer.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
		
		try container.encode(abilities, forKey: .abilities)
		try container.encode(gearLevel, forKey: .gearLevel)
		try container.encode(level, forKey: .level)
		try container.encode(name, forKey: .name)
		try container.encode(id, forKey: .id)
		try container.encode(power, forKey: .power)
		try container.encode(stats, forKey: .stats)
		try container.encode(zetas, forKey: .zetas)
		try container.encode(stars, forKey: .stars)
	}
	
	func erase() -> Unit {
		return Unit(id: id, name: name, abilities: abilities.map { $0.erase() })
	}
	
	func value(_ key: CodingKeys) -> Int {
		switch key {
		case .gearLevel: return gearLevel
		case .level: return level
		case .power: return power
		case .stars: return stars
		default: fatalError("not an Int property")
		}
	}
	
	var commonName: String {
		return commonNames[name] ?? name
	}
	
	static func == (lhs: Unit, rhs: Unit) -> Bool {
		return lhs.id == rhs.id
	}
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(id)
	}
	
	func structure() -> String {
		var result = "\(id): \(commonName)\n"
		for ability in abilities {
			result.append("\t")
			result.append(ability.structure())
			result.append("\n")
		}
		return result
	}
}

struct Player : CustomStringConvertible, Codable, Hashable, Comparable {

	struct Stats : Codable {
		let allyCode: Int
		let name: String
		let gp: Int
		let characterGP: Int
		let shipGP: Int
		
		enum CodingKeys: String, CodingKey {
			case allyCode = "ally_code"
			case name
			case gp = "galactic_power"
			case characterGP = "character_galactic_power"
			case shipGP = "ship_galactic_power"
		}
	}

	let stats: Stats
	let units: [String:Unit]
	
	enum CodingKeys: String, CodingKey {
		case stats = "data"
		case units
	}
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		
		stats = try values.decode(Stats.self, forKey: .stats)
		
		let units = try values.decode([Unit].self, forKey: .units)
		self.units = Dictionary(uniqueKeysWithValues: units.map({ ($0.id, $0 )}))
	}
	
	init(name: String, allyCode: Int, gp: Int, units: [String:Unit]) {
		self.units = units
		self.stats = Stats(allyCode: allyCode, name: name, gp: gp, characterGP: gp, shipGP: gp)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(stats, forKey: .stats)
		try container.encode(Array(units.values), forKey: .units)
	}
	
	var mention: String? {
		let name = stats.name
		if let mention = mentions[name] {
			return "@" + mention
		} else {
			return nil
		}
	}
	
	var description: String {
		return self.stats.name
	}
	
	static func == (lhs: Player, rhs: Player) -> Bool {
		return lhs.stats.name == rhs.stats.name
	}
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(stats.name)
	}

	static func < (lhs: Player, rhs: Player) -> Bool {
		return lhs.stats.name.lowercased() < rhs.stats.name.lowercased()
	}

}

// MARK: - Characters File -- https://swgoh.gg/api/characters/

struct Character : Decodable {
    
    let abilityClasses: [String]
    let alignment: String
    let id: String
    let categories: [String]
    let name: String
    let role: String
	let ship: Bool
    
    enum CodingKeys: String, CodingKey {
        case abilityClasses = "ability_classes"
        case alignment
        case id = "base_id"
        case categories
        case name
        case role
    }
	
	init(_ id: String, _ name: String, role: String, abilityClasses: [String], categories: [String], ship: Bool = false) {
		self.abilityClasses = abilityClasses
		self.alignment = "Unspecified"
		self.id = id
		self.categories = categories
		self.name = name
		self.role = role
		self.ship = ship
	}
	
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		
		abilityClasses = try values.decode([String].self, forKey: .abilityClasses)
		alignment = try values.decode(String.self, forKey: .alignment)
		id = try values.decode(String.self, forKey: .id)
		categories = try values.decode([String].self, forKey: .categories)
		name = try values.decode(String.self, forKey: .name)
		role = try values.decode(String.self, forKey: .role)

		ship = false
	}
	
	static func find(_ name: String) -> Character {
		
		if let character = characters[name] {
			return character
		}
		
		if let id = NameCache.get(name) {
			return characters[id]!
		}
		
		var resolvedId: String? = nil
		let lowercaseName = name.lowercased()

		// exact match first
		for character in characters.values {
			if character.name.lowercased() == lowercaseName || character.commonName.lowercased() == lowercaseName {
				if let resolvedId = resolvedId { fatalError("duplicate unit for \(name): \(resolvedId) and \(character.id)")}
				resolvedId = character.id
			}
		}
		
		if resolvedId == nil {
			// fall back to substring match
			for character in characters.values {
				if character.name.lowercased().contains(lowercaseName) || character.commonName.lowercased().contains(lowercaseName) {
					if let resolvedId = resolvedId { fatalError("duplicate unit for \(name): \(resolvedId) and \(character.id)")}
					resolvedId = character.id
				}
			}
		}

		if let resolvedId = resolvedId {
			NameCache.set(name, resolvedId)
			return characters[resolvedId]!
		}
			
		fatalError("unable to look up \(name)")
	}

    func matches(_ match: String) -> Bool {
		if match == "all" {
			return true
		}
        let match = match.lowercased()
        if name.lowercased().contains(match) || role.lowercased().contains(match) || alignment.lowercased().contains(match) {
            return true
        }
        if abilityClasses.contains(where: { $0.lowercased().contains(match) }) {
            return true
        }
        if categories.contains(where: { $0.lowercased().contains(match) }) {
            return true
        }
        return false
    }
	
	var commonName: String {
		return commonNames[name] ?? name
	}
    
    func description(unit: Unit, display: String) -> String {
        switch display {
		case "id":
			return id
			
		case "id,":
			return "\"\(id)\", "
			
        default:
            return name
        }
    }

}

