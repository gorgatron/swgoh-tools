//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

//let jsonData = try Data(contentsOf: URL(fileURLWithPath: "/tmp/players.json"))
//print(try! JSONSerialization.jsonObject(with: jsonData, options: []))
//exit(0)

let t = NSDate.timeIntervalSinceReferenceDate

func usage(_ argument: String?) -> Never {
	let message = argument == nil ? "" : "Unknown argument: \(argument!)\n\n"
	print(
		"""
		\(message)USAGE: swgoh <arguments>
		
		--guild <guild.json>
		
		--character <match>
		
		--allPlayers
		--player <player name,...>
		--skip-player <player name,...>
		--hasUnit <unit id>
		--listUnits
		--listPlayers
		--dumpUnit <unit id or name>
		
		--snapshot <snapshot.json>
		--save-snapshot <snapshot.json>

		--filter-tw
		--filter-viable
		--filter-only-nonviable -- list players who have teams that might work but are not viable
		--filter-preferred
		--filter-farm
		--filter-squad-list

		--report-debug
		--report-assign
		--report-farm
		--report-farm-csv
		--report-farm-critical
		--report-tw-planning
		--report-tw-teams
		--report-tw-players
		--report-tw-compare
		
		--report-tw-simulate <opponent.snapshot.json> <sections>
		
		--hstr-schedule

		--jtr
		
		--p2 <type>
		
		--chexmix
		--p3-mix
		--p3-ns
		
		--p4-ns
		--p4-fo
		
		--tw [FO]
		--ga-3v3
		""")
	exit(1)
}

var guildURL: URL?
var charactersURL: URL?

var allPlayers = false

var playerSelector: PlayerSelect = PlayerSelectAll()
var squadBuilder: SquadBuilder = SelectUnitSquadBuilder(unitSelectors: [])
var squadEvaluator = SquadEvaluator(rules: [:])
var evaluationFilter: Filter = PreferredViabilitySortFilter()
var report: Report = DebugReport()

func load(_ url:URL, _ guildName: String? = nil) throws -> GuildFile {
	let jsonDecoder = JSONDecoder()
	
	guard let jsonData = try? Data(contentsOf: url) else {
		fatalError("Unable to open \(url)")
	}
	
	do {
		return try jsonDecoder.decode(GuildFile.self, from: jsonData)
	} catch {
		let outerError = error
		do {
			return try SWGOHHelp.load(jsonData, guildName: guildName)
		} catch {
			throw outerError
		}
	}
}

func evaluatePlayers(players: [Player], guildFile: GuildFile, squadBuilder: SquadBuilder = squadBuilder, squadEvaluator: SquadEvaluator = squadEvaluator) -> [Player:[SquadEvaluation]] {
	var result = [Player:[SquadEvaluation]]()
	
	let queue = DispatchQueue(label: "build")
	func update(player: Player, evaluations: [SquadEvaluation]) {
		queue.sync {
			result[player] = evaluations
		}
	}
	
	DispatchQueue.concurrentPerform(iterations: players.count) {
		let player = players[$0]
		
		let squads = squadBuilder.generateSquads(units: guildFile.unitProviderFor(player: player))
		
		let evaluations = squadEvaluator.evaluate(squads: squads)
		let finalEvaluations = evaluationFilter.filter(evaluations)
		
		if !finalEvaluations.isEmpty || allPlayers {
			update(player: player, evaluations: finalEvaluations)
		}
	}
	
	return result
}

var prepare = { (players: [Player], guildFile: GuildFile) throws -> [Player:[SquadEvaluation]] in
	return evaluatePlayers(players: players, guildFile: guildFile)
}

var run = { (guildFile: GuildFile ) throws in print("no action selected") }

func squadRun() -> (GuildFile) throws -> Void {
    return { (guildFile: GuildFile) throws in
		let players = guildFile.players.filter { playerSelector.matches(player: $0) }
		
		let result = try prepare(players, guildFile)
		
		print(report.report(data: result))
	}
}

var arguments = CommandLine.arguments.dropFirst()
while !arguments.isEmpty {
	let argument = arguments.removeFirst()
	
	switch argument {
    case "--guild":
        guildURL = URL(fileURLWithPath: arguments.removeFirst())
        
	case "--allPlayers":
		allPlayers = true
		
	case "--player":
		let names = arguments.removeFirst().split(separator: ",").map { String($0) }
		playerSelector = PlayersSelectMatch(names)
		
	case "--skip-player":
		let names = arguments.removeFirst().split(separator: ",").map { String($0) }
		playerSelector = PlayersSelectMatch(names, invert: true)
		
	case "--hasUnit":
		playerSelector = PlayerSelectCharacter(arguments.removeFirst())
		
	case "--listUnits":
		run = { guildFile in
			let units = guildFile.allUnits()
			var namedUnits = [String:String]()
			for (id, unit) in units {
				namedUnits[unit.name] = id
			}
			for name in namedUnits.keys.sorted() {
				print("\(namedUnits[name]!) : \(name)")
			}
		}
		
	case "--dumpUnit":
		let match = arguments.removeFirst().lowercased()
		run = { guildFile in
			let units = guildFile.allUnits()
			for unit in units.values {
				if unit.id.lowercased().contains(match) || unit.name.lowercased().contains(match) {
					print(unit.structure())
					print("")
				}
			}
		}
		
	case "--geonosianTB":
		allPlayers = true
		report = GeonosianTBReport()
		run = { guildFile in
			var evaluations = [Player:[SquadEvaluation]]()
			for player in guildFile.players {
				evaluations[player] = []
			}
			print(report.report(data: evaluations))
		}

	case "--snapshot":
		let inputURL = URL(fileURLWithPath: arguments.removeFirst())
		prepare = { (players: [Player], guildFile: GuildFile) throws -> [Player:[SquadEvaluation]] in
			let jsonDecoder = JSONDecoder()
			let jsonData = try Data(contentsOf: inputURL)
			
			var results = try jsonDecoder.decode([Player:[SquadEvaluation]].self, from: jsonData)
			
			let removePlayers = Set(players).subtracting(results.keys)
			for player in removePlayers {
				results.removeValue(forKey: player)
			}
			
			return results
		}
		
		// use the normal runner by default
		run = squadRun()
		
	case "--save-snapshot":
		let outputURL = URL(fileURLWithPath: arguments.removeFirst())
		run = { (guildFile: GuildFile) throws in
			let players = guildFile.players.filter { playerSelector.matches(player: $0) }
			let evaluations = try prepare(players, guildFile)
			
			let jsonEncoder = JSONEncoder()
			let jsonData = try jsonEncoder.encode(evaluations)
			
			try jsonData.write(to: outputURL)
		}
		
	case "--report-tw-simulate":
		// we filter the defensive teams

		let opponentSnapshotURL = URL(fileURLWithPath: arguments.removeFirst())
		let sections = arguments.removeFirst().split(separator: ",").map { String($0) }
		run = { (guildFile: GuildFile) throws in
			let players = guildFile.players.filter { playerSelector.matches(player: $0) }
			var evaluations = try prepare(players, guildFile)
			
			// apply filtering to the defense teams
			let filter = PreferredViabilitySortFilter().compose(TWSquadFilter()).compose(ViableSquadFilter()).compose(SquadListFilter())
			for (player, ev) in evaluations {
				let newEv = filter.filter(ev)
				evaluations[player] = newEv
			}

			let jsonDecoder = JSONDecoder()
			let jsonData = try Data(contentsOf: opponentSnapshotURL)
			let opponent = try jsonDecoder.decode([Player:[SquadEvaluation]].self, from: jsonData)
			
			let simulator = TWCombatSimulator(defenseGuild: evaluations, offenseGuild: opponent)
			
			print(simulator.simulate(plan: sections).description)
		}
		
	case "--tw-ga":
		// we filter the defensive teams
		
		let opponentSnapshotURL = URL(fileURLWithPath: arguments.removeFirst())
		let gaURL = URL(fileURLWithPath: arguments.removeFirst())
		
		run = { (guildFile: GuildFile) throws in
			let players = guildFile.players.filter { playerSelector.matches(player: $0) }
			var evaluations = try prepare(players, guildFile)
			
			// apply filtering to the defense teams
			let filter = PreferredViabilitySortFilter().compose(TWSquadFilter()).compose(ViableSquadFilter()).compose(SquadListFilter())
			for (player, ev) in evaluations {
				let newEv = filter.filter(ev)
				evaluations[player] = newEv
			}
			
			let jsonDecoder = JSONDecoder()
			let jsonData = try Data(contentsOf: opponentSnapshotURL)
			let opponent = try jsonDecoder.decode([Player:[SquadEvaluation]].self, from: jsonData)

			var initialChromosone: TWGAChromosone?
			if let gaData = try? Data(contentsOf: gaURL) {
				initialChromosone = try jsonDecoder.decode(TWGAChromosone.self, from: gaData)
			}

			let ga = TWGA(defenseGuild: evaluations, offenseGuild: opponent, config: TWGAConfig(), initialChromosone: initialChromosone)
			ga.announce()
			let chromosone = ga.run(count: 5)
			
			print("")
			print(chromosone)
			
			let jsonEncoder = JSONEncoder()
			let resultData = try jsonEncoder.encode(chromosone)
			
			try resultData.write(to: gaURL)
			
			let scheduler = TWGAScheduler()
			let defenseTuples = try scheduler.schedule(data: evaluations, chromosone: chromosone, config: TWGAConfig())
			var defense = [String:[Squad]]()
			for item in defenseTuples {
				defense[item.2, default: []].append(item.1.squad)
			}

			let simulator = TWCombatSimulator(defense: defense, offense: TWCombatSimulator.prepareOffense(offenseGuild: opponent))
			
			print(simulator.simulate(plan: ["B1", "B2", "B3", "B4", "T3", "T4", "T1", "T2"]).description)
			print("\n\n\n")
			print(simulator.simulate(plan: ["T1", "T2", "T3", "T4", "B1", "B2", "B3", "B4"]).description)
			print("\n\n\n")
			
			let reportScheduler = TWGASchedulerAdaptor(chromosone: chromosone, config: TWGAConfig())
			let report = TWPlanningReport(scheduler: reportScheduler)
			print(report.report(data: evaluations))
			
			let playerReport = TWPlayerReport(scheduler: reportScheduler)
			print(playerReport.report(data: evaluations))
		}


	case "--listPlayers":
		run = { guildFile in
			for player in guildFile.players.sorted() {
				print("\(player.stats.name)")
			}
		}
		
	case "--filter-tw":
		evaluationFilter = evaluationFilter.compose(TWSquadFilter())
		
	case "--filter-viable":
		evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
		
    case "--filter-only-nonviable":
        evaluationFilter = evaluationFilter.compose(OnlyNonViableSquadFilter())

    case "--filter-preferred":
		evaluationFilter = evaluationFilter.compose(PreferredSquadFilter())
		
	case "--filter-farm":
		evaluationFilter = evaluationFilter.compose(FarmFilter())
		
	case "--filter-squad-list":
		evaluationFilter = evaluationFilter.compose(SquadListFilter())
		
	case "--report-debug":
		report = DebugReport()
		run = squadRun()

	case "--report-assign":
		report = AssignmentReport()
		run = squadRun()

	case "--report-assign-nonames":
		report = AssignmentReport(showPlayerNames: false)
		run = squadRun()

	case "--report-farm":
		report = FarmReport()
		run = squadRun()

	case "--report-farm-csv":
		report = CSVSingleSquadFarmReport()
		run = squadRun()

	case "--report-farm-csv-critical":
		var r = CSVSingleSquadFarmReport()
		r.threshold = .needsRequiredGear
		report = r
		run = squadRun()

	case "--report-farm-critical":
		report = CriticalFarmReport()
		run = squadRun()

	case "--report-tw-planning":
        evaluationFilter = evaluationFilter.compose(TWSquadFilter())
        evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
        evaluationFilter = evaluationFilter.compose(SquadListFilter())
		report = TWPlanningReport()
		
	case "--report-tw-teams":
        evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
        evaluationFilter = evaluationFilter.compose(SquadListFilter())
		report = TWPlanningReport(scheduler: TeamNameScheduler())
		
	case "--report-tw-player":
        evaluationFilter = evaluationFilter.compose(TWSquadFilter())
        evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
        evaluationFilter = evaluationFilter.compose(SquadListFilter())
		report = TWPlayerReport()
		
	case "--report-tw-compare":
		evaluationFilter = evaluationFilter.compose(TWSquadFilter())
		evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
		evaluationFilter = evaluationFilter.compose(SquadListFilter())
		
		let opponentURL = URL(fileURLWithPath: arguments.removeFirst())

		run = { (guildFile: GuildFile) throws in
			var report = TWCompare()
			let players = guildFile.players.filter { playerSelector.matches(player: $0) }
			let evaluations = evaluatePlayers(players: players, guildFile: guildFile)
			report.add(guild: guildFile, evaluations: evaluations)
			
			let opponent = try load(opponentURL, "opponent")
			let opponentPlayers = opponent.players.filter { playerSelector.matches(player: $0) }
			let opponentEvaluations = evaluatePlayers(players: opponentPlayers, guildFile: opponent)
			report.add(guild: opponent, evaluations: opponentEvaluations)
			
			print(report.report())
		}

	case "--report-squad-summary":
		report = SquadSummaryReport()

	case "--player-squads":
		evaluationFilter = evaluationFilter.compose(TWPlayerSquadFilter())
		evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
		evaluationFilter = evaluationFilter.compose(SquadListFilter())
		
		report = PlayerSquadReport()

	case "--hstr-schedule":
		evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
		evaluationFilter = evaluationFilter.compose(SquadListFilter())
		allPlayers = true
		
		run = { (guildFile: GuildFile) throws in
			let players = guildFile.players.filter { playerSelector.matches(player: $0) }
			
			var report = MultiAssignmentReport()
			
			report.add(header: "P1", data: evaluatePlayers(players: players, guildFile: guildFile, squadBuilder: JTR.squadBuilder, squadEvaluator: JTR.squadEvaluator))
			report.add(header: "P2", data: evaluatePlayers(players: players, guildFile: guildFile, squadBuilder: HSTRP2.configurations["all"]!))
			report.add(header: "P3", data: evaluatePlayers(players: players, guildFile: guildFile, squadBuilder: MultipleSquadBuilder(HSTRP3.chexMixBuilder, HSTRP3.roloBuilder, HSTRP3.bhBuilder), squadEvaluator: HSTRP3.chexMixSquadEvaluator))
			report.add(header: "P3", data: evaluatePlayers(players: players, guildFile: guildFile, squadBuilder: NS.deathStormBuilder, squadEvaluator: NS.deathStormEvaluator))
			report.add(header: "P4", data: evaluatePlayers(players: players, guildFile: guildFile, squadBuilder: NS.p4NSBuilder, squadEvaluator: NS.p4NSEvaluator))
			report.add(header: "P4", data: evaluatePlayers(players: players, guildFile: guildFile, squadBuilder: TW.foSquadBuilder))
			
			print(report.report())
		}

	case "--jtr":
		playerSelector = JTR.playerSelect
		squadBuilder = JTR.squadBuilder
		squadEvaluator = JTR.squadEvaluator
		run = squadRun()
		
	case "--p2":
		squadBuilder = HSTRP2.configurations[arguments.removeFirst()]!
		run = squadRun()
		
	case "--chexmix":
		playerSelector = HSTRP3.chexMixPlayerSelect
		squadBuilder = HSTRP3.chexMixBuilder
		squadEvaluator = HSTRP3.chexMixSquadEvaluator
		run = squadRun()
		
	case "--p3-mix":
		squadBuilder = MultipleSquadBuilder(HSTRP3.chexMixBuilder, HSTRP3.roloBuilder, HSTRP3.bhBuilder)
		squadEvaluator = HSTRP3.chexMixSquadEvaluator
		run = squadRun()
		
	case "--p3-ns":
		playerSelector = NS.playerSelect
		squadBuilder = NS.deathStormBuilder
		squadEvaluator = NS.deathStormEvaluator
		run = squadRun()
		
	case "--p4-ns":
		playerSelector = NS.playerSelect
		squadBuilder = NS.p4NSBuilder
		squadEvaluator = NS.p4NSEvaluator
		run = squadRun()
		
	case "--p4-fo":
		// we will hijack the FO one from TW for now
		squadBuilder = TW.foSquadBuilder
		run = squadRun()
		
	case "--tw":
		squadBuilder = MultipleSquadBuilder(TW.configurations[arguments.removeFirst().lowercased()]!)
		run = squadRun()
		
	case "--ga-3v3":
		squadBuilder = GA3v3.ga3v3SquadBuilder
		report = TWPlanningReport()
		run = squadRun()

	default:
		usage(argument)
	}
}

guard let guildURL = guildURL else {
	fatalError("Must specify --guild")
}

if allPlayers {
	playerSelector = PlayerSelectAll()
}

do {
	let guildFile = try load(guildURL)
	try run(guildFile)
} catch {
	print(error)
}

print(NSDate.timeIntervalSinceReferenceDate - t)

