//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

protocol UnitProvider {

	/// Provide the Player's Unit or an empty Unit if not owned
	subscript(_ id: String) -> Unit? { get }
	
}

protocol UnitSelect {
	
    /// Select one or more Units from the UnitProvider
	func select(units: UnitProvider) -> [Unit]
	
}

func u(_ id: String) -> UnitSelect {
	return SingleUnitSelect(id)
}

func u(_ ids: [String]) -> UnitSelect {
	return OneOfUnitSelect(ids)
}

func u(_ ids: String...) -> UnitSelect {
	return OneOfUnitSelect(ids)
}

struct SingleUnitSelect : UnitSelect {
	let id: String
	
	init(_ id: String) {
		self.id = id
	}
	
	func select(units: UnitProvider) -> [Unit] {
		if let unit = units[id] {
			return [ unit ]
		} else {
			return [ ]
		}
	}
	
	static func build(_ ids: String...) -> [UnitSelect] {
		var result = [UnitSelect]()
		for id in ids {
			result.append(u(id))
		}
		return result
	}
}

struct OneOfUnitSelect : UnitSelect {
	
	let ids: [String]
	
	init(_ ids: String...) {
		self.ids = ids
	}
    
    init(_ ids: [String]) {
        self.ids = ids
    }
	
	func select(units: UnitProvider) -> [Unit] {
		var result = [Unit]()
		for id in ids {
			if let unit = units[id] {
				result.append(unit)
			}
		}
		return result
	}
}

protocol SquadBuilder {
	
	func generateSquads(units: UnitProvider) -> [Squad]
	
}

struct MultipleSquadBuilder : SquadBuilder {
	
	let squadBuilders: [SquadBuilder]
	
	init(_ squadBuilders: SquadBuilder...) {
		self.squadBuilders = squadBuilders
	}
	
	init(_ squadBuilders: [SquadBuilder]) {
		self.squadBuilders = squadBuilders
	}
	
	func generateSquads(units: UnitProvider) -> [Squad] {
		var result = Set<Squad>()
		
		for squadBuilder in squadBuilders {
			let squads = squadBuilder.generateSquads(units: units)
			result.formUnion(squads)
		}
		
		return Array(result)
	}
	
}

struct SelectUnitSquadBuilder : SquadBuilder {

	let properties: Squad.Properties?
	let unitSelectors: [UnitSelect]
	
	init(_ name: String, _ preference: Int, _ rules: [String : UnitEvaluator], _ unitSelectors: UnitSelect...) {
		self.properties = Squad.Properties(name: name, preference: preference, rules: rules)
		self.unitSelectors = unitSelectors
	}
	
	init(properties: Squad.Properties? = nil, _ unitSelectors: UnitSelect...) {
		self.properties = properties
		self.unitSelectors = unitSelectors
	}
	
	init(properties: Squad.Properties? = nil, unitSelectors: [UnitSelect]) {
		self.properties = properties
		self.unitSelectors = unitSelectors
	}

	func generateSquads(units: UnitProvider) -> [Squad] {
		var result = Set<Squad>()
		enumerateUnits(availableUnits: units, prefixSquad: [], remainingUnitSelectors: unitSelectors, result: &result)
		return Array(result)
	}
	
	private func enumerateUnits(availableUnits: UnitProvider, prefixSquad: [Unit], remainingUnitSelectors: [UnitSelect], result: inout Set<Squad>) {
		var unitSelectors = remainingUnitSelectors
		let currentUnitSelector = unitSelectors.removeFirst()
		
		for unit in currentUnitSelector.select(units: availableUnits) {
			if let minimumGP = properties?.unitMinimumGP, unit.power < minimumGP {
				continue
			}
			
			if prefixSquad.contains(unit) {
				// no repeats
				continue
			}
			
			var currentSquad = prefixSquad
			currentSquad.append(unit)
			
			if unitSelectors.isEmpty {
				result.insert(Squad(properties, units: currentSquad))
			} else {
				enumerateUnits(availableUnits: availableUnits, prefixSquad: currentSquad, remainingUnitSelectors: unitSelectors, result: &result)
			}
		}
	}
}
