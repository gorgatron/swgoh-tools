//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

protocol Report {

	func report(data: [Player:[SquadEvaluation]]) -> String
	
	func report(player: Player, evaluations: [SquadEvaluation]) -> String
	
}

extension Report {
	
	func report(data: [Player:[SquadEvaluation]]) -> String {
		var result = ""
		for player in data.keys.sorted() {
			result += report(player: player, evaluations: data[player]!)
			result += "\n"
		}
		return result
	}

}

/// produces detailed dump of the evaluations
struct DebugReport : Report {
	
	func report(player: Player, evaluations: [SquadEvaluation]) -> String {
		var result = ""
		
		result += "\(player.stats.name):\n"
		
		var index = 1
		for squadEv in evaluations {
			result += "Variation \(index) \(squadEv.squad.properties.name?.appending(" ") ?? "")status: \(squadEv.status) viabilityScore: \(squadEv.viabilityScore) \(squadEv.status.isViable ? "viable " : " ")preference: \(squadEv.preference) quality: \(Int(squadEv.scorePercent * 100))\n"
			
			for (unit, unitEv) in squadEv.unitEvaluations {
				result += "\(unit.commonName): \(unitEv.status) viabilityScore: \(unitEv.status.rawValue) preference: \(unitEv.preference) quality: \(Int(Double(unitEv.score) / Double(unitEv.max)  * 100)) \(unitEv.score)/\(unitEv.max)\n"
				for message in unitEv.messages {
					result += "\t\(message.message) \(message.status) \(message.score)/\(message.max)\n"
				}
			}
			result += "\n"

			index += 1
		}
		
		return result
	}
	
}

/// a report suitable for producing assignments -- use this with --filter-viable and the default sort order
struct AssignmentReport : Report {
	
	let showPlayerNames: Bool
	
	init(showPlayerNames: Bool = true) {
		self.showPlayerNames = showPlayerNames
	}
	
	func report(player: Player, evaluations: [SquadEvaluation]) -> String {
		var result = ""
		
		if showPlayerNames {
			result += "\(player.stats.name), "
		}
		
		var first = true
		for squadEv in evaluations {
			if !first {
				result += ", "
			}
			
			if squadEv.status <= .needsRequiredGear {
				result += "*** "
			}
			
			if let name = squadEv.squad.properties.name {
				result += "(\(name)) "
			}
			
			result += squadEv.squad.units.map { $0.commonName }.joined(separator: " ")
			first = false
		}
		
		return result
	}
	
}

/// a report suitable for producing assignments -- use this with --filter-viable and the default sort order
struct MultiAssignmentReport {
	
	var headers = [String]()
	var reportByPlayer = [Player:[String]]()
	
	init() {
	}
	
	mutating func add(header:String, data: [Player:[SquadEvaluation]]) {
		
		let columns = data.map { $0.value.count }.max()!
		
		for _ in 0 ..< columns {
			headers.append(header)
		}
		
		for (player, evaluations) in data {
			for squadEv in evaluations {
				var result = ""
				
				if squadEv.status <= .needsRequiredGear {
					result += "*** "
				}
				
				if let name = squadEv.squad.properties.name {
					result += "(\(name)) "
				}
				result += squadEv.squad.units.map { $0.commonName }.joined(separator: " ")

				reportByPlayer[player, default: []].append(result)
			}
			
			for _ in 0 ..< (columns - evaluations.count) {
				reportByPlayer[player, default: []].append("")
			}
		}
	}
	
	func report() -> String {
		var result = ""
		
		result += "Player,"
		result += headers.joined(separator: ",")
		result += "\n"
		
		for player in reportByPlayer.keys.sorted() {
			result += player.stats.name
			result += ","
			result += reportByPlayer[player, default: []].joined(separator: ",")
			result += "\n"
		}
		
		return result
	}
	
}


struct SquadSummaryReport : Report {
	
	func report(player: Player, evaluations: [SquadEvaluation]) -> String {
		var result = ""
		
		var index = 1
		for squadEv in evaluations {
			if !result.isEmpty {
				result += "\n"
			}
			result += "\(player.stats.name)"
			if let name = squadEv.squad.properties.name {
				result += ",\(name),"
			} else {
				result += ",,"
			}
			
			if squadEv.status <= .needsRequiredGear {
				result += "*** "
			}
			
			result += squadEv.squad.units.map {
				var z = ""
				for _ in $0.zetas {
					z += "z"
				}
				let stars = $0.stars == 7 ? "" : "\($0.stars)*/"
				return "\(z)\($0.commonName) \(stars)G\($0.gearLevel)"
				}.joined(separator: " ")
			
			let gp = squadEv.squad.gp
			result += ", \(gp)"
			
			index += 1
		}
		
		return result
	}
	
}

struct PlayerSquadReport : Report {
	
	func report(player: Player, evaluations: [SquadEvaluation]) -> String {
		var result = ""
		
		result += "**\(player.stats.name)**\n"
		
		var index = 1
		for squadEv in evaluations {
			
			let twConfig = OptimizedConfig()
			
			result += "\(index). \(twConfig.randomPlacementDefenseTeams.contains(squadEv.squad.nameOrDefault) ? ":shield: " : "") **\(squadEv.squad.nameOrDefault)**: "
			
			result += squadEv.squad.units.map {
				var z = ""
				for _ in $0.zetas {
					z += "z"
				}
				return "\(z)\($0.commonName)"
				}.joined(separator: " ")
			
			if squadEv.scorePercent >= 0.90 {
				result += " :star:"
			}

			if squadEv.status <= .needsRequiredGear {
				result += " :gear:"
			}

			let gp = squadEv.squad.gp
			result += " - \(gp)"
			result += "\n"
			
			let compareCounter = { (c1: TWCounter, c2: TWCounter ) -> Bool in
				if c1.effectiveness > c2.effectiveness {
					return true
				} else if c1.effectiveness == c2.effectiveness {
					return c1.team > c2.team
				} else {
					return false
				}
			}
			
			if let counters = TWCounter.counterMap[squadEv.squad.nameOrDefault] {
				result += "\t\tbeat by: "
				result += counters.values.sorted(by: compareCounter).map { $0.counter }.joined(separator: ", ")
				result += "\n"
			}
			if let counters = TWCounter.reverseCounterMap[squadEv.squad.nameOrDefault] {
				result += "\t\tgood vs: "
				result += counters.values.sorted(by: compareCounter).map { $0.team }.joined(separator: ", ")
				result += "\n"
			}

			index += 1
		}
		
		return result
	}
	
}


/// a report suitable for producing farming assignments -- use this with --filter-farm
struct FarmReport : Report {
	
	func report(player: Player, evaluations: [SquadEvaluation]) -> String {
		var result = ""

		let firstSquad = evaluations[0]
		if evaluations.count == 1 {
			result += "**\(player.stats.name)** \(firstSquad.squad.properties.name ?? "") \(Int(firstSquad.scorePercent * 100))/100 \(firstSquad.status.viability)\n"
		} else {
			result += "**\(player.stats.name)** \(firstSquad.status.viability)\n"
		}
		
		var index = 1
		for squadEv in evaluations {
			if evaluations.count > 1 {
				result += "Variation \(index)/\(evaluations.count) \(squadEv.squad.properties.name ?? "") \(Int(squadEv.scorePercent * 100))/100 \(squadEv.preference > 0 ? "(preferred) " : "")\(squadEv.status.marker)\n"
			}
			
			var oks = [Unit]()
			let appendOks = {
				result += oks.map { "*\($0.commonName)*" }.joined(separator: ", ")
				result += ": OK\n"
				oks.removeAll()
			}
			
			for (unit, unitEv) in squadEv.unitEvaluations {
				if unitEv.status == .ok {
					// collect up OK ones in a row
					oks.append(unit)
					
				} else {
					if !oks.isEmpty {
						appendOks()
					}
					
					result += "*\(unit.commonName)*:\n"
					for message in unitEv.messages.sorted() where message.status != .ok {
						result += "\t\(message.message) \(message.status.marker)\n"
					}
				}
			}
			if !oks.isEmpty {
				appendOks()
			}

			result += "\n"
			
			index += 1
		}

		return result
	}

}

struct CriticalFarmReport : Report {
	
	func report(player: Player, evaluations: [SquadEvaluation]) -> String {
		let lowestStatus = evaluations.map { $0.status }.min()
		if let lowestStatus = lowestStatus, lowestStatus > .needsRequiredGear {
			return ""
		}

		var result = ""
		
		let firstSquad = evaluations[0]
		if evaluations.count == 1 {
			result += "**\(player.stats.name)** \(firstSquad.squad.properties.name ?? "") \(Int(firstSquad.scorePercent * 100))/100 \(firstSquad.status.viability)\n"
		} else {
			result += "**\(player.stats.name)** \(firstSquad.status.viability)\n"
		}
		
		var index = 1
		for squadEv in evaluations {
			if evaluations.count > 1 {
				result += "Variation \(index)/\(evaluations.count) \(squadEv.squad.properties.name ?? "") \(Int(squadEv.scorePercent * 100))/100 \(squadEv.preference > 0 ? "(preferred) " : "")\(squadEv.status.marker)\n"
			}
			
			for (unit, unitEv) in squadEv.unitEvaluations {
				result += "*\(unit.commonName)*:\n"
				for message in unitEv.messages.sorted() where message.status <= Status.needsRequiredGear {
					result += "\t\(message.message) \(message.status.marker)\n"
				}
			}
			
			result += "\n"
			
			index += 1
		}
		
		return result
	}
	
}

/// a report that outputs info for a single squad in CSV form
struct CSVSingleSquadFarmReport : Report {
	
	var threshold = Status.needsTuning
	
	func report(data: [Player:[SquadEvaluation]]) -> String {
		var result = ""
		for (_, evaluations) in data {
			if !evaluations.isEmpty {
				result += header(evaluations[0])
				result += "\n"
				break
			}
		}
		for player in data.keys.sorted() {
			result += report(player: player, evaluations: data[player]!)
			result += "\n"
		}
		return result
	}
	
	func header(_ evaluation:SquadEvaluation) -> String {
		return "Player,Score,Viability Score,Viable," + evaluation.squad.units.map { $0.commonName }.joined(separator: ",")
	}

	func report(player: Player, evaluations: [SquadEvaluation]) -> String {
		var result = ""
		
		for squadEv in evaluations {
			result += player.stats.name
			result += ","
			result += Int(squadEv.scorePercent * 100).description
			result += ","
			result += squadEv.viabilityScore.description
			result += ","
			result += squadEv.status.isViable ? "yes" : "no"
			result += ","
			
			for (_, unitEv) in squadEv.unitEvaluations {
				result += "\""
				let messages = unitEv.messages.filter { $0.status <= threshold }.sorted()
				var printedMessage = false
				for message in messages {
					if printedMessage {
						result += "\n"
					}
					result += "\(message.message) \(message.status.unicodeMarker)"
					printedMessage = true
				}
				result += "\","
			}
		}
		
		return result
	}
	
}

struct GeonosianTBReport : Report {
	
	let galacticRepublicNames = characters.values.filter({ $0.categories.contains("Galactic Republic") && !$0.ship }).map({ $0.id }).sorted()
	let separatistNames = characters.values.filter({ $0.categories.contains("Separatist") && !$0.ship }).map({ $0.id }).sorted()

	func report(data: [Player:[SquadEvaluation]]) -> String {
		var result = header() + "\n"
		for player in data.keys.sorted() {
			result += player.stats.name
			result += ","
			var unitGp = [String]()
			for unitId in galacticRepublicNames.adding(separatistNames) {
				if let unit = player.units[unitId] {
					unitGp.append(unit.power.description)
				} else {
					unitGp.append("")
				}
			}
			result += unitGp.joined(separator: ",")
			result += "\n"
		}
		return result
	}
	
	func header() -> String {
		var header = "Player,"
		header += galacticRepublicNames.map { characters[$0]!.commonName }.joined(separator: ",")
		header += ","
		header += separatistNames.map { characters[$0]!.commonName }.joined(separator: ",")
		return header
	}
	
	func report(player: Player, evaluations: [SquadEvaluation]) -> String {
		return ""
	}
	
}

