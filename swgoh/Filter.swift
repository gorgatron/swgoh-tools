//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

protocol Filter {
	
	/// filter and possibly sort the squad evaluations
    func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation]
    
}

extension Filter {
	
	func compose(_ other: Filter) -> Filter {
		return ComposeFilter(filter1: self, filter2: other)
	}
	
}

let preferenceSort = { (s1: SquadEvaluation, s2: SquadEvaluation) -> Bool in
	// sort by preference, viabilityScore and scorePercent
	let p1 = s1.preference
	let p2 = s2.preference
	
	if p1 == p2 {
		let v1 = s1.viabilityScore
		let v2 = s2.viabilityScore
		
		if v1 == v2 {
			let sp1 = s1.scorePercent
			let sp2 = s2.scorePercent
			
			if sp1 == sp2 {
				return s1.squad.gp > s2.squad.gp
			} else {
				return sp1 > sp2
			}
		} else {
			return v1 > v2
		}
	} else {
		return p1 > p2
	}
}

struct ComposeFilter : Filter {
	
	let filter1: Filter
	let filter2: Filter
	
	func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
		// filter2 runs on the output of filter1
		return filter2.filter(filter1.filter(squadEvaluations))
	}
	
}

struct AllFilter : Filter {
	
	func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
		return squadEvaluations
	}

}

struct ViableSquadFilter : Filter {

    func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
        return squadEvaluations.filter { $0.status.isViable }
    }
    
}

/// only return results if all of them are not viable
struct OnlyNonViableSquadFilter : Filter {
    
    func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
        let viableCount = squadEvaluations.reduce(0) { $0 + ($1.status.isViable ? 1 : 0) }
        if viableCount > 0 {
            return []
        } else {
            return squadEvaluations
        }
    }
    
}

struct PreferredSquadFilter : Filter {
    
    func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
        return squadEvaluations.filter { $0.preference > 0 }
    }
    
}

/// sort by viability score
struct ViabilitySortFilter : Filter {
	
	func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
		return squadEvaluations.sorted(by: { (s1, s2) -> Bool in
			return s1.viabilityScore > s2.viabilityScore
		})
	}
	
}

/// sort by preference and then viability score
struct PreferredViabilitySortFilter : Filter {
	
	func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
		return squadEvaluations.sorted(by: preferenceSort)
	}
	
}

struct FarmFilter : Filter {
	
	func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
		// Produce output which contains some combination of these:
		// viable, preferred (max viableScore, percentScore)
		// viable (max viableScore, percentScore)
		// not viable, preferred (max viableScore, percentScore)
		// not viable (max viableScore, percentScore)
		
		let viablePreferred = squadEvaluations.filter { $0.status.isViable && $0.preference > 0 }.min(by: preferenceSort)
		let viable = squadEvaluations.filter { $0.status.isViable }.min(by: preferenceSort)
		let notViablePreferred = squadEvaluations.filter { !$0.status.isViable && $0.preference > 0 }.min(by: preferenceSort)
		let notViable = squadEvaluations.filter { !$0.status.isViable }.min(by: preferenceSort)
				
		if let s = viablePreferred {
			return [s]
		}
		
		if let s = viable {
			if let nvp = notViablePreferred {
				return [s, nvp]
			} else {
				return [s]
			}
		}
		
		if let s = notViablePreferred {
			return [s]
		}
		
		if let s = notViable {
			return [s]
		}
		
		return []
	}
	
}

struct SquadListFilter : Filter {
	
	func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
		// given a list of squads in priority order, return any that have not used units from previous squads
		
		var usedUnits = Set<Unit>()
		var result = [SquadEvaluation]()
		
		for squadEvaluation in squadEvaluations {
			var usable = true
			for unit in squadEvaluation.squad.units {
				if usedUnits.contains(unit) {
					usable = false
					break
				}
			}
			
			if usable {
				usedUnits.formUnion(squadEvaluation.squad.units)
				result.append(squadEvaluation)
			}
		}
		
		return result
	}
	
}
