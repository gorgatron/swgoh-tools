//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

struct JTR {
	
	static let playerSelect = PlayerSelectCharacter("REYJEDITRAINING")
	
	static let squadBuilder = SelectUnitSquadBuilder(u("REYJEDITRAINING"), u("REY", "C-3PO"), u("BB8"), u("R2D2_LEGENDARY"), u("RESISTANCETROOPER", "VISASMARR", "BARRISSOFFEE", "Finn"))

	
	static let squadEvaluator = SquadEvaluator(rules: [
		"REYJEDITRAINING" : UnitEvaluator(
			AllOmegasRule(),
			OmegaZetaAbilityRule("leaderskill_REYJEDITRAINING", status: .needsRequiredGear),
			OmegaZetaAbilityRule("uniqueskill_REYJEDITRAINING02", status: .needsTuning),

			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
		
			StatRule(.speed, atLeast: 200, target: 220, weight: 200),
			StatRule(.criticalDamage, atLeast: 186, target: 216, weight: 150),
			StatRule(.potency, atLeast: 55, target: 70),
			StatRule(.physicalDamage, atLeast: 2900, target: 3400)
		),
		"REY" : UnitEvaluator(
			AllOmegasRule(),
			
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			StatRule(.speed, atLeast: 220, target: 250, weight: 200),
			StatRule(.criticalDamage, atLeast: 186, target: 216, weight: 150),
			StatRule(.physicalDamage, atLeast: 2900, target: 3400)
		),
		"BB8" : UnitEvaluator(
			AllOmegasRule(),
			OmegaZetaAbilityRule("uniqueskill_BB801", status: .needsTuning),

			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			StatRule(.speed, atLeast: 230, target: 270, weight: 200),
			StatRule(.physicalCritChance, atLeast: 30, target: 45, weight: 150)
		),
		"R2D2_LEGENDARY" : UnitEvaluator(
			AllOmegasRule(),
			OmegaZetaAbilityRule("uniqueskill_R2D2_LEGENDARY02", status: .needsTuning),
			OmegaZetaAbilityRule("uniqueskill_R2D2_LEGENDARY01", weight: 50, status: .needsTuning),

			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			StatRule(.speed, atLeast: 220, target: 260, weight: 200),
			StatRule(.physicalCritChance, atLeast: 30, target: 45, weight: 150)
		),
		"RESISTANCETROOPER" : UnitEvaluator(
			AllOmegasRule(),
			
			// we prefer RT to all others
			PreferenceRule(),
			
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			StatRule(.criticalDamage, atLeast: 186, target: 216, weight: 150),
			StatRule(.physicalDamage, atLeast: 3000, target: 3400),
			
			CustomRule() { unit in
				if unit.stats.speed > 150 {
					return Message(message: "speed is too high \(unit.stats.speed) - target +0", status: .needsTuning, score: 0, max: 100)
				} else {
					return Message(message: "speed ok", status: .ok, score: 100, max: 100)
				}
			}
		),
		
		"FINN" : UnitEvaluator(
			AllOmegasRule(),
			
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			PreferenceRule()
		),
		"C3POLEGENDARY" : UnitEvaluator(
			AllOmegasRule(),
			
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			PreferenceRule()
		),

		// not preferred
		"VISASMARR" : UnitEvaluator(
			AllOmegasRule(),
			
			PreferenceRule(preference: -1),
			
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			OmegaZetaAbilityRule("uniqueskill_VISASMARR01", status: .needsTuning)
		),
		"BARRISSOFFEE" : UnitEvaluator(
			PreferenceRule(preference: -1),
			
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			OmegaZetaAbilityRule("uniqueskill_BARRISSOFFEE01", status: .needsTuning),

			OmegaZetaAbilityRule("basicskill_BARRISSOFFEE", status: .needsTuning),
			OmegaZetaAbilityRule("specialskill_BARRISSOFFEE01", status: .needsTuning)
		)


	])

}
