//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

struct GA3v3 {
	
	// https://gaming-fans.com/2018/12/the-grand-arena-3v3/
	static let builders: [SquadBuilder] = [
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Sith", preference: 10, rules: rules),  u("DARTHTRAYA"), u("DARTHSION"), u("DARTHNIHILUS")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Revan", preference: 10, rules: rules), u("JEDIKNIGHTREVAN"), u("GRANDMASTERYODA"), u("Jolee Bindo")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "CLS", preference: 10, rules: rules), u("COMMANDERLUKESKYWALKER"), u("CHEWBACCALEGENDARY"), u("HANSOLO")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Qira", preference: 10, rules: rules), u("QIRA"), u("L3_37"), u("ENFYSNEST")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "JTR", preference: 10, rules: rules), u("REYJEDITRAINING"), u("BB8"), u("R2D2_LEGENDARY")),
		
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "BS", preference: 5, rules: rules), u("BASTILASHAN"), u("AAYLASECURA"), u("HERMITYODA")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Sith2", preference: 5, rules: rules), u("Palpatine"), u("BASTILASHANDARK"), u("sith trooper")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "BH", preference: 5, rules: rules), u("Bossk"), u("Dengar"), u("Boba Fett")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "BH2", preference: 5, rules: rules), u("Jango Fett"), u("Cad Bane"), u("Embo")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "FO", preference: 5, rules: rules), u("KYLORENUNMASKED"), u("KYLOREN"), u("First Order Stormtrooper")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "FO2", preference: 5, rules: rules), u("Phasma"), u("First Order Executioner"), u("First Order Officer")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Wampa", preference: 5, rules: rules), u("Rex"), u("Wampa"), u("Visas")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Thrawn", preference: 5, rules: rules), u("Thrawn"), u("Death Trooper"), u("Magmatrooper")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Res", preference: 5, rules: rules), u("Finn"), u("Poe Dameron"), u("Resistance Trooper")),
		
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Troopers", preference: 1, rules: rules), u("Veers"), u("Starck"), u("Range")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Rogue", preference: 1, rules: rules), u("Jyn Erso"), u("Chirrut"), u("Baze Malbus")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Wiggs", preference: 1, rules: rules), u("Wedge"), u("Biggs"), u("Scarif Rebel")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "C3P0", preference: 1, rules: rules), u("Ackbar"), u("Princess Leia"), u("C-3PO")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Ewok", preference: 1, rules: rules), u("Chief Chirpa"), u("Paploo"), u("Wicket")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Ewok2", preference: 1, rules: rules), u("Teebo"), u("Ewok Elder"), u("Logray")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Phoenix", preference: 1, rules: rules), u("Hera Syndula"), u("Chopper"), u("Ezra Bridger")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Maul", preference: 1, rules: rules), u("Darth Maul"), u("Savage Oppress"), u("Sith Assassin")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Maul", preference: 1, rules: rules), u("Darth Vader"), u("Darth Sidious"), u("Shore Trooper")),
		
		// I am demoting this one, I don't like it
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "NS", preference: 0, rules: rules), u("Talzin"), u("Asajj"), u("Daka")),
		
		// from the GA Counter Discord, https://cdn.discordapp.com/attachments/539385210685358101/542010961465180168/3V3.png
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Troopers", preference: 1, rules: rules), u("Veers"), u("Starck"), u("Snowtrooper")),
		SelectUnitSquadBuilder(properties: Squad.Properties(preference: 1, rules: rules), u("Princess Leia"), u("Paploo"), u("C3P0")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Empire", preference: 1, rules: rules), u("Tarkin"), u("TIE Fighter Pilot"), u("Shoretrooper")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "FO", preference: 1, rules: rules), u("Sidious"), u("First Order Officer"), u("First Order Executioner")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Revan", preference: 10, rules: rules), u("Revan"), u("Grand Master Yoda"), u("General Kenobi")),
		SelectUnitSquadBuilder(properties: Squad.Properties(preference: 1, rules: rules), u("Old Ben"), u("REY"), u("Nightsister Spirit")),
		SelectUnitSquadBuilder(properties: Squad.Properties(preference: 1, rules: rules), u("Bossk"), u("Cad Bane"), u("Zam")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "NS", preference: 5, rules: rules), u("Talzin"), u("Acolyte"), u("Zombie")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "BS", preference: 5, rules: rules), u("BASTILASHAN"), u("Ezra"), u("Hermit Yoda")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "NS2", preference: 5, rules: rules), u("Asajj"), u("Daka"), u("Talia")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Chaze", preference: 2, rules: rules), u("Ackbar"), u("Chirrut"), u("Baze")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Ewoks", preference: 2, rules: rules), u("Chief Chirpa"), u("Wicket"), u("Ewok Scout")),

		
		// my defense
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "Ewoks", preference: 2, rules: rules), u("Chief Chirpa"), u("Elder"), u("Paploo")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "NS-dk", preference: 0, rules: rules), u("Daka"), u("Acolyte"), u("Zombie")),
		SelectUnitSquadBuilder(properties: Squad.Properties(name: "FO-dk", preference: 0, rules: rules), u("KYLORENUNMASKED"), u("KYLOREN"), u("First Order Tie Pilot"))
	]
	
	static let ga3v3SquadBuilder = MultipleSquadBuilder(builders)

	static let rules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
	]

	
}
