//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

struct HSTRP2 {
    
    // jedi -- https://drive.google.com/file/d/1Z06JomJBransHh8MDl7ikQKsn_J383st/view
	static let jediSquadBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "GK Jedi", preference: 4, rules: jediRules), u("GENERALKENOBI"), u("GRANDMASTERYODA"), u("EZRABRIDGERS3"), u("HERMITYODA", "OLDBENKENOBI"), u("BASTILASHAN", "GRANDADMIRALTHRAWN"))
	
	static let bastilaSquadBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Bastila Jedi", preference: 6, rules: jediRules), u("BASTILASHAN"), u("GENERALKENOBI"), u("GRANDMASTERYODA"), u("HERMITYODA"), u("EZRABRIDGERS3"))
    
    static let jediRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "BASTILASHAN" : UnitEvaluator(
            PreferenceRule(preference: 1),
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
		"HERMITYODA" : UnitEvaluator(
			PreferenceRule(preference: 1),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		)

    ]

    // leia spam -- https://drive.google.com/file/d/1zRxBz7r5qOS-G9WCsz3PHHSz6VRNmLzt/view
    // https://www.reddit.com/r/SWGalaxyOfHeroes/comments/8hv47a/heroic_sith_p2_ackbarleia_teams_are_the_best_in/
    static let leiaSpamBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "MG Leia", rules: leiaSpamRules), u("ADMIRALACKBAR"), u("PRINCESSLEIA", "SABINEWRENS3"), u("GENERALKENOBI"), u("GRANDADMIRALTHRAWN", "STORMTROOPERHAN", "SUNFAC"), u("JAWAENGINEER", "HERMITYODA", "BARRISSOFFEE"))
    
    static let leiaSpamRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "GENERALKENOBI" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "GRANDADMIRALTHRAWN" : UnitEvaluator(
            PreferenceRule(preference: 1),
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "BARRISSOFFEE" : UnitEvaluator(
            PreferenceRule(preference: 1),
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        )

    ]

    // revan, BS, jolee/ezra, hermit, GMY
    static let revanSquadBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Revan", preference: 10, rules: revanRules), u("JEDIKNIGHTREVAN"), u("GRANDMASTERYODA"), u("BASTILASHAN"), u("JOLEEBINDO"), u(UnitTypes.jedi))

    static let revanRules = [
		"JEDIKNIGHTREVAN" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"GRANDMASTERYODA" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"BASTILASHAN" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"JOLEEBINDO" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"HERMITYODA" : UnitEvaluator(
            PreferenceRule(preference: 2),
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
		"BARRISSOFFEE" : UnitEvaluator(
			PreferenceRule(preference: 1),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"DEFAULT" : UnitEvaluator(
			PreferenceRule(preference: -5),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),

    ]
    
    // beastmode -- https://drive.google.com/file/d/1EzVY-UgJ4RaaNTfhYERNR4CkXfccvwXI/view
	static let wampaSquadBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Beastmode", preference: 2, rules: wampaRules), u("BOBAFETT", "GENERALKENOBI"), u("WAMPA"), u("HERMITYODA"), u("GRANDADMIRALTHRAWN"), u("GRANDMASTERYODA", "BARRISSOFFEE", "SABINEWRENS3"))
    
    static let wampaRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
    ]
    
    // sith -- https://gaming-fans.com/swgoh-advanced/sith-triumvirate-raid-guide/sith-triumvirate-raid-phase-2-teams-strategy/
	// palp, thrawn, DN/sion, tarkin, vader.  the squad is not high damage so the negative preference offsets the potential bonuses from the team
	static let sithSquadBuilder1 = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Sith", preference: -5, rules: sithRules),  u("EMPERORPALPATINE"), u("DARTHSION"), u("DARTHNIHILUS"), u(UnitTypes.sith), u(UnitTypes.sith))
	static let sithSquadBuilder2 = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Sith", preference: -5, rules: sithRules),  u("EMPERORPALPATINE"), u("GRANDADMIRALTHRAWN"), u("SION", "DARTHNIHILUS"), u(UnitTypes.sith), u(UnitTypes.sith))
	static let sithSquadBuilder = MultipleSquadBuilder(sithSquadBuilder1, sithSquadBuilder2)

    static let sithRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
		"SITHMARAUDER" : UnitEvaluator(
			PreferenceRule(preference: 2),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"SITHTROOPER" : UnitEvaluator(
			PreferenceRule(preference: 2),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"GRANDMOFFTARKIN" : UnitEvaluator(
			PreferenceRule(preference: 1),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"VADER" : UnitEvaluator(
			PreferenceRule(preference: 1),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
    ]

	// mission impozalbar - qira, young han, hermit, zaalbar, mission
	static let impozaalbarSquadBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "impozaalbar", rules: impozalbarRules),  u("QIRA"), u("YOUNGHAN"), u("HERMITYODA"), u("ZAALBAR"), u("MISSIONVAO"))
	
	static let impozalbarRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		]

	// veers, thrawn, snowtrooper, shore, starck
	static let trooperSquadBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "troopers", preference: -2, rules: trooperRules),  u("VEERS"), u("GRANDADMIRALTHRAWN"), u("SNOWTROOPER"), u("COLONELSTARCK"), u("SHORETROOPER", "STORMTROOPER"))
	
	static let trooperRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		]
	
	// nute, leia/ezra, scarif/st han, acolyte/gk, hermit/wampa
	static let nuteSquadBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Church of Nute", rules: nuteRules),  u("NUTEGUNRAY"), u("PRINCESSLEIA", "EZRABRIDGERS3"), u("SCARIFREBEL", "STORMTROOPERHAN"), u("NIGHTSISTERACOLYTE", "GENERALKENOBI"), u("HERMITYODA", "WAMPA"))
	
	static let nuteRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		]

	// hera, ezra, kanan, sabine, zeb
	static let phoenixSquadBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Phoenix", preference: 1, rules: nuteRules),  u("HERASYNDULLAS3"), u("EZRABRIDGERS3"), u("SABINEWRENS3"), u("KANANJARRUSS3"), u("ZEBS3"))

	static let phoenixSquadBuilder2 = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Phoenix2", preference: -3, rules: nuteRules),  u("HERASYNDULLAS3"), u("Chopper"), u("SABINEWRENS3"), u("KANANJARRUSS3"), u("ZEBS3"))

	static let phoenixRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		]

	static let combinedSqadBuilder = MultipleSquadBuilder(jediSquadBuilder, bastilaSquadBuilder, leiaSpamBuilder, revanSquadBuilder, wampaSquadBuilder, sithSquadBuilder1, sithSquadBuilder2, impozaalbarSquadBuilder, trooperSquadBuilder, nuteSquadBuilder, phoenixSquadBuilder, phoenixSquadBuilder2)
	
	static let configurations: [String:SquadBuilder] = [
		"all" : combinedSqadBuilder,
		"jedi" : jediSquadBuilder,
		"spam" : leiaSpamBuilder,
		"revan" : revanSquadBuilder,
		"wampa" : wampaSquadBuilder,
		"sith" : sithSquadBuilder,
		"zaalbar" : impozaalbarSquadBuilder,
		"troopers" : trooperSquadBuilder,
		"nute" : nuteSquadBuilder,
		"phoenix" : phoenixSquadBuilder,
		]


	// leia, hermit, scarif/gk, sabine, ezra/thrawn
	// boba, thrawn, sabine, hermit/barriss, leia/wampa
	// chirpa, wicket, elder, scout, logray
	// quira, young han, young lando, vandor, storm han
	// GMY, hermit, GK, aayla, ezra
	
}
