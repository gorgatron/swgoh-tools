//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

/// ChexMix based on https://drive.google.com/file/d/1Pg1goUW-bRebWd8nzJ9es8nF0Nk4S9KC/view
struct HSTRP3 {
	
	static let playerSelect = PlayerSelectCharacter("DEATHTROOPER")
	
	static let highDamageAttackerIds = [ "PAO", "ANAKINKNIGHT", "EMPERORPALPATINE", "POGGLETHELESSER",
		"CHEWBACCALEGENDARY", "CHIRRUTIMWE", "CT7567",
		"BOBAFETT", "NIGHTSISTERSPIRIT", "FIRSTORDERTIEPILOT", "TIEFIGHTERPILOT", "PRINCESSLEIA", "SMUGGLERCHEWBACCA", "EWOKSCOUT", "NIGHTSISTERINITIATE", "NIGHTSISTERACOLYTE" ]
	
	static let highDamageAttackers = u(highDamageAttackerIds)
	
	// Note: the second group could contain "GRANDMASTERYODA" but we want him for P2.  QGJ is another option with offense up, but he is not an attacker.
	static let chexMixBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "ChexMix"),
		 u("COMMANDERLUKESKYWALKER"),
		 u("HANSOLO"),
		 u("DEATHTROOPER"),
		 u("PAO", "ANAKINKNIGHT", "EMPERORPALPATINE", "POGGLETHELESSER", "QUIGONJINN"),
		 u("chirrut", "CHEWBACCALEGENDARY", "CT7567", "BAZEMALBUS", "STORMTROOPERHAN"))
	
	static let roloBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "ROLO", preference: -4),
		u("HOTHLEIA"),
		u("PAO", "ANAKINKNIGHT", "POGGLETHELESSER"),
		highDamageAttackers, highDamageAttackers, highDamageAttackers)
	
	static let bhBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "BH", preference: -6), u("BOSSK"), u("GREEDO"), u(highDamageAttackerIds.adding("PAO", "ANAKINKNIGHT", "POGGLETHELESSER")), highDamageAttackers, highDamageAttackers)
	
	// harder to use but potentially higher damage
	static let flamingGreedoBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Flaming", preference: 2),
		u("BOBAFETT"),
		u("GREEDO"),
		u("DEATHTROOPER"),
		u("PAO", "ANAKINKNIGHT", "EMPERORPALPATINE", "POGGLETHELESSER"),
		highDamageAttackers)
	
	static let customLowGearRule = CustomRule { (unit) -> Message in
		switch unit.gearLevel {
		case 0 ... 6:
			return Message(message: "gear level must be at least 7", status: .needsRequired, score: 0, max: 400)
		case 7 ... 8:
			return Message(message: "should be G9 (G\(unit.gearLevel))", status: .needsRequiredGear, score: 100, max: 400)
		case 9:
			return Message(message: "gear G10/G12", status: .needsTuning, score: 150, max: 400)
		case 10:
			return Message(message: "gear G10/G12", status: .needsTuning, score: 200, max: 400)
		case 11:
			return Message(message: "gear G11/G12", status: .needsTuning, score: 300, max: 400)
		case 12:
			return Message(message: "G12", status: .ok, score: 400, max: 400)
		default:
			return Message(message: "unknown gear level", status: .needsRequired, score: 0, max: 400)
		}
	}
	
	static let squadEvaluator = SquadEvaluator(rules: [
		"COMMANDERLUKESKYWALKER" : UnitEvaluator(
			AllOmegasRule(),
			
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"HANSOLO" : UnitEvaluator(
			AllOmegasRule(),
			
			// the team works without this, but it is double damage with it
			OmegaZetaAbilityRule("uniqueskill_HANSOLO01", status: .needsRequired),

			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"DEATHTROOPER" : UnitEvaluator(
			// this is the skill for deathmark -- it doesn't work without it
			OmegaZetaAbilityRule("specialskill_DEATHTROOPER02", status: .needsRequired),

			// DT can work with lower gear levels
			CustomRule { (unit) -> Message in
				switch unit.gearLevel {
				case 0 ... 4:
					return Message(message: "gear level must be at least 5", status: .needsRequired, score: 0, max: 400)
				case 5 ... 8:
					return Message(message: "should be G9 (G\(unit.gearLevel))", status: .needsRequiredGear, score: 100, max: 400)
				case 9:
					return Message(message: "gear G10/G12", status: .needsTuning, score: 150, max: 400)
				case 10:
					return Message(message: "gear G10/G12", status: .needsTuning, score: 200, max: 400)
				case 11:
					return Message(message: "gear G11/G12", status: .needsTuning, score: 300, max: 400)
				case 12:
					return Message(message: "G12", status: .ok, score: 400, max: 400)
				default:
					return Message(message: "unknown gear level", status: .needsRequired, score: 0, max: 400)
				}
			},

			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"PAO" : UnitEvaluator(
			// preferred -- offense up
			PreferenceRule(preference: 1),

			// Pao can work with lower gear levels
			customLowGearRule,

			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"ANAKINKNIGHT" : UnitEvaluator(
			// offense up
			customLowGearRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"EMPERORPALPATINE" : UnitEvaluator(
			// just an attacker with high offense
			OmegaZetaAbilityRule("specialskill_EMPERORPALPATINE01"),
			
			PreferenceRule(preference: -1),

			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"POGGLETHELESSER" : UnitEvaluator(
			// offense up
			customLowGearRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"CHEWBACCALEGENDARY" : UnitEvaluator(
			AllOmegasRule(),
			
			OmegaZetaAbilityRule("uniqueskill_CHEWBACCALEGENDARY01", status: .needsTuning),
			OmegaZetaAbilityRule("uniqueskill_CHEWBACCALEGENDARY02", status: .needsTuning),

			// we only prefer this is it is 7-star (as a legendary, it is not reasonable to farm it at any time).  but if you do have him, a boost of 2
			PreferenceRule(preference: 2, ifSevenStar: true),

			UnitRule(.gearLevel, atLeast: 9, target: 9, weight: 400, status: .needsGear),
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"CHIRRUTIMWE" : UnitEvaluator(
			PreferenceRule(preference: 1),
			
			UnitRule(.gearLevel, atLeast: 9, target: 9, weight: 400, status: .needsGear),
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"CT7567" : UnitEvaluator(
			OmegaZetaAbilityRule("specialskill_REX01"),
			
			UnitRule(.gearLevel, atLeast: 9, target: 9, weight: 400, status: .needsGear),
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"GREEDO" : UnitEvaluator(
			StatRule(.physicalCritChance, atLeast: 50, target: 65),
			
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"DEFAULT" : UnitEvaluator(
			// offense up
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			// this is a random attacker -- we don't like it but we will take what we can get
			PreferenceRule(preference: -3),
			
			// bonus points for higher damage
			StatRule(.physicalDamage, atLeast: 2000, target: 3400)
			
			
		)
	])
	
	static let roloRules = [
		"HOTHLEIA" : UnitEvaluator(
			CustomRule { (unit) -> Message in
				switch unit.gearLevel {
				case 0 ... 6:
					return Message(message: "gear level must be at least 5", status: .needsRequired, score: 0, max: 400)
				case 7:
					return Message(message: "should be G8 (G\(unit.gearLevel))", status: .needsRequiredGear, score: 100, max: 400)
				case 8 ... 12:
					return Message(message: "G\(unit.gearLevel)", status: .ok, score: 400, max: 400)
				default:
					return Message(message: "unknown gear level", status: .needsRequired, score: 0, max: 400)
				}
			},
			
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"PAO" : UnitEvaluator(
			PreferenceRule(preference: 1),
			customLowGearRule,
			
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"ANAKINKNIGHT" : UnitEvaluator(
			PreferenceRule(preference: 1),
			customLowGearRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"POGGLETHELESSER" : UnitEvaluator(
			PreferenceRule(preference: 1),
			customLowGearRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"DEFAULT" : UnitEvaluator(
			// offense up
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			// this is a random attacker -- we don't like it but we will take what we can get
			PreferenceRule(preference: -3),
			
			// bonus points for higher damage
			StatRule(.physicalDamage, atLeast: 2000, target: 3400)
		)
	]
	
	static let bhRules = [
		
		"PAO" : UnitEvaluator(
			PreferenceRule(preference: 1),
			customLowGearRule,

			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"ANAKINKNIGHT" : UnitEvaluator(
			PreferenceRule(preference: 1),
			customLowGearRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"POGGLETHELESSER" : UnitEvaluator(
			PreferenceRule(preference: 1),
			customLowGearRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"BOBAFETT" : UnitEvaluator(
			PreferenceRule(preference: 1),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),

		"DEFAULT" : UnitEvaluator(
			// offense up
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			// this is a random attacker -- we don't like it but we will take what we can get
			PreferenceRule(preference: -3),
			
			// bonus points for higher damage
			StatRule(.physicalDamage, atLeast: 2000, target: 3400)
		)
	]

}
