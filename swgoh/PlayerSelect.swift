//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

protocol PlayerSelect {
	
	func matches(player: Player) -> Bool
	
}

struct PlayerSelectAll : PlayerSelect {
	
	func matches(player: Player) -> Bool {
		return true
	}
	
}

struct PlayersSelectMatch : PlayerSelect {
	
	let names: [String]
	let invert: Bool
	
	init(_ names: [String], invert: Bool = false) {
		self.names = names.map { $0.lowercased() }
		self.invert = invert
	}
	
	func matches(player: Player) -> Bool {
		let match = player.stats.name.lowercased()
		
		return names.contains { match.contains($0) } != invert
	}
	
}

struct PlayerSelectCharacter : PlayerSelect {
	
	let match: String
	
	init(_ match: String) {
		self.match = match
	}

	func matches(player: Player) -> Bool {
		return player.units[match] != nil
	}
	
}
