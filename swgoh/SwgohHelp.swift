//
//  SwgohHelp.swift
//  swgoh
//
//  Created by David Koski on 5/13/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

// Serialization for https://api.swgoh.help with a mappping to the structures in Serialization (Player, etc.)

struct SWGOHHelp {
	
	static func load(_ data:Data, guildName: String? = nil) throws -> GuildFile {
		let jsonDecoder = JSONDecoder()

		let unitsData = try jsonDecoder.decode([String:[Unit]].self, from: data)

		var playerUnits = [Int:[String:swgoh.Unit]]()
		var playerMap = [Int:String]()

		for (unitName, units) in unitsData {
			for unit in units {
				var abilities = [swgoh.Unit.Ability]()
				for zeta in unit.zetas {
					let ability = swgoh.Unit.Ability(id: zeta.id, name: zeta.id, maxTier: 8, isOmega: false, isZeta: true, hasOmegaZeta: true)
					abilities.append(ability)
				}
				
				let friendlyName = Character.find(unitName).name

				// record the allyCode -> player name mapping
				playerMap[unit.allyCode] = unit.player

				let newUnit = swgoh.Unit(id: unitName, name: friendlyName, gearLevel: unit.gearLevel, level: unit.level, power: unit.gp, stars: unit.starLevel, abilities: abilities)
				playerUnits[unit.allyCode, default: [:]][unitName] = newUnit
			}
		}
		
		var players = [swgoh.Player]()
		for (allyCode, name) in playerMap {
			let units = playerUnits[allyCode, default:[:]]
			let gp = units.values.reduce(0) { $0 + $1.power }
			let newPlayer = swgoh.Player(name: name, allyCode: allyCode, gp: gp, units: units)
			players.append(newPlayer)
		}
		
		let guildGp = players.reduce(0) { $0 + $1.stats.gp }
		let guildInfo = GuildInfo(gp: guildGp, id: 0, memberCount: players.count, name: guildName ?? "Guild", profileCount: 0, rank: 0)
		let guildFile = GuildFile(guild: guildInfo, players: players)
		
		return guildFile
	}
	
	struct Zeta : Decodable {
		let id: String
	}
	
	struct Unit : Decodable {
		let gp: Int
		let gearLevel: Int
		let starLevel: Int
		let player: String
		let allyCode: Int
		let level: Int
		let zetas: [Zeta]
	}
	
	struct Player : Decodable {
		let name: String
		let gp: Int
		let allyCode: Int
	}
	
	struct Guild : Decodable {
		let name: String
		let gp: Int
		let roster: [Player]
	}
	
}
