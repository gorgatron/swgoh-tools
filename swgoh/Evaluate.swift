//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

struct Squad : CustomStringConvertible, Hashable, Codable {

	let units: [Unit]
	let gp: Int
	var properties: Properties

	// memoize this -- it is used for hashing and comparison
	private let unitIdsMinusLeader: [String]

    struct Properties : Codable {
		let name: String?
		let preference: Int
		let unitMinimumGP: Int
		let rules: [String:UnitEvaluator]?
		let squadRules: [SquadEvaluationRule]?

		init(name: String? = nil, preference: Int = 0, unitMinimumGP: Int = 0, rules: [String:UnitEvaluator]? = nil, squadRules: [SquadEvaluationRule]? = nil) {
			self.name = name
			self.preference = preference
			self.unitMinimumGP = unitMinimumGP
			self.rules = UnitEvaluator.canonicalize(rules)
			self.squadRules = squadRules
		}
		
		enum CodingKeys: String, CodingKey {
			case name
			case preference
		}

		init(from decoder: Decoder) throws {
			let values = try decoder.container(keyedBy: CodingKeys.self)
			
			self.name = try values.decode(String.self, forKey: .name)
			self.preference = try values.decode(Int.self, forKey: .preference)
			self.unitMinimumGP = 0
            self.rules = nil
            self.squadRules = nil
		}
	}
	
	init(_ properties: Properties? = nil, units: [Unit]) {
		self.properties = properties ?? Properties()
		self.units = units
		self.gp = units.reduce(0) { $0 + $1.power }
		self.unitIdsMinusLeader = units.dropFirst().map { $0.id }.sorted()
	}
		
	var name: String? {
		return properties.name
	}
	
	var nameOrDefault: String {
		return properties.name ?? "Squad"
	}
	
	var description: String {
		return units.map { $0.commonName }.joined(separator: ", ")
	}
	
	func hash(into hasher: inout Hasher) {
		// the lead is considered separate
		hasher.combine(units[0].id)
		
		// combine the rest in a canonical order
		hasher.combine(unitIdsMinusLeader)
	}

	static func == (lhs: Squad, rhs: Squad) -> Bool {
		if lhs.units.count == 0 || rhs.units.count == 0 {
			// e.g. an Any Squad from TW
			return false
		}
		if lhs.units[0] == rhs.units[0] {
			return lhs.unitIdsMinusLeader == rhs.unitIdsMinusLeader
		}
		return false
	}
	
}

enum Status : Int, Comparable, Codable {
	
	case ok = 100
	case needsTuning = 90
	case needsGear = 75
	case needsRequiredGear = 60
	case needsRequired = 50
	case needsFarming = 25
	case needsUnlock = 0
	case unknown = -1
	
	var isViable: Bool {
		switch self {
		case .ok, .needsTuning, .needsGear, .needsRequiredGear:
			return true
		default:
			return false
		}
	}
	
	var viability: String {
		switch self {
		case .ok: return ":white_check_mark:"
		case .needsTuning: return ""
		case .needsGear: return ":small_orange_diamond:"
		case .needsRequiredGear: return ":gear:"
		case .needsRequired: return ":point_left:"
		case .needsFarming: return ":octagonal_sign:"
		case .needsUnlock: return ":octagonal_sign::closed_lock_with_key:"
		case .unknown: return "???"
		}
	}
	
	var marker: String {
		switch self {
		case .ok: return ""
		case .needsTuning: return ""
		case .needsGear: return ":small_orange_diamond:"
		case .needsRequiredGear: return ":gear:"
		case .needsRequired: return ":point_left:"
		case .needsFarming: return ":octagonal_sign:"
		case .needsUnlock: return ":octagonal_sign::closed_lock_with_key:"
		case .unknown: return "???"
		}
	}
	
	var unicodeMarker: String {
		switch self {
		case .ok: return ""
		case .needsTuning: return ""
		case .needsGear: return "🔸"
		case .needsRequiredGear: return "⚙️"
		case .needsRequired: return "👈"
		case .needsFarming: return "🛑"
		case .needsUnlock: return "🛑"
		case .unknown: return "???"
		}
	}

	static func < (lhs: Status, rhs: Status) -> Bool {
		return lhs.rawValue < rhs.rawValue
	}

}

/// The result of a SquadEvaluator.  This answers questions
/// like: is this squad workable?  What problems does it have?
struct SquadEvaluation : CustomStringConvertible, Equatable, Codable {
	
	let squad: Squad
	let evaluations: [String:EvaluationSummary]
	let squadEvaluations: EvaluationSummary
	
	var scorePercent: Double {
		return Double(score) / Double(max)
	}

	var score: Int {
		return evaluations.values.reduce(0) { $0 + $1.score } + squadEvaluations.score
	}
	
	var max: Int {
		return evaluations.values.reduce(0) { $0 + $1.max } + squadEvaluations.max
	}
	
	var preference: Int {
		return evaluations.values.reduce(0) { $0 + $1.preference } + squad.properties.preference + squadEvaluations.preference
	}
	
	var status: Status {
		return min(evaluations.values.reduce(Status.ok) { min($0, $1.status) }, squadEvaluations.status)
	}
	
	var viabilityScore: Int {
		return evaluations.values.reduce(0) { $0 + $1.status.rawValue } + squadEvaluations.status.rawValue
	}
	
	var unitEvaluations: [(Unit, EvaluationSummary)] {
		return squad.units.map { ($0, evaluations[$0.id]!) }
	}
	
	var description: String {
        let unitResult = unitEvaluations.map { (unit, e) -> String in
            return "\(unit.commonName) \(e.status.viability)"
        }.joined(separator: ", ")
		return "\(unitResult): status: \(status) viabilityScore: \(viabilityScore) preference: \(preference) score: \(scorePercent)"
	}
	
	static func == (lhs: SquadEvaluation, rhs: SquadEvaluation) -> Bool {
		return lhs.squad == rhs.squad
	}

}

/// The result of a UnitEvaluator (typically part of a SquadEvaluation).
struct EvaluationSummary : Codable {
	
	var status = Status.ok
	var score = 0
	var max = 0
	var preference = 0
	
	var messages = [Message]()
	
	mutating func add(message: Message) {
		score += message.score
		max += message.max
		preference += message.preference
		
		if message.status < status {
			status = message.status
		}
		
		messages.append(message)
	}
}

/// A single message with information about the Unit.  Messages that are Status.ok typically don't need to be printed.  The rest contain some information about why the Unit is not perfect.
struct Message : Comparable, Codable {
	let message: String
	let status: Status
	let score: Int
	let max: Int
	
	/// a way to mark a unit as preferred (positive) or not recommended (negative)
	let preference: Int
	
	init(message: String, status: Status, score: Int, max: Int, preference: Int = 0) {
		self.message = message
		self.status = status
		self.score = score
		self.max = max
		self.preference = preference
	}
	
	static func < (lhs: Message, rhs: Message) -> Bool {
		if lhs.status == rhs.status {
			let ls = Double(lhs.score) / Double(lhs.max == 0 ? 1 : lhs.max)
			let rs = Double(rhs.score) / Double(rhs.max == 0 ? 1 : lhs.max)
			return ls < rs
		} else {
			return lhs.status < rhs.status
		}
	}
	

}

struct UnitEvaluator {
	
	var rules: [UnitEvaluationRule]
	
	init(_ rules: UnitEvaluationRule...) {
		self.rules = rules
	}
	
	mutating func add(_ rule: UnitEvaluationRule) {
		rules.append(rule)
	}
	
	func evaluate(unit: Unit) -> EvaluationSummary {
		var evaluation = EvaluationSummary()
		
		for rule in rules {
			let messages = rule.evaluate(unit: unit)
			for message in messages {
				evaluation.add(message: message)
			}
		}

		// special case for not unlocked -- we collect all the messages above for the scoring
		if unit.stars == 0 {
			evaluation.messages.removeAll()
			evaluation.add(message: Message(message: "not unlocked", status: .needsUnlock, score: 0, max: 0))
		}
		
		return evaluation
	}
	
	/// converts rules from [ name : evaluator ] -> [ character_id : evaluator ]
	static func canonicalize(_ rules: [String:UnitEvaluator]?) -> [String:UnitEvaluator]? {
		guard let rules = rules else {
			return nil
		}
		var result = [String:UnitEvaluator]()
		
		for (id, evaluator) in rules {
			if id == "DEFAULT" {
				result[id] = evaluator
			} else {
				let character = Character.find(id)
				result[character.id] = evaluator
			}
		}
		
		return result
	}
}

struct SquadEvaluator {
	
	let rules: [String:UnitEvaluator]
	
	init(rules: [String:UnitEvaluator]) {
		self.rules = rules
	}
	
	func evaluate(squad: Squad) -> SquadEvaluation {
		var evaluations = [String:EvaluationSummary]()
		for unit in squad.units {
			let rules = squad.properties.rules ?? self.rules
			guard let evaluator = rules[unit.id] ?? rules["DEFAULT"] else {
				fatalError("no evaluation rule for \(unit.id)")
			}
			
			let unitEvaluation = evaluator.evaluate(unit: unit)
			evaluations[unit.id] = unitEvaluation
		}
		
		var squadEvaluations = EvaluationSummary()
		if let squadRules = squad.properties.squadRules {
			for squadRule in squadRules {
				for message in squadRule.evaluate(squad: squad) {
					squadEvaluations.add(message: message)
				}
			}
		}
		
		let squadEvaluation = SquadEvaluation(squad: squad, evaluations: evaluations, squadEvaluations: squadEvaluations)
		return squadEvaluation
	}
	
	func evaluate(squads: [Squad]) -> [SquadEvaluation] {
		var squadEvaluations = [SquadEvaluation]()
		
		for squad in squads {
			squadEvaluations.append(evaluate(squad: squad))
		}
		
		return squadEvaluations
	}
		
}

// MARK: - Rules

protocol UnitEvaluationRule {
	
	func evaluate(unit: Unit) -> [Message]
}

protocol SquadEvaluationRule {
	
	func evaluate(squad: Squad) -> [Message]
}

struct ComboRule : UnitEvaluationRule {
	
	let rules: [UnitEvaluationRule]
	
	func evaluate(unit: Unit) -> [Message] {
		return rules.flatMap { $0.evaluate(unit: unit) }
	}
	
}

protocol AtLeastEvaluationRule : UnitEvaluationRule {

    var atLeast: Int { get }
    var target: Int { get }
    var weight: Int { get }
    var status: Status { get }

}

extension AtLeastEvaluationRule {
    
    func evaluate(value: Int, name: String) -> [Message] {
        if value < atLeast {
            let text = "\(name) is \(value)/\(target)"
            return [Message(message: text, status: status, score: 0, max: weight)]
        } else if value < target {
			let text = "\(name) is \(value)/\(target)"
            let score = Int(Double(value - atLeast) / Double(target - atLeast) * Double(weight))
            return [Message(message: text, status: .needsTuning, score: score, max: weight)]
        } else {
            return [Message(message: "\(name) OK", status: .ok, score: weight, max: weight)]
        }
    }

}

struct StatRule : AtLeastEvaluationRule {
	
	let key: Unit.Stats.CodingKeys
	let atLeast: Int
	let target: Int
	let weight: Int
	let status: Status
	
	init(_ key: Unit.Stats.CodingKeys, atLeast: Int, target: Int, weight: Int = 100, status: Status = .needsGear) {
		self.key = key
		self.atLeast = atLeast
		self.target = target
		self.weight = weight
		self.status = status
	}
	
	func evaluate(unit: Unit) -> [Message] {
		let value = unit.stats.value(key)
		if unit.hasStats {
        	return evaluate(value: value, name: key.name)
		} else {
			// unable to evaluate, e.g. from swgoh.help
			return []
		}
	}
	
}

struct UnitRule : AtLeastEvaluationRule {
	
	let key: Unit.CodingKeys
	let atLeast: Int
	let target: Int
	let weight: Int
	let status: Status
	
	init(_ key: Unit.CodingKeys, atLeast: Int, target: Int, weight: Int = 100, status: Status = .needsFarming) {
		self.key = key
		self.atLeast = atLeast
		self.target = target
		self.weight = weight
		self.status = status
	}
	
	func evaluate(unit: Unit) -> [Message] {
		let value = unit.value(key)
        return evaluate(value: value, name: key.name)
	}

	/// custom rule for this -- hard stop at G7 and below.
	static let gear10PlusRule = CustomRule { (unit) -> Message in
		switch unit.gearLevel {
		case 0 ... 7:
			return Message(message: "gear level must be at least 8", status: .needsRequired, score: 0, max: 400)
		case 8 ... 9:
			return Message(message: "should be G10 (G\(unit.gearLevel))", status: .needsRequiredGear, score: 100, max: 400)
		case 10:
			return Message(message: "gear G10/G12", status: .needsTuning, score: 200, max: 400)
		case 11:
			return Message(message: "gear G11/G12", status: .needsTuning, score: 300, max: 400)
		case 12:
			return Message(message: "G12", status: .ok, score: 400, max: 400)
		default:
			return Message(message: "unknown gear level", status: .needsRequired, score: 0, max: 400)
		}
	}
	
	static let gear9PlusRule = CustomRule { (unit) -> Message in
		switch unit.gearLevel {
		case 0 ... 6:
			return Message(message: "gear level must be at least 7", status: .needsRequired, score: 0, max: 400)
		case 7 ... 8:
			return Message(message: "should be G9 (G\(unit.gearLevel))", status: .needsRequiredGear, score: 100, max: 400)
		case 9:
			return Message(message: "gear G10/G12", status: .needsTuning, score: 150, max: 400)
		case 10:
			return Message(message: "gear G10/G12", status: .needsTuning, score: 200, max: 400)
		case 11:
			return Message(message: "gear G11/G12", status: .needsTuning, score: 300, max: 400)
		case 12:
			return Message(message: "G12", status: .ok, score: 400, max: 400)
		default:
			return Message(message: "unknown gear level", status: .needsRequired, score: 0, max: 400)
		}
	}
	
	static let level85Rule = UnitRule(.level, atLeast: 85, target: 85, weight: 200, status: .needsTuning)
	static let sevenStarRule = UnitRule(.stars, atLeast: 7, target: 7, weight: 200, status: .needsFarming)

}

struct AllOmegasRule : UnitEvaluationRule {
	
	let weight: Int
	let status: Status
	
	init(weight: Int = 100, status: Status = .needsGear) {
		self.weight = weight
		self.status = status
	}
	
	func evaluate(unit: Unit) -> [Message] {
		var missingAbilities = [Unit.Ability]()
		var totalOmegas = 0
		for ability in unit.abilities {
			if ability.isOmega {
				totalOmegas += 1
				if !ability.hasOmegaZeta {
					missingAbilities.append(ability)
				}
			}
		}
		if missingAbilities.isEmpty {
			return [Message(message: "all omegas present", status: .ok, score: totalOmegas * weight, max: totalOmegas * weight)]
		} else {
			return [Message(message: "missing omegas: \(missingAbilities.map { $0.name }.joined(separator: ", "))", status: status, score: (totalOmegas - missingAbilities.count) * weight, max: totalOmegas * weight)]
		}
	}
}

struct OmegaZetaAbilityRule : UnitEvaluationRule {
	
	let id: String
	let weight: Int
	let status: Status
	
	init(_ id: String, weight: Int = 100, status: Status = .needsRequired) {
		self.id = id
		self.weight = weight
		self.status = status
	}
	
	func evaluate(unit: Unit) -> [Message] {
		var result = [Message]()
		for ability in unit.abilities {
			if ability.id == id {
				if ability.hasOmegaZeta {
					result.append(Message(message: "\(ability.name) has \(ability.isZeta ? "zeta" : "omega")", status: .ok, score: weight, max: weight))
				} else {
					result.append(Message(message: "\(ability.name) missing \(ability.isZeta ? "zeta" : "omega")", status: status, score: 0, max: weight))
				}
			}
		}
		return result
	}
}

struct PreferenceRule : UnitEvaluationRule {
	
	let preference: Int
	
	/// only apply the preference if the unit is 7 stars, e.g. it is a very difficult unlock (legendaries)
	let ifSevenStar: Bool
	
	init(preference: Int = 1, ifSevenStar: Bool = false) {
		self.preference = preference
		self.ifSevenStar = ifSevenStar
	}
	
	func evaluate(unit: Unit) -> [Message] {
		if ifSevenStar && unit.stars != 7 {
			return []
		}
		return [ Message(message: "unit boost", status: .ok, score: 0, max: 0, preference: preference) ]
	}
	
}

struct CustomRule : UnitEvaluationRule {
	
	let rule: (Unit) -> Message
	
	func evaluate(unit: Unit) -> [Message] {
		return [ rule(unit) ]
	}
	
}

struct CustomSquadRule : SquadEvaluationRule {
	
	let rule: (Squad) -> Message
	
	func evaluate(squad: Squad) -> [Message] {
		return [ rule(squad) ]
	}
	
}

// MARK: - TW

struct TWRule : UnitEvaluationRule {
	
	let requiresLeaderZeta: Bool
	let requiresUniqueZetas: Bool
	let minimumStars: Int
	let minimumGear: Int
	let preference: Int
	
	/// required gear level to get the preference boost
	let gearPreferenceLevel: Int
	
	init(leaderZeta: Bool = false, uniqueZetas: Bool = false, stars: Int = 7, gear: Int = 8, preference: Int = 0, gearPreference: Int = 0) {
		self.requiresLeaderZeta = leaderZeta
		self.requiresUniqueZetas = uniqueZetas
		self.minimumStars = stars
		self.minimumGear = gear
		self.preference = preference
		self.gearPreferenceLevel = gearPreference
	}
	
	func score(_ value: Int, _ min: Int, _ max: Int, _ scale: Int) -> Int {
		if min == max {
			return scale
			
		} else {
			let numerator = Double(value - min)
			let denominator = Double(max - min)
			
			return Int((numerator / denominator) * Double(scale))
		}
	}
	
	func evaluate(unit: Unit) -> [Message] {
		let hasLeaderZeta = unit.zetas.contains { $0.starts(with: "leaderskill") }
		let hasUniqueZeta = unit.zetas.contains { $0.starts(with: "uniqueskill") }
		
		if (requiresLeaderZeta && !hasLeaderZeta) || (requiresUniqueZetas && !hasUniqueZeta) {
			return [ Message(message: "missing zetas", status: .needsRequired, score: 0, max: 1000) ]
		}
		
		if unit.level < 70 {
			return [ Message(message: "low level", status: .needsRequired, score: 0, max: 1000) ]
		}
		
		if unit.stars < minimumStars {
			return [ Message(message: "low stars", status: .needsRequired, score: 0, max: 1000) ]
		}
		
		if unit.gearLevel < minimumGear {
			return [ Message(message: "low stars", status: .needsRequired, score: 0, max: 1000) ]
		}
		
		let s = score(unit.stars, minimumStars, 7, 300) + score(unit.gearLevel, minimumGear, 12, 700)
		let preference = unit.gearLevel > gearPreferenceLevel ? self.preference : 0
		
		return [ Message(message: "ok", status: .ok, score: s, max: 1000, preference: preference) ]
	}
	
	static func table(_ namesAndPreference: (String, Int)...) -> [String:UnitEvaluator] {
		var result = [String:UnitEvaluator]()
		
		for (name, preference) in namesAndPreference {
			result[name] = TWRule(preference: preference).e
		}
		
		return result
	}
	
	var e: UnitEvaluator {
		return UnitEvaluator(self)
	}
}
