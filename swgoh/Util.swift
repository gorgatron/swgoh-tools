//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
	
	func removing(_ value: Element) -> [Element] {
		var copy = self
		copy.removeAll { $0 == value }
		return copy
	}
	
	func removing(_ values: Element...) -> [Element] {
		var copy = self
		copy.removeAll { values.contains($0) }
		return copy
	}
	
	func removing(_ values: [Element]) -> [Element] {
		var copy = self
		copy.removeAll { values.contains($0) }
		return copy
	}
	
	func adding(_ values: Element...) -> [Element] {
		var copy = self
		copy.append(contentsOf: values)
		return copy
	}
	
	func adding(_ values: [Element]) -> [Element] {
		var copy = self
		copy.append(contentsOf: values)
		return copy
	}

}

extension Dictionary {
	
	func union(_ other: [Key:Value]) -> [Key:Value] {
		return self.merging(other, uniquingKeysWith: { v1, v2 in v1 })
	}
	
}

private var defaultRandomNumberGenerator = SystemRandomNumberGenerator()

struct CountedSet<Element> where Element: Hashable {
	
	var contents: [Element:Int]
	var count: Int
	
	init() {
		contents = [:]
		count = 0
	}
	
	init(keysAndValues: [(Element, Int)]) {
		contents = Dictionary(uniqueKeysWithValues: keysAndValues)
		count = contents.reduce(0, { $0 + $1.value })
	}
	
	init(contents: [Element:Int]) {
		self.contents = contents
		count = contents.reduce(0, { $0 + $1.value })
	}
	
	init(items: [Element]) {
		contents = [:]
		count = 0
		for item in items {
			add(item)
		}
	}
	
	var isEmpty: Bool {
		return count == 0
	}
	
	mutating func remove(_ item: Element, count: Int = 1) {
		if contents[item, default:0] < count {
			fatalError("unable to remove \(item) -- not enough present")
		}
		
		contents[item, default:0] -= count
		self.count -= count
	}
	
	mutating func add(_ item: Element, count: Int = 1) {
		contents[item, default:0] += count
		self.count += count
	}
	
	func anyElement() -> Element {
		return anyElement(using: &defaultRandomNumberGenerator)
	}
	
	func anyElement<G: RandomNumberGenerator>(using generator: inout G) -> Element {
		var i = Int.random(in: 0 ..< count, using: &generator)
		for (key, count) in contents {
			if i < count {
				return key
			} else {
				i -= count
			}
		}
		fatalError("anyElement failed")
	}
	
	var asArray: [Element] {
		var result = [Element]()
		
		for (key, count) in contents {
			for _ in 0 ..< count {
				result.append(key)
			}
		}

		return result
	}
	
	func randomSubset(count: Int) -> CountedSet<Element> {
		return randomSubset(count: count, using: &defaultRandomNumberGenerator)
	}
	
	func randomSubset<G: RandomNumberGenerator>(count: Int, using generator: inout G) -> CountedSet<Element> {
		if count > self.count {
			fatalError("asking for too many elements")
		}
		
		if count == self.count {
			return self
		}
		
		if count < (self.count - count) {
			// faster to build it up
			var result = CountedSet<Element>()
			
			for _ in 0 ..< count {
				let item = self.anyElement(using: &generator)
				result.add(item)
			}
			
			return result
			
		} else {
			// faster to cut it down
			var result = self
			
			for _ in 0 ..< (self.count - count) {
				let item = result.anyElement(using: &generator)
				result.remove(item)
			}
			
			return result
		}
	}
	
	func subset(_ keys:[Element]) -> CountedSet<Element> {
		return CountedSet(contents: contents.filter { keys.contains($0.0) })
	}
	
	subscript(_ key: Element) -> Int {
		return contents[key, default:0]
	}
	
	func subtracting(_ other:CountedSet<Element>) -> CountedSet<Element> {
		var result = self
		
		for (key, value) in other.contents {
			result.remove(key, count: value)
		}
		
		return result
	}
}

extension CountedSet : Codable where Element : Codable {
	
}
