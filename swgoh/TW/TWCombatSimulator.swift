import Foundation

struct TWCounter {
	let team: String
	let counter: String
	let effectiveness: Double
	let preference: Int
	
	init(team: String, counter: String, effectiveness: Double = 0.95, preference: Int = 1) {
		self.team = team
		self.counter = counter
		self.effectiveness = effectiveness
		self.preference = preference
	}
	
	static func chanceOfWin(defense:Squad, offense:Squad) -> Double {
		if let counter = TWCounter.counterMap[defense.nameOrDefault]?[offense.nameOrDefault] {
			let effectiveOffenseGP = counter.effectiveness * Double(offense.gp)
			switch effectiveOffenseGP / Double(defense.gp) {
			case 0.0 ..< 0.5:
				return 0.1
			case 0.5 ..< 0.75:
				return 0.2
			case 0.75 ..< 0.85:
				return 0.3
			case 0.85 ..< 1.0:
				return 0.4
			case 1.0 ..< 1.15:
				return 0.95
			case 1.15 ..< 1.3:
				return 0.975
			case 1.3 ..< 10.0:
				return 0.99
			default:
				return 1.0
			}
		} else {
			return 0.05
		}
	}
	
	static func compare(defense:Squad, offense1:Squad, offense2:Squad) -> Bool {
		let gp1 = offense1.gp
		let gp2 = offense2.gp
		
		if offense1.nameOrDefault == offense2.nameOrDefault {
			return gp1 < gp2
		}
		
		let preference1 = TWCounter.counterMap[defense.nameOrDefault]?[offense1.nameOrDefault]?.preference ?? -1
		let preference2 = TWCounter.counterMap[defense.nameOrDefault]?[offense2.nameOrDefault]?.preference ?? -1

		let chance1 = chanceOfWin(defense: defense, offense: offense1)
		let chance2 = chanceOfWin(defense: defense, offense: offense2)
		
		// honor preference first, if something is viable

		if preference1 > preference2 {
			if chance1 > 0.5 {
				return true
			}
		}

		if preference2 > preference1 {
			if chance2 > 0.5 {
				return false
			}
		}
		
		// then prefer the weaker team if it has a better chance
		if gp1 < gp2 && chance1 >= chance2 {
			return true
		}
		if gp1 > gp2 && chance1 <= chance2 {
			return false
		}
		
		// finally go by chance
		if chance1 == chance2 {
			return gp1 < gp2
		} else {
			return chance1 > chance2
		}
	}

	// altername names to register
	static let aliases = [
		"NS" : [ "NestSisters" ],
	]
	
	// generated from my counters spreadsheet.  based on https://wiki.swgoh.help/wiki/Territory_War_and_Grand_Arena_Counter_List

	// these teams have a chance of losing a battle for free
	static let freeWinTeams: Set = [ "Phoenix" ]
	
	static let counters = [
		TWCounter(team: "BH", counter: "BS"),
		TWCounter(team: "BH", counter: "CLS"),
		TWCounter(team: "BH", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "BH", counter: "FO"),
		TWCounter(team: "BH", counter: "JTR", effectiveness: 1.25, preference: 10),
		TWCounter(team: "BH", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "BH", counter: "Troopers"),
		TWCounter(team: "BS", counter: "BH"),
		TWCounter(team: "BS", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "BS", counter: "FO"),
		TWCounter(team: "BS", counter: "JTR"),
		TWCounter(team: "BS", counter: "NS"),
		TWCounter(team: "BS", counter: "Qira"),
		TWCounter(team: "BS", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "BS", counter: "Troopers"),
		TWCounter(team: "CLS", counter: "BS"),
		TWCounter(team: "CLS", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "CLS", counter: "zEP"),
		TWCounter(team: "CLS", counter: "GK"),
		TWCounter(team: "CLS", counter: "JTR"),
		TWCounter(team: "CLS", counter: "NS"),
		TWCounter(team: "CLS", counter: "Nute"),
		TWCounter(team: "CLS", counter: "Revan", effectiveness: 0.9, preference: 0),
		TWCounter(team: "CLS", counter: "Rex"),
		TWCounter(team: "DR", counter: "CLS"),
		TWCounter(team: "DR", counter: "DR", effectiveness: 1, preference: 2),
		TWCounter(team: "DR", counter: "NS"),
		TWCounter(team: "DR", counter: "Revan", effectiveness: 0.9, preference: 2),
		TWCounter(team: "zEP", counter: "BS"),
		TWCounter(team: "zEP", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "zEP", counter: "JTR"),
		TWCounter(team: "zEP", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "zEP", counter: "Rex"),
		TWCounter(team: "zEP", counter: "Troopers"),
		TWCounter(team: "Ewoks", counter: "BH"),
		TWCounter(team: "Ewoks", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "Ewoks", counter: "GK"),
		TWCounter(team: "Ewoks", counter: "zMaul"),
		TWCounter(team: "Ewoks", counter: "NS"),
		TWCounter(team: "Ewoks", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "Ewoks", counter: "Rex"),
		TWCounter(team: "Ewoks", counter: "Wampa"),
		TWCounter(team: "Ewoks", counter: "Zader"),
		TWCounter(team: "Ewoks", counter: "JTR", effectiveness: 1.3, preference: 0),
		TWCounter(team: "FO", counter: "CLS"),
		TWCounter(team: "FO", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "FO", counter: "Ewoks", effectiveness: 1.3, preference: 2),
		TWCounter(team: "FO", counter: "Nute"),
		TWCounter(team: "FO", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "FO", counter: "Troopers"),
		TWCounter(team: "FO", counter: "Wampa"),
		TWCounter(team: "FO", counter: "Wiggs"),
		TWCounter(team: "GK", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "GK", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "GK", counter: "NS", effectiveness: 1.1),
		TWCounter(team: "GK", counter: "Traya"),
		TWCounter(team: "GK", counter: "Wampa"),
		TWCounter(team: "GK", counter: "Zader"),
		TWCounter(team: "GK", counter: "NS", effectiveness: 1.1),
		TWCounter(team: "NS", counter: "BH"),
		TWCounter(team: "NS", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "NS", counter: "zEP"),
		TWCounter(team: "NS", counter: "FO"),
		TWCounter(team: "NS", counter: "Jango"),
		TWCounter(team: "NS", counter: "zMaul"),
		TWCounter(team: "NS", counter: "Phoenix", effectiveness: 0.75),
		TWCounter(team: "NS", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "NS", counter: "Troopers", effectiveness: 1.1, preference: 3),
		TWCounter(team: "Phoenix", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "Phoenix", counter: "Ewoks", effectiveness: 1.3, preference: 2),
		TWCounter(team: "Phoenix", counter: "zMaul"),
		TWCounter(team: "Phoenix", counter: "Nute"),
		TWCounter(team: "Phoenix", counter: "NS"),
		TWCounter(team: "Phoenix", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "Phoenix", counter: "Wampa"),
		TWCounter(team: "Phoenix", counter: "Wiggs"),
		TWCounter(team: "Phoenix", counter: "Zody"),
		TWCounter(team: "Phoenix", counter: "Zader"),
		TWCounter(team: "Phoenix", counter: "zEP"),
		TWCounter(team: "R1", counter: "BH"),
		TWCounter(team: "R1", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "R1", counter: "Jango"),
		TWCounter(team: "R1", counter: "QGJ"),
		TWCounter(team: "R1", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "R1", counter: "Troopers"),
		TWCounter(team: "R1", counter: "Wampa"),
		TWCounter(team: "R1", counter: "Zody"),
		TWCounter(team: "Revan", counter: "CLS"),
		TWCounter(team: "Revan", counter: "DR", effectiveness: 1.5, preference: 2),
		TWCounter(team: "Revan", counter: "zEP"),
		TWCounter(team: "Revan", counter: "JTR"),
		TWCounter(team: "Revan", counter: "NS"),
		TWCounter(team: "Revan", counter: "Revan", effectiveness: 1, preference: 2),
		TWCounter(team: "Revan", counter: "Traya"),
		TWCounter(team: "Traya", counter: "BS"),
		TWCounter(team: "Traya", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "Traya", counter: "Ewoks"),
		TWCounter(team: "Traya", counter: "FO"),
		TWCounter(team: "Traya", counter: "IPD"),
		TWCounter(team: "Traya", counter: "Jango"),
		TWCounter(team: "Traya", counter: "JTR"),
		TWCounter(team: "Traya", counter: "Magma"),
		TWCounter(team: "Traya", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "Traya", counter: "Wiggs"),
		TWCounter(team: "Troopers", counter: "BS"),
		TWCounter(team: "Troopers", counter: "CLS"),
		TWCounter(team: "Troopers", counter: "DR", effectiveness: 1.5, preference: 0),
		TWCounter(team: "Troopers", counter: "zEP"),
		TWCounter(team: "Troopers", counter: "Jango"),
		TWCounter(team: "Troopers", counter: "zMaul"),
		TWCounter(team: "Troopers", counter: "Revan", effectiveness: 1.3, preference: 0),
		TWCounter(team: "Troopers", counter: "Rex"),
		TWCounter(team: "Troopers", counter: "Troopers", effectiveness: 0.95),
		TWCounter(team: "Wiggs-D", counter: "BH"),
		TWCounter(team: "Wiggs-D", counter: "Troopers"),
		TWCounter(team: "Wiggs-D", counter: "QGJ"),
		TWCounter(team: "Wiggs-D", counter: "Wampa"),
		TWCounter(team: "Wiggs-D", counter: "JTR"),
		TWCounter(team: "Wiggs-D", counter: "DR"),
		TWCounter(team: "Wiggs-D", counter: "Revan"),
		TWCounter(team: "Wiggs-D", counter: "Traya"),
	]
	
	/// Map of TeamName : [CounterName : CounterInfo]
	static let counterMap: [String:[String:TWCounter]] = {
		var result = [String:[String:TWCounter]]()
		for item in TWCounter.counters {
			result[item.team, default:[:]][item.counter] = item
			
			for alias in TWCounter.aliases[item.team, default:[]] {
				result[alias, default:[:]][item.counter] = item
			}
		}
		return result
	}()
	
	/// Map of CounterName : [TeamName : CounterInfo] -- the reverse counters.  Given a squad, what are the other teams that counter it
	static let reverseCounterMap: [String:[String:TWCounter]] = {
		var result = [String:[String:TWCounter]]()
		for item in TWCounter.counters {
			result[item.counter, default:[:]][item.team] = item
			
			for alias in TWCounter.aliases[item.counter, default:[]] {
				result[alias, default:[:]][item.team] = item
			}
		}
		return result
	}()
}

struct TWCombatSimulator {
	
	let defense: [String:[Squad]]
	let offense: [Squad]
	
	init(defense: [String:[Squad]], offense: [Squad]) {
		self.defense = defense
		self.offense = offense
	}
	
	init(defenseGuild: [Player : [SquadEvaluation]], offenseGuild: [Player : [SquadEvaluation]]) {
		
		// come up with the defensive placements for the defenseGuild
		let defenseTuples = defaultTWScheduler().schedule(data: defenseGuild, config: TW.config)
		var defense = [String:[Squad]]()
		for item in defenseTuples {
			defense[item.2, default: []].append(item.1.squad)
		}
		
		// figure out what squads the offense might put on defense
		let offense = TWCombatSimulator.prepareOffense(offenseGuild: offenseGuild)
		
		self.init(defense: defense, offense: offense)
	}
	
	static func prepareOffense(offenseGuild: [Player : [SquadEvaluation]], scheduler: TWSectionScheduler = defaultTWScheduler(), config: TWConfig = OptimizedConfig()) -> [Squad] {
		// figure out what squads the offense might put on defense
		let offenseTuples = scheduler.schedule(data: offenseGuild, config: config)
		
		// and copy out the offense squads, subtracting the ones that are scheduled for defense
		var offense = [Squad]()
		for (player, squadEvaluations) in offenseGuild {
			let squads = squadEvaluations.map { $0.squad }.filter { squad in
				return !offenseTuples.contains {
					$0.0 == player && $0.1.squad == squad
				}
			}
			offense.append(contentsOf: squads)
		}
		
		return offense
	}
	
	/// a count of teams by type for defense and offense
	func summary() -> String {
		var result = ""
		
		let allDefenseSquads = Dictionary(grouping: defense.values.flatMap { $0 }, by: { $0.nameOrDefault })
		let allOffenseSquads = Dictionary(grouping: offense, by: { $0.nameOrDefault })
		let allSquadNames = Set(allDefenseSquads.keys).union(allOffenseSquads.keys)
		
		for name in allSquadNames.sorted() {
			result += name
			result += ","
			result += allDefenseSquads[name, default:[]].count.description
			result += ","
			result += allOffenseSquads[name, default:[]].count.description
			result += "\n"
		}
		
		return result
	}
	
	/// a count of teams by type for defense and offense counters
	func counters() -> String {
		var result = ""
		
		let allDefenseSquads = Dictionary(grouping: defense.values.flatMap { $0 }, by: { $0.nameOrDefault })
		let allOffenseSquads = Dictionary(grouping: offense, by: { $0.nameOrDefault })
		
		for name in allDefenseSquads.keys.sorted() {
			result += name
			result += ","
			result += allDefenseSquads[name, default:[]].count.description
			result += ","
			
			for counter in TWCounter.counterMap[name, default:[:]].values {
				if let count = allOffenseSquads[counter.counter]?.count {
					result += counter.counter
					result += " "
					result += count.description
					result += " "
				}
			}
			result += "\n"
		}
		
		return result
	}
	
	static let SECTION_LOSS_THRESHOLD = 5
	
	struct SquadResult {
		let win: Bool
		let defender: Squad
		let attackers: [Squad]
		
		var score: Int {
			if win {
				switch attackers.count {
				case 0:
					// anonymous defender, lost on first attack
					return 20
				case 1:
					return 20
				case 2:
					return 15
				default:
					return 10
				}
			} else {
				return 0
			}
		}
	}
	
	struct SectionResult {
		let squadResults: [SquadResult]
		
		var score: (wins: Int, losses: Int, squadsUsed: Int, score: Int) {
			let wins = squadResults.filter { $0.win }.count
			let losses = squadResults.count - wins
			let squadsUsed = squadResults.reduce(0, { $0 + $1.attackers.count })
			let score = squadResults.reduce(0, { $0 + $1.score })
			
			return (wins, losses, squadsUsed, score)
		}
	}
	
	struct Result : Comparable {
		
		static func < (lhs: TWCombatSimulator.Result, rhs: TWCombatSimulator.Result) -> Bool {
			return lhs.score.score < rhs.score.score
		}
		
		static func == (lhs: TWCombatSimulator.Result, rhs: TWCombatSimulator.Result) -> Bool {
			return lhs.score.score == rhs.score.score
		}
		
		let plan: [String]
		let sections: [String:SectionResult]
		
		var score: (sectionsWon: Int, squadsUsed: Int, squadsDefended: Int, lastLoss: Int, lastSection: String, score: Int) {
			var score = 0
			var sectionsWon = 0
			var squadsUsed = 0
			var squadsDefended = 0
			var lastLoss = 0
			var lastSection = "CLEAR"
			
			for sectionName in plan {
				let section = sections[sectionName]!
				let sectionScore = section.score
				
				score += sectionScore.score
				
				squadsUsed += sectionScore.squadsUsed
				squadsDefended += section.squadResults.count
				
				if sectionScore.losses > SECTION_LOSS_THRESHOLD {
					// failed here
					lastLoss = sectionScore.losses
					lastSection = sectionName
					break
					
				} else {
					// beat the section
					score += sectionConquerScore(name: sectionName, result: section)
					sectionsWon += 1
				}
			}
			
			return (sectionsWon, squadsUsed, squadsDefended, lastLoss, lastSection, score)
		}
		
		func sectionConquerScore(name: String, result: SectionResult) -> Int {
			let base: Int
			switch name {
			case "T4", "B4":
				base = 900
			case "T2":
				// if they take T2 they have access to F1 and F2 -- let's assume they clear F1 and almost F2
				base = 450 + (result.squadResults.count) * (10 + 15) + 450 + (result.squadResults.count) * (10 + 15)

			default:
				base = 450
			}
			
			return base + (result.squadResults.count) * 10
		}
		
		var description: String {
			var result = ""
			
			let overallScore = self.score
			result += "score: \(overallScore.score), sections lost: \(overallScore.sectionsWon), squads used: \(overallScore.squadsUsed), last section: \(overallScore.lastSection), last section losses: \(overallScore.lastLoss)\n"
			
			for sectionName in plan {
				result += "**Section \(sectionName)** "

				let section = sections[sectionName]!
				let score = section.score
				
				result += " team used: \(score.squadsUsed) vs \(score.wins + score.losses)\n"

				if score.losses > SECTION_LOSS_THRESHOLD {
					result += ":octagonal_sign:\n"
				}
				result += "\n"
				
				for squadResult in section.squadResults.sorted(by: {
					let ac1 = $0.attackers.count
					let ac2 = $1.attackers.count
					
					if ac1 == ac2 {
						let gp1 = $0.defender.gp
						let gp2 = $1.defender.gp
						return gp1 <= gp2
					} else {
						return ac1 <= ac2
					}
				}) {
					if squadResult.defender.nameOrDefault.contains("Any Squad") {
						result += "Any Squad"
					} else {
						result += "\(squadResult.defender.nameOrDefault) (\(squadResult.defender.gp))"
					}
					if !squadResult.win {
						result += ":octagonal_sign:"
					}
					result += ": "
					if squadResult.win && squadResult.attackers.count == 0 {
						result += "misc team"
					} else {
						result += squadResult.attackers.map { "\($0.nameOrDefault) (\($0.gp))" }.joined(separator: ", ")
					}
					result += "\n"
				}
				
				result += "\n"
			}
			
			return result
		}
	}
	
	/// chance that we would have a win against an Any Squad without consuming an offense team
	let ANY_SQUAD_WIN_NO_OFFENSE = 0.5
	
	func simulate(defense:Squad, offense:inout [Squad]) -> SquadResult {
		let defenseSquadName = defense.nameOrDefault
		
		if defenseSquadName.contains("Any Squad") {
			// special rule for "Any Squad" -- we might win without using a squad
			if Double.random(in: 0 ..< 1) <= ANY_SQUAD_WIN_NO_OFFENSE {
				return SquadResult(win: true, defender: defense, attackers: [])
			}
			
			let min = offense.enumerated().filter { $0.element.gp >= 60000 }.min { $0.element.gp < $1.element.gp }
			if let min = min {
				offense.remove(at: min.offset)
				return SquadResult(win: true, defender: defense, attackers: [min.element])
			}
		
			// not able to beat it
			return SquadResult(win: false, defender: defense, attackers: [])
		}
		
		if TWCounter.freeWinTeams.contains(defenseSquadName) && Float.random(in: 0 ... 1) < 0.5 {
			return SquadResult(win: true, defender: defense, attackers: [])
		}
		
		// normal path
		let counterSquadNames = TWCounter.counterMap[defenseSquadName, default:[:]].keys
		
		// these are the counter squads
		var possibleOffense = offense.enumerated().filter { counterSquadNames.contains($0.element.nameOrDefault) }
		
		// see if the best (per our rules) one wins
		if let firstCounter = possibleOffense.min(by: { TWCounter.compare(defense: defense, offense1: $0.element, offense2: $1.element) }) {

			let chance = TWCounter.chanceOfWin(defense: defense, offense: firstCounter.element)
			if Double.random(in: 0 ..< 1) <= chance {
				// win!
				offense.remove(at: firstCounter.offset)

				return SquadResult(win:true, defender: defense, attackers: [firstCounter.element])
			}

			// didn't win, run the whole set
		}
		
		possibleOffense.sort { TWCounter.compare(defense: defense, offense1: $0.element, offense2: $1.element) }

//		print("squad \(defense.nameOrDefault) \(defense.gp): \(possibleOffense.map { $0.element.nameOrDefault + " " + $0.element.gp.description + " " + TWCounter.chanceOfWin(defense: defense, offense: $0.element).description}.joined(separator: ", "))")
		
		var usedSquads = [Squad]()
		var usedIndexes = [Int]()
		
		// the first one already lost, take care of it
		if !possibleOffense.isEmpty {
			let (index, squad) = possibleOffense.removeFirst()
			
			usedIndexes.append(index)
			usedSquads.append(squad)
		}
		
		while !possibleOffense.isEmpty {
			let (index, squad) = possibleOffense.removeFirst()
			
			usedIndexes.append(index)
			usedSquads.append(squad)
			
			let chance = TWCounter.chanceOfWin(defense: defense, offense: squad)
			if Double.random(in: 0 ..< 1) <= chance {
				break
			}
		}
		
		// remove the used squads (reverse index)
		usedIndexes.sort(by: >)
		for index in usedIndexes {
			offense.remove(at: index)
		}
		
		return SquadResult(win:!possibleOffense.isEmpty, defender: defense, attackers: usedSquads)
	}

	func simulate(defense:[Squad], offense:inout [Squad]) -> [SquadResult] {
		var result = [SquadResult]()
		for defenseSquad in defense {
			let battle = simulate(defense: defenseSquad, offense: &offense)
			result.append(battle)
		}
		return result
	}
	
	/// top level call
	func simulate(plan: [String]) -> Result {
		var sectionResults = [String:SectionResult]()
		var remainingOffense = offense
		for section in plan {
			let squadResults = simulate(defense: defense[section, default: []], offense: &remainingOffense)
			let sectionResult = SectionResult(squadResults: squadResults)
			sectionResults[section] = sectionResult
		}
		
		return Result(plan: plan, sections: sectionResults)
	}
}
