//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

struct TW3v3 {
	
	// MARK: - Squads
	
	// based on mcmole's graphic
	// https://cdn.discordapp.com/attachments/472985802356293634/536718417026220047/image0.jpg
	
	// and https://gaming-fans.com/2018/12/the-grand-arena-3v3/
	
	static let revanAdds = u("Bastila Shan", "Aayla", "GMY", "Ezra", "GK", "Jolee", "Juhani")
	static let carthAdds = u("Jolee", "Juhani", "Canderous", "Zaalbar", "Mission")
	static let qiraAdds = u("Nest", "Young Lando", "Vandor Chewbacca", "HANSOLO", "Young Han Solo", "CHEWBACCALEGENDARY", "Zaalbar", "Mission")
	static let bosskAdds = u("Boba Fett", "Greedo", "Cad Bane", "Dengar")
	static let jangoAdds = u("Aurra", "Greedo", "Cad Bane", "Zam")
	static let thrawnAdds = u("Krennic", "Death Trooper", "Magmatrooper", "Shoretrooper")
	static let palpAdds = u("Shoretrooper", "Tarkin", "Darth Vader", "BASTILASHANDARK", "Dooku")
	static let opressAdds = u("Dooku", "Sith Trooper", "Sith Marauder")
	static let trayaAdds = u("BASTILASHANDARK", "Sith Trooper", "Sion", "Nihilus")
	static let asajjAdds = u("Zombie", "Spirit", "Acolyte")
	static let kruAdds = u("First Order Stormtrooper", "First Order Executioner", "Kylo Ren", "First Order Officer")
	static let phasmaAdds = u("First Order Officer", "First Order Stormtrooper", "First Order Tie Pilot", "First Order SF TIE Pilot")

	static let builders: [SquadBuilder] = [
		SelectUnitSquadBuilder("QGJ", 15, rules, u("QGJ"), u("JKA"), u("Ahsoka", "Bastila Shan")),
		
		SelectUnitSquadBuilder("GK", 14, rules, u("GK"), u("Barriss"), u("Bastila Shan")),
		
		SelectUnitSquadBuilder("BS", 15, rules, u("Bastila Shan"), u("JKA"), u("Ahsoka", "GK")),
		SelectUnitSquadBuilder("BS", 15, rules, u("Bastila Shan"), u("JKA"), u("Old Ben", "Aayla")),

		SelectUnitSquadBuilder("Revan", 50, rules, u("Revan"), u("Bastila Shan"), u("Old Ben", "JKA")),
		SelectUnitSquadBuilder("Revan", 50, rules, u("Revan"), revanAdds, revanAdds),

		SelectUnitSquadBuilder("Carth", 10, rules, u("Carth"), carthAdds, carthAdds),

		SelectUnitSquadBuilder("Qira", 12, rules, u("QIRA"), qiraAdds, qiraAdds),
		SelectUnitSquadBuilder("Qira", 12, rules, u("QIRA"), u("HANSOLO", "CHEWBACCALEGENDARY"), u("ADMINISTRATORLANDO")),
		SelectUnitSquadBuilder("Qira", 15, rules, u("QIRA"), u("Nest"), u("L3_37")),

		SelectUnitSquadBuilder("Lando", 10, rules, u("ADMINISTRATORLANDO"), u("HANSOLO"), u("CHEWBACCALEGENDARY")),

		SelectUnitSquadBuilder("BH", 17, rules, u("Bossk"), u("Dengar"), u("Boba Fett")),
		SelectUnitSquadBuilder("BH", 16, rules, u("Bossk"), bosskAdds, bosskAdds),
		SelectUnitSquadBuilder("BH", 16, rules, u("Zam"), u("Dengar"), u("Jango")),
		SelectUnitSquadBuilder("BH", 17, rules, u("Jango"), u("Cad Bane"), u("Embo")),
		SelectUnitSquadBuilder("BH", 16, rules, u("Jango"), jangoAdds, jangoAdds),

		SelectUnitSquadBuilder("Empire", 14, rules, u("Veers"), u("Colonel Starck"), u("Snowtrooper")),
		SelectUnitSquadBuilder("Empire", 14, rules, u("Krennic"), u("Tie Fighter Pilot"), u("Snowtrooper")),
		SelectUnitSquadBuilder("Empire", 14, rules, u("Thrawn"), thrawnAdds, thrawnAdds),
		
		SelectUnitSquadBuilder("Sith", 16, rules, u("Palpatine"), u("Sith Trooper"), u("BASTILASHANDARK")),
		SelectUnitSquadBuilder("Sith", 15, rules, u("Palpatine"), palpAdds, palpAdds),
		SelectUnitSquadBuilder("Sith", 14, rules, u("Dooku"), u("Sith Assassin"), u("BASTILASHANDARK")),
		SelectUnitSquadBuilder("Sith", 14, rules, u("Maul"), u("Savage Opress"), u("Sith Assassin")),
		SelectUnitSquadBuilder("Sith", 15, rules, u("Savage Opress"), opressAdds, opressAdds),
		SelectUnitSquadBuilder("Sith", 16, rules, u("Traya"), trayaAdds, trayaAdds),

		// i didn't model it this way, but talzin and asajj could be on the same team
		SelectUnitSquadBuilder("NS", 16, rules, u("Talzin"), u("Zombie"), u("Daka")),
		SelectUnitSquadBuilder("NS", 16, rules, u("Asajj"), asajjAdds, asajjAdds),

		SelectUnitSquadBuilder("FO", 15, rules, u("KRU"), kruAdds, kruAdds),
		SelectUnitSquadBuilder("FO", 15, rules, u("Phasma"), phasmaAdds, phasmaAdds),

		SelectUnitSquadBuilder("CLS", 15, rules, u("CLS"), u("Chewbacca"), u("Han Solo")),
		SelectUnitSquadBuilder("CLS", 15, rules, u("CLS"), u("Baze"), u("Chirrut")),

		SelectUnitSquadBuilder("R1", 16, rules, u("Jyn"), u("Baze"), u("Chirrut")),
		SelectUnitSquadBuilder("R1", 14, rules, u("Jyn"), u("Scarif"), u("Captain Han Solo")),

		SelectUnitSquadBuilder("Wiggs", 15, rules, u("Wedge"), u("Biggs"), u("CLS", "Leia", "Stormtrooper Han")),
		SelectUnitSquadBuilder("Rebels", 15, rules, u("Ackbar"), u("Leia"), u("C3POLEGENDARY")),
		
		SelectUnitSquadBuilder("Ewoks", 15, rules, u("Chirpa"), u("Paploo"), u("Wicket")),

		SelectUnitSquadBuilder("Resistance", 15, rules, u("Finn"), u("Poe"), u("RT", "C3POLEGENDARY")),
		SelectUnitSquadBuilder("Resistance", 15, rules, u("JTR"), u("BB8"), u("Holdo", "Scav Rey", "R2-D2", "C3POLEGENDARY")),

	]
	
	static let standardPreference1Rules = UnitEvaluator(
		AllOmegasRule(),
		UnitRule.gear10PlusRule,
		UnitRule.level85Rule,
		UnitRule.sevenStarRule,
		
		CustomRule() { unit in
			var preference = 0
			if unit.gearLevel >= 10 {
				preference = 1
			}
			return Message(message: "unit boost", status: .ok, score: 0, max: 0, preference: preference)
		}
	)

	static let rules = [
		"DEFAULT" : UnitEvaluator(
			AllOmegasRule(),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"QUIGONJINN" : UnitEvaluator(
			AllOmegasRule(),
			
			OmegaZetaAbilityRule("leaderskill_QUIGONJINN", status: .needsRequired),
			
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"COMMANDERLUKESKYWALKER" : UnitEvaluator(
			AllOmegasRule(),
			
			OmegaZetaAbilityRule("leaderskill_COMMANDERLUKESKYWALKER", status: .needsRequired),
//			OmegaZetaAbilityRule("uniqueskill_COMMANDERLUKESKYWALKER02", status: .needsRequired),

			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"JEDIKNIGHTREVAN" : UnitEvaluator(
			AllOmegasRule(),
			
			OmegaZetaAbilityRule("leaderskill_JEDIKNIGHTREVAN", status: .needsRequired),

			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			PreferenceRule(preference: 5)
		),
		"BASTILASHAN" : standardPreference1Rules,
		"GRANDMASTERYODA" : standardPreference1Rules,
		"HERMITYODA" : UnitEvaluator(
			AllOmegasRule(),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			PreferenceRule(preference: 3)
		),
		"GENERALKENOBI" : standardPreference1Rules,
		"BARRISSOFFEE" : UnitEvaluator(
			AllOmegasRule(),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			OmegaZetaAbilityRule("uniqueskill_BARRISSOFFEE01", status: .needsRequired)
		),
		"BOSSK" : UnitEvaluator(
			AllOmegasRule(),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			OmegaZetaAbilityRule("leaderskill_BOSSK", status: .needsRequired),
			
			PreferenceRule(preference: 2)
		),
		"DENGAR" : standardPreference1Rules,
		"EMBO" : standardPreference1Rules,
		"JANGOFETT" : standardPreference1Rules,
		"BOBAFETT" : standardPreference1Rules,
		"EMPERORPALPATINE" : UnitEvaluator(
			AllOmegasRule(),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			OmegaZetaAbilityRule("leaderskill_EMPERORPALPATINE", status: .needsRequired),
			
			PreferenceRule(preference: 1)
		),
		"CHEWBACCALEGENDARY" : standardPreference1Rules,
		"C3POLEGENDARY" : UnitEvaluator(
			// c3po is a bit of a glass, well, not cannon, but glass -- require him to be tough
			AllOmegasRule(),
			UnitRule(.gearLevel, atLeast: 10, target: 12),
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			PreferenceRule(preference: 1)
		),
		"KYLORENUNMASKED" : UnitEvaluator(
			AllOmegasRule(),
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,

			OmegaZetaAbilityRule("leaderskill_KYLORENUNMASKED", status: .needsRequired),

			PreferenceRule(preference: 1)
		),
		"FIRSTORDEREXECUTIONER" : standardPreference1Rules,
		"CT5555" : standardPreference1Rules,
		"ENFYSNEST" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule(.stars, atLeast: 5, target: 7)
		),

	]
	
}
