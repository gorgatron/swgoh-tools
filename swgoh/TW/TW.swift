//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

func TWProperties(name: String? = nil, preference: Int = 0, unitMinimumGP: Int = 0, rules: [String:UnitEvaluator]? = nil, squadRules: [SquadEvaluationRule]? = nil) -> Squad.Properties {
	return Squad.Properties(name: name, preference: preference, unitMinimumGP: 6000, rules: rules, squadRules: squadRules)
}

struct TW {
	
	static let config = OptimizedConfig()
	
	static func squadDefenseFactor(name: String?, gp: Int) -> Int {
		return Int(TW.config.squadDefenseFactor[name ?? "", default: 1] * Double(gp))
	}
	
	// MARK: - Squads
	
	// per hewbris, s-class
	static let trayaAdds = u("BSF", "Count Dooku", "Palpatine", "Sith Assassin", "Sith Trooper", "Nest", "Tarkin")
	static let trayaSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Traya", preference: 20, rules: defenseSithRules), u("Traya"), u("Nihilus"), u("Sion"), u("Thrawn"), u("Nest", "Sith Trooper"))
	static let traya2SquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Traya", preference: 15, rules: defenseSithRules), u("Traya"), u("Nihilus"), u("Sion"), trayaAdds, trayaAdds)
	static let darthRevanSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "DR", preference: 25, rules: darthRevanRules), u("Darth Revan"), u("HK"), u("Bastila Shan (Fallen)"), u("Malak", "Sion"), u("SiT", "DN"))

	static let foRemainder = u(UnitTypes.firstOrder)
	static let foSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "FO", rules: defenseRules), u("KYLORENUNMASKED"), u("KYLOREN"), foRemainder, foRemainder, foRemainder)
	
	static let gkAdds = u(UnitTypes.jedi.removing("JEDIKNIGHTREVAN", "BARRISSOFFEE", "EZRABRIDGERS3", "BASTILASHAN", "GENERALKENOBI", "GRANDMASTERYODA", "HERMITYODA", "KANANJARRUSS3").adding(UnitTypes.clones))
	static let allJedi = u(UnitTypes.jedi)
	
	static let gkSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "GK", preference:8, rules: gkDefenseRules), u("GENERALKENOBI"), u("BARRISSOFFEE"), u("Ahsoka"), u("ANAKINKNIGHT"), u("GMY"))
	static let gkSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "GK", rules: gkDefenseRules), u("GENERALKENOBI"), u("BARRISSOFFEE"), gkAdds, gkAdds, gkAdds)

	// this one isn't as good, but if producing lists for people to use, it may be worthwhile
	static let gk2SquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "GK", rules: gkNoZarrisDefenseRules), u("GENERALKENOBI"), u("BARRISSOFFEE"), gkAdds, gkAdds, gkAdds)
	static let revanSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Revan", preference: 15, rules: defenseRules), u("JEDIKNIGHTREVAN"), u("BASTILASHAN"), u("GMY"), u("Jolee"), u("GK"))
	static let revanSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Revan Offense", preference: 1, rules: defenseRules), u("JEDIKNIGHTREVAN"), u("GMY"), u("Hermit Yoda"), u("Jolee"), u("JKA", "Ezra", "BASTILASHAN"))
	static let bastilaSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "BS", rules: bastilaShanRules), u("BASTILASHAN"), u("GRANDMASTERYODA"), u("HERMITYODA"), u("GENERALKENOBI"), u("EZRABRIDGERS3"))
	
	static let phoenixRemainder = u(UnitTypes.phoenix)
	static let phoenixSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Phoenix", rules: defenseRules), u("Hera"), phoenixRemainder, phoenixRemainder, phoenixRemainder, phoenixRemainder)
	
	static let bhAdds = u(UnitTypes.bountyHunters.removing("EMBO", "BOSSK"))
	static let bhSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "BH", preference: 5, rules: defenseRules), u("Bossk"), bhAdds, bhAdds, bhAdds, bhAdds)
	static let bhSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "BH", preference: 5, rules: defenseRules), u("Embo"), bhAdds, bhAdds, bhAdds, bhAdds)

	// per hewbris, this is S class
	static let clsSquadBuilder1 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", preference: 15, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("CHEWBACCALEGENDARY"), u("R2D2_LEGENDARY"), u("C3POLEGENDARY"))
	static let clsSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", preference: 1, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("Baze"), u("Chirrut"), u("CHEWBACCALEGENDARY"))
	static let clsSquadBuilder3 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", preference: 4, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("Leia"), u("Fulcrum"), u("CHEWBACCALEGENDARY"))
	static let clsSquadBuilder4 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", preference: 4, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("Leia"), u("Old Ben"), u("CHEWBACCALEGENDARY"))
	static let clsSquadBuilder5 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", preference: 4, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("Wedge"), u("Biggs"), u("CHEWBACCALEGENDARY"))
	static let clsSquadBuilder6 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", preference: 0, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("CHEWBACCALEGENDARY"), u(UnitTypes.rebels), u(UnitTypes.rebels))
	static let clsSquadBuilder = MultipleSquadBuilder(clsSquadBuilder1, clsSquadBuilder2, clsSquadBuilder3, clsSquadBuilder4, clsSquadBuilder5, clsSquadBuilder6)

	// this one is supposed to be over the top
	static let qiraSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Qira", preference: 40, rules: defenseRules), u("QIRA"), u("ZAALBAR"), u("VANDOR"), u("L3_37"), u("NEST"))
	
	static let krennicSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Krennic", preference: 2, rules: defenseRules), u("Krennic"), u("DEATHTROOPER"), u("Shoretrooper"))
	static let defenseSithSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "zEP", preference: 2, rules: defenseSithRules), u("Palpatine"), u("Darth Vader"), u("DARTHSION"), u(UnitTypes.sith), u(UnitTypes.sith))
	static let defenseSithSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "zEP", preference: 2, rules: defenseSithRules), u("Palpatine"), u("Darth Vader"), u("Shoretrooper"), u("Tarkin"), u("TIEFIGHTERPILOT"))

	static let wiggsDefenseBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Wiggs-D", preference: 2, rules: defenseRules), u("Wedge"), u("Biggs"), u("Leia"), u("Chirrut"), u("Baze"))

	static let standardPreference1Rules = UnitEvaluator(
		UnitRule.gear10PlusRule,
		UnitRule.level85Rule,
		UnitRule.sevenStarRule,
		
		CustomRule() { unit in
			var preference = 0
			if unit.gearLevel >= 10 {
				preference = 1
			}
			return Message(message: "unit boost", status: .ok, score: 0, max: 0, preference: preference)
		}
	)

	static let defenseRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"COMMANDERLUKESKYWALKER" : UnitEvaluator(
			OmegaZetaAbilityRule("leaderskill_COMMANDERLUKESKYWALKER", status: .needsRequired),
//			OmegaZetaAbilityRule("uniqueskill_COMMANDERLUKESKYWALKER02", status: .needsRequired),

			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"JEDIKNIGHTREVAN" : UnitEvaluator(
			OmegaZetaAbilityRule("leaderskill_JEDIKNIGHTREVAN", status: .needsRequired),

			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			PreferenceRule(preference: 5)
		),
		"BASTILASHAN" : standardPreference1Rules,
		"GRANDMASTERYODA" : standardPreference1Rules,
		"HERMITYODA" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			PreferenceRule(preference: 3)
		),
		"GENERALKENOBI" : standardPreference1Rules,
		"BARRISSOFFEE" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			OmegaZetaAbilityRule("uniqueskill_BARRISSOFFEE01", status: .needsRequired),
			
			PreferenceRule(preference: 1)
		),
		"BOSSK" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			OmegaZetaAbilityRule("leaderskill_BOSSK", status: .needsRequired),
			
			PreferenceRule(preference: 2)
		),
		"EMBO" : TWRule(leaderZeta: true, preference: 2).e,
		"DENGAR" : standardPreference1Rules,
		"JANGOFETT" : standardPreference1Rules,
		"BOBAFETT" : standardPreference1Rules,
		"EMPERORPALPATINE" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			OmegaZetaAbilityRule("leaderskill_EMPERORPALPATINE", status: .needsRequired),
			
			PreferenceRule(preference: 1)
		),
		"CHEWBACCALEGENDARY" : standardPreference1Rules,
		"C3POLEGENDARY" : UnitEvaluator(
			// c3po is a bit of a glass, well, not cannon, but glass -- require him to be tough
			UnitRule(.gearLevel, atLeast: 10, target: 12),
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			PreferenceRule(preference: 1)
		),
		"KYLORENUNMASKED" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,

			OmegaZetaAbilityRule("leaderskill_KYLORENUNMASKED", status: .needsRequired),

			PreferenceRule(preference: 1)
		),
		"FIRSTORDEREXECUTIONER" : standardPreference1Rules,
		"CT5555" : standardPreference1Rules,
		"ENFYSNEST" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule(.stars, atLeast: 5, target: 7)
		),

	]
	
	static let defenseSithRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"Malak" : TWRule(stars: 5, gear: 10, preference: 2).e,
		
		"EMPERORPALPATINE" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			OmegaZetaAbilityRule("leaderskill_EMPERORPALPATINE", status: .needsRequired),
			
			PreferenceRule(preference: 1)
		),
		"BASTILASHANDARK" : UnitEvaluator(
			UnitRule.gear9PlusRule,
			UnitRule.level85Rule,
			
			CustomRule() { unit in
				var preference = 0
				if unit.stars > 6 && unit.gearLevel >= 9 {
					preference = 2
				}
				return Message(message: "unit boost", status: .ok, score: 0, max: 0, preference: preference)
			}
		),
		"DARTHTRAYA" : standardPreference1Rules,
		"DARTHSION" : standardPreference1Rules,
		"DARTHNIHILUS" : standardPreference1Rules,
		"SITHTROOPER" : standardPreference1Rules,

	]
	
	static let darthRevanRules = [
		"DEFAULT" : TWRule().e,
		"Darth Revan" : TWRule(leaderZeta: true, uniqueZetas: true, stars: 7, gear: 11, preference: 2).e,
		"SiT" : TWRule(preference: 1).e,
		"Malak" : TWRule(uniqueZetas: true, stars: 5, gear: 10, preference: 1).e,
	]

	static let gkDefenseRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"GENERALKENOBI" : standardPreference1Rules,
		"BARRISSOFFEE" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			OmegaZetaAbilityRule("uniqueskill_BARRISSOFFEE01", status: .needsRequired),
			
			PreferenceRule(preference: 1)
		),
		"JOLEEBINDO" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			PreferenceRule(preference: 2)
		),
		"QUIGONJINN" : standardPreference1Rules,
		"AHSOKATANO" : standardPreference1Rules,

	]
	
	static let gkNoZarrisDefenseRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		"GENERALKENOBI" : standardPreference1Rules,
		"BARRISSOFFEE" : standardPreference1Rules,
		"JOLEEBINDO" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			PreferenceRule(preference: 2)
		),
		"QUIGONJINN" : standardPreference1Rules,
		"AHSOKATANO" : standardPreference1Rules,
		
		]

	// this is meant for offense and is quite broad -- low priority
	static let zetaMaulBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "zMaul", preference: 0, rules: zetaMaulRules), u("MAUL"), u(UnitTypes.sith), u(UnitTypes.sith), u(UnitTypes.sith), u(UnitTypes.sith))

	static let zetaMaulRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"MAUL" : TWRule(leaderZeta: true).e,

		]

	static let ewokBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Ewoks", preference: 10, rules: ewokRules), u("CHIEFCHIRPA"), u(UnitTypes.ewoks), u(UnitTypes.ewoks), u(UnitTypes.ewoks), u(UnitTypes.ewoks))
	
	static let ewokRules = [
		"DEFAULT" : TWRule().e,
		"Chirpa" : TWRule(leaderZeta: true, preference: 1).e,
		"Wicket" : TWRule(preference: 1).e,
		"Elder" : TWRule(preference: 1).e,
		"Paploo" : TWRule(preference: 1).e,
		"Logray" : TWRule(preference: 1).e,
	]
	
	static let nsBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "NS", preference: 10, rules: nsRules), u("ASAJVENTRESS", "MOTHERTALZIN"), u(UnitTypes.nightsisters), u(UnitTypes.nightsisters), u(UnitTypes.nightsisters), u(UnitTypes.nightsisters))
	static let nestSistersBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "NestSisters", preference: 15, rules: nsRules), u("MOTHERTALZIN"), u("ASAJJ"), u("DAKA"), u("Zombie"), u("ENFYSNEST"))
	
	static let nsRules = [
		"DEFAULT" : TWRule().e,
		"Talzin" : TWRule(leaderZeta: true, preference: 5).e,
		"Asajj" : TWRule(leaderZeta: true, uniqueZetas: true, preference: 5).e,
		"Daka" : TWRule(preference: 3).e,
		"Zombie" : TWRule(preference: 3).e,
		"Spirit" : TWRule(preference: 3).e,
		"Acolyte" : TWRule(preference: 1).e,
		"Nest" : TWRule(stars: 5).e,
	]
	
	static let grievousBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "grievous", preference: 10, rules: grievousRules), u("GRIEVOUS"), u("B1BATTLEDROIDV2"), u("B2SUPERBATTLEDROID"), u("DROIDEKA"), u("MAGNAGUARD"))
	
	static let grievousRules = [
		"DEFAULT" : TWRule().e,
		]
	
	static let carthAdds = u(UnitTypes.oldRepublic.removing("JEDIKNIGHTREVAN", "JOLEE", "BASTILASHAN"))
	static let carthBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Carth", preference: 10, rules: carthRules), u("Carth"), u(UnitTypes.oldRepublic.removing("JEDIKNIGHTREVAN", "JOLEE", "BASTILASHAN").adding("Wampa")), carthAdds, carthAdds, carthAdds)
	
	static let carthRules = [
		"DEFAULT" : TWRule().e,
		"L3-37" : TWRule(preference:1).e,
		"T3-M4" : TWRule(preference:1).e,
		"Canderous" : TWRule(preference:1).e,
		"Mission" : TWRule(preference:1).e,
		"Zaalbar" : TWRule(preference:1).e,
		"Wampa" : TWRule(preference:1).e,
	]

	// padme (test team)
	static let padmeBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "padme", preference: 30, rules: padmeRules), u("PADMEAMIDALA"), u("GENERALKENOBI"), u("ANAKINKNIGHT", "C3POLEGENDARY"), u("AHSOKATANO", "C3POLEGENDARY"), u("BARRISSOFFEE", "C3POLEGENDARY"))
	static let padmeClones = SelectUnitSquadBuilder(properties: TWProperties(name: "padme", preference: 1, rules: padmeRules), u("PADMEAMIDALA"), u("CC2224"), u("CT210408"), u("CT5555"), u("CT7567"))

	static let padmeRules = [
		"PADMEAMIDALA" : UnitEvaluator(
			UnitRule(.gearLevel, atLeast: 7, target: 10),
			UnitRule.level85Rule
			// no seven star rule yet
		),

		"ANAKINKNIGHT" : UnitEvaluator(
			UnitRule.gear9PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			PreferenceRule(preference: 1)
		),

		"DEFAULT" : UnitEvaluator(
			UnitRule.gear9PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		]
	
	static let rogue1Extras = characters.values.filter({ $0.categories.contains("Rogue One") }).map({ $0.id }).removing("JYNERSO", "CHIRRUTIMWE", "BAZEMALBUS")
	
	static let rogue1Builder = SelectUnitSquadBuilder(properties: TWProperties(name: "Rogue1", preference: 1, rules: rogue1Rules), u("Jyn Erso"), u("Chirrut"), u("Baze"), u(rogue1Extras), u(rogue1Extras))
	
	static let rogue1Rules = [
		"DEFAULT" : TWRule(gear: 8).e,
		]
	
	/// special geonosian builder
	static let geonosianBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Geonosians", preference: 25, rules: geonosianRules), u("GEONOSIANBROODALPHA"), u("Poggle"), u("GEONOSIANSOLDIER"), u("GEONOSIANSPY"), u("SUNFAC"))
	
	static let geonosianRules = [
		"GEONOSIANBROODALPHA" : TWRule(leaderZeta: true, uniqueZetas: true, stars: 5, gear: 10, preference: 2).e,

		"DEFAULT" : UnitEvaluator(
			UnitRule.gear9PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		]
	
	/// special clones builder
	static let clonesBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Clones", preference: 20, rules: specialDefenseRules), u("CT7567"), u("CC2224"), u("CT210408"), u("CT5555"), u("CLONESERGEANTPHASEI"))

	static let hansBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "All Hans", preference: 50, rules: specialHanRules), u("STORMTROOPERHAN"), u("HOTHHAN"), u("HANSOLO"), u("SMUGGLERHAN"), u("YOUNGHAN", "CHEWBACCALEGENDARY"))


	static let specialHanRules = [
		"YOUNGHAN" : UnitEvaluator(
			UnitRule(.gearLevel, atLeast: 7, target: 10, weight: 200, status: .needsFarming),
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			
			// prefer young han over chewie
			PreferenceRule(preference: 1)

		),
		"CHEWBACCALEGENDARY" : UnitEvaluator(
			UnitRule.gear9PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,

			// prefer young han over chewie
			PreferenceRule(preference: -1)
			
		),
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear9PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
	]

	static let specialDefenseRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule(.gearLevel, atLeast: 5, target: 12, weight: 200, status: .needsFarming),
			CustomRule(rule: { (unit) -> Message in
				return Message(message: "OK", status: .ok, score: 100, max: 100)
			})
		),
	]

	
	static let bastilaShanRules = [
		"DEFAULT" : TWRule().e,
		"Revan" : TWRule(preference: 5).e,
		
		// we prefer Jolee on offense
		"Jolee" : TWRule(preference: 10).e,
		"BASTILASHAN" : standardPreference1Rules,
		"GRANDMASTERYODA" : standardPreference1Rules,
		
		"Hermit Yoda" : TWRule(preference: 3).e,
		
	]
	
	// MARK: - Offense
	
	static let troopersBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Troopers", preference: 5, rules: offenseRules), u("Veers"), u("Starck"), u(UnitTypes.imperialTroopers), u(UnitTypes.imperialTroopers), u(UnitTypes.imperialTroopers))
	
	static let wiggsAdds = u("CLS", "Han Solo", "Chewbacca", "Fulcrum", "Ezra", "C-3PO", "Princess Leia")
	static let wiggsBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Wiggs", preference: 5, rules: offenseRules), u("Wedge"), u("Biggs"), wiggsAdds, wiggsAdds, wiggsAdds)
	
	static let jtrBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "JTR", preference: 5, rules: offenseRules), u("REYJEDITRAINING"), u("REY", "C-3PO", "Poe"), u("BB8"), u("R2D2_LEGENDARY"), u("RESISTANCETROOPER", "Finn"))

	static let magmaAdds = u("Death trooper", "Director Krennic", "Tarkin", "Stormtrooper", "Shoretrooper")
	static let magmaBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Magma", preference: 5, rules: offenseRules), u("Thrawn"), u("MAGMATROOPER"), magmaAdds, magmaAdds, magmaAdds)

	static let jangoBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Jango", preference: 5, rules: offenseRules), u("Jango"), u("Boba"), u("Bossk"), u("Cad Bane"), u("Dengar"))

	static let rexBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Rex", preference: 5, rules: offenseRules), u("Rex"), u("Wampa", "Nest", "Nihilus"), u("Chirrut"), u("Baze"), u("R2-D2"))
	
	static let wampaAdds = u("Death trooper", "Director Krennic", "Visas Marr", "Hermit Yoda", "Jyn", "FOO")
	static let wampaBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Wampa", preference: 5, rules: offenseRules), u("Rex"), u("Wampa"), wampaAdds, wampaAdds, wampaAdds)

	static let qgjAdds = u("General Kenobi", "Old Ben", "Kanan", "Hermit Yoda")
	static let qgjBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "QGJ", preference: 1, rules: offenseRules), u("QGJ"), u("Aayla"), u("Jedi Knight Anakin"), qgjAdds, u("Ezra Bridger", "Ahsoka Tano"))

	static let offenseRules = [
		// JTR
		"REYJEDITRAINING" : UnitEvaluator(
			OmegaZetaAbilityRule("leaderskill_REYJEDITRAINING", status: .needsRequiredGear),
			OmegaZetaAbilityRule("uniqueskill_REYJEDITRAINING02", status: .needsTuning),
			
			UnitRule.gear10PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		
		// Imperial Troopers
		"VEERS" : UnitEvaluator(
			UnitRule.gear9PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,

			OmegaZetaAbilityRule("uniqueskill_VEERS01", status: .needsRequired)
		),
		"RANGETROOPER" : UnitEvaluator(
			UnitRule.gear9PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			PreferenceRule(preference: 1)
		),
		"MAGMATROOPER" : UnitEvaluator(
			UnitRule.gear9PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			PreferenceRule(preference: -1)
		),
		
		"DEFAULT" : UnitEvaluator(
			UnitRule.gear9PlusRule,
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
	]

	// MARK: - fleets
	
	// From zynix
	// So, the best fleet is Ackbar Capital, OGMF 5+, Houndstooth 7, Tie Silencer 7. Then, Phantom II + Ghost both 7, Cassian’s U-wing 7, Bigg’s or Wedge’s X-wing, in that order. It’s better if the toons associated with the ships are 7 + G12 and zetaed.
	// Some people put Bigg’s X-wing in place of the Tie Silencer, but, the Silencer goes first if KRU is geared/zetaed and can stun an opponent’s ship right away.
	//	I don’t have Cassian’s U-wing/Rogue One toons up to snuff so I substitute it with DV’s Tie.

	static let fleet1Bbuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "OGMF", preference: 20, rules: fleetRules), u("CAPITALMONCALAMARICRUISER"), u("MILLENNIUMFALCON"), u("HOUNDSTOOTH"), u("TIESILENCER", "XWINGRED3"), u("PHANTOM2"), u("GHOST"), u("UWINGROGUEONE", "TIEADVANCED"), u("XWINGRED3", "XWINGRED2"))
	static let fleet2Bbuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "OGMF", preference: 25, rules: fleetRules), u("CAPITALMONCALAMARICRUISER"), u("MILLENNIUMFALCON"), u("HOUNDSTOOTH"), u("XWINGRED3"), u("PHANTOM2"), u("GHOST"), u("UWINGROGUEONE"), u("MILLENNIUMFALCONEP7", "XWINGRED2"))

	
	static let fleetRules = [
		"DEFAULT" : UnitEvaluator(
			UnitRule.level85Rule,
			UnitRule.sevenStarRule
		),
		"TIESILENCER" : UnitEvaluator(
			UnitRule.level85Rule,
			UnitRule.sevenStarRule,
			PreferenceRule(preference: 1)
		),
		"MILLENNIUMFALCON" : UnitEvaluator(
			UnitRule(.stars, atLeast: 5, target: 7),
			UnitRule.level85Rule
		),
		"HOUNDSTOOTH" : UnitEvaluator(
			UnitRule(.stars, atLeast: 5, target: 7),
			UnitRule.level85Rule
		),
	]

	// MARK: - Configurations

	static let configurations: [String:[SquadBuilder]] = [
		"all": [ foSquadBuilder, gkSquadBuilder, gkSquadBuilder2, revanSquadBuilder, revanSquadBuilder2, bastilaSquadBuilder, phoenixSquadBuilder, bhSquadBuilder, bhSquadBuilder2, clsSquadBuilder, qiraSquadBuilder, krennicSquadBuilder, defenseSithSquadBuilder, defenseSithSquadBuilder2, wiggsDefenseBuilder, gk2SquadBuilder, ewokBuilder, grievousBuilder, nsBuilder, nestSistersBuilder, trayaSquadBuilder, traya2SquadBuilder, darthRevanSquadBuilder, troopersBuilder, wiggsBuilder, jtrBuilder, magmaBuilder, zetaMaulBuilder, jangoBuilder, rexBuilder, wampaBuilder, qgjBuilder, fleet1Bbuilder, fleet2Bbuilder, padmeBuilder, geonosianBuilder, rogue1Builder, carthBuilder ],
		"clones": [ clonesBuilder ],
		"revan": [ revanSquadBuilder ],
		"bh": [ bhSquadBuilder ],
		"zmaul": [ zetaMaulBuilder ],
		"gk": [ gkSquadBuilder ],
		"bugs" : [ geonosianBuilder ],
		"special": [ foSquadBuilder, gkSquadBuilder, gkSquadBuilder2, revanSquadBuilder, bastilaSquadBuilder, phoenixSquadBuilder, bhSquadBuilder, clsSquadBuilder, qiraSquadBuilder, krennicSquadBuilder, defenseSithSquadBuilder, defenseSithSquadBuilder2, wiggsDefenseBuilder, gk2SquadBuilder, ewokBuilder, grievousBuilder, nsBuilder, nestSistersBuilder, trayaSquadBuilder, hansBuilder, padmeClones, padmeBuilder ],
		"test" : [ carthBuilder, padmeBuilder, revanSquadBuilder, revanSquadBuilder2 ],
		"3v3" : TW3v3.builders,
	]

}
