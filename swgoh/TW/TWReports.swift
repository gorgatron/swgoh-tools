//
//  TWReports.swift
//  swgoh
//
//  Created by David Koski on 5/9/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

func defaultTWScheduler() -> TWSectionScheduler {
	return RandomScheduler3()
}

struct TWPlanningReport : Report {
	
	let scheduler: TWSectionScheduler
	
	init(scheduler: TWSectionScheduler = defaultTWScheduler()) {
		self.scheduler = scheduler
	}
	
	func report(data: [Player : [SquadEvaluation]]) -> String {
		
		let schedule = scheduler.schedule(data: data, config: TW.config)
		var groups = [String:[(Player,SquadEvaluation)]]()
		
		for (player, evaluation, section) in schedule {
			groups[section, default: []].append( (player, evaluation) )
		}
		
		var result = ""
		for group in groups.keys.sorted() {
			result += "**\(group)** count = \(groups[group]!.count)\n"
			
			let squads = groups[group]!.sorted { (s1, s2) -> Bool in
				let gp1 = s1.1.squad.gp
				let gp2 = s2.1.squad.gp
				return gp1 > gp2
			}
			for (player, evaluation) in squads {
				result += "\(player.stats.name): "
				if let name = evaluation.squad.name {
					result += "\(name)"
				}
				if !evaluation.squad.units.isEmpty {
					result += " - "
					result += evaluation.squad.units.map {
						var z = ""
						for _ in $0.zetas {
							z += "z"
						}
						let stars = $0.stars == 7 ? "" : "\($0.stars)*/"
						return "\(z)\($0.commonName) \(stars)G\($0.gearLevel)"
						}.joined(separator: " ")
					
					let gp = evaluation.squad.gp
					result += " - \(gp)"
				}
				
				result += "\n"
			}
			
			result += "\n\n"
		}
		
		return result
	}
	
	func report(player: Player, evaluations: [SquadEvaluation]) -> String {
		// not used
		return ""
	}
}

struct TWPlayerReport : Report {
	
	let scheduler: TWSectionScheduler
	
	init(scheduler: TWSectionScheduler = defaultTWScheduler()) {
		self.scheduler = scheduler
	}
	
	func report(data: [Player : [SquadEvaluation]]) -> String {
		
		var result = ""
		
		let schedule = scheduler.schedule(data: data, config: TW.config)
		var byPlayer = [Player: [(String, SquadEvaluation)]]()
		
		for (player, evaluation, section) in schedule {
			byPlayer[player, default: []].append( (section, evaluation) )
		}
		
		for player in byPlayer.keys.sorted() {
			let sectionEvaluations = byPlayer[player]!
			
			result += "**\(player.stats.name)**"
			if let mention = player.mention {
				result += " " + mention
			}
			
			let hasFleet = sectionEvaluations.contains { TW.config.fleetSections.contains($0.0) }
			
			let squadCount = sectionEvaluations.count - (hasFleet ? 1 : 0)
			let placementScore = 34 /* fleet */ + 30 * squadCount
			result += " score: \(placementScore)"
			
			result += "\n"
			
			for (section, evaluation) in sectionEvaluations.sorted(by: { return $0.0 < $1.0 }) {
				result += "\(section): "
				if let name = evaluation.squad.name {
					result += "\(name)"
				}
				if !evaluation.squad.units.isEmpty {
					result += " - "
					let squadDesc = evaluation.squad.units.map {
						var z = ""
						for _ in $0.zetas {
							z += "z"
						}
						let c = $0.commonName.contains(" ") ? "," : ""
						return "\(z)\($0.commonName)\(c)"
						}.joined(separator: " ")
					result += squadDesc
					if result.last == "," {
						result.removeLast()
					}
				}
				result += "\n"
			}
			if !hasFleet {
				result += "\(TW.config.anyPlacedFleet)\n"
			}
			result += "\n\n"
		}
		
		return result
	}
	
	func report(player: Player, evaluations: [SquadEvaluation]) -> String {
		// not used
		return ""
	}
}

