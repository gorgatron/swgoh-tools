//
//  TWGAScheduler.swift
//  swgoh
//
//  Created by David Koski on 5/24/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

struct TWGAConfig {
	
	let fullSectionCount = 21
	let placementSections = [ "T1", "T2", "T3", "T4", "B1", "B2", "B3", "B4" ]
	
	let behind = [
		"T1" : [ "T2", "T3", "T4" ],
		"T2" : [ "T3", "T4" ],
		"T3" : [ "T4" ],
		"B1" : [ "T2", "T3", "T4", "B2", "B3", "B4" ],
		"B2" : [ "T3", "T4", "B3", "B4"],
		"B3" : [ "B4" ],
	]

	let maximumSquadsThresholds: [(Int, TWMaximumSquads)] = [
		( 1_500_000, TWMaximumSquads(named: 1, fluff: 1) ),
		( 2_200_000, TWMaximumSquads(named: 2, fluff: 1) ),
		( 3_000_000, TWMaximumSquads(named: 3, fluff: 1) ),
		( 4_000_000, TWMaximumSquads(named: 4, fluff: 1) ),
		( 10_000_000, TWMaximumSquads(named: 4, fluff: 2) ),
	]
	
	let defenseTeams: [String:Int] = [
		"BH": 0,
		"FO": 0,
		"GK": 0,
		"CLS": 0,
		"BS": 0,
		"zEP": 0,
		"Wiggs-D": 0,
		
		// I like NS on offense
		"NS": 8,
		"NestSisters": 4,
		
		"Phoenix": 0,
		
		"Ewoks": 0,
		"Revan": 4,
		
		"Any Squad": 0,
	]
	
	let seedInstructions = [
		[
			"BH" : ( 21, [ "T3" ]),
			"zEP" : ( 16, [ "T4" ]),
			"NestSisters" : ( 1, [ "T4" ]),
			"NS" : ( 2, [ "T4" ]),
			"Revan" : ( 2, [ "T4" ]),
			"FO" : ( 10, [ "T1", "B1" ]),
			"Ewoks" : ( 10, [ "T1", "B1" ]),
		]
	]
	
	let interestingSections: [[String]] = [
		[ "T3", "T4" ],
		[ "T1", "B1", "T2", "B2" ],
		[ "T3", "B3", "T4", "B4" ],
		[ "T1", "T2", "T3", "T4" ],
		[ "B1", "B2", "B3", "B4" ],
		[ "T1", "B1" ],
		[ "T2", "B2" ],
		[ "T3" ],
		[ "T4" ],
		[ "B4" ],
	]

	func maximumSquadsFor(player: Player) -> TWMaximumSquads {
		for entry in maximumSquadsThresholds {
			if player.stats.gp <= entry.0 {
				return entry.1
			}
		}
		return maximumSquadsThresholds.last!.1
	}
}

struct TWGASquadGene : Codable {
	let name: String
	let placements: [String]
}

struct TWGAChromosone : Codable, CustomStringConvertible {
	let sectionInventory: CountedSet<String>
	let squadInventory: CountedSet<String>
	let genes: [String:TWGASquadGene]
	
	var description: String {
		var result = ""
		
		var allSections = [String:CountedSet<String>]()
		
		for (name, gene) in genes {
			for placement in gene.placements {
				allSections[placement, default: CountedSet()].add(name)
			}
		}
		
		for section in allSections.keys.sorted() {
			let sectionMap = allSections[section]!
			result += "\(section) (\(sectionMap.count) named): "
			var first = true
			for squadName in sectionMap.contents.keys.sorted(by: { sectionMap[$0] > sectionMap[$1] }) {
				if !first {
					result += ", "
				}
				result += "\(squadName) \(allSections[section]![squadName])"
				first = false
			}
			result += "\n"
		}
		
		result += "Unused:\n"
		for (key, value) in squadInventory.contents {
			if value > 0 {
				result += "\t\(key): \(value)\n"
			}
		}
		
		return result
	}
}

struct TWGA {
	
	let defenseGuild: [Player : [SquadEvaluation]]
	let offense: [Squad]
	let config: TWGAConfig
	
	let totalNamedSquadsAllowed: Int
	let countBySquad: CountedSet<String>
	
	let initialChromosone: TWGAChromosone?
	
	init(defenseGuild: [Player : [SquadEvaluation]], offenseGuild: [Player : [SquadEvaluation]], config: TWGAConfig, initialChromosone: TWGAChromosone? = nil) {
		self.defenseGuild = defenseGuild
		self.config = config
		
		self.offense = TWCombatSimulator.prepareOffense(offenseGuild: offenseGuild)
		
		self.totalNamedSquadsAllowed = defenseGuild.keys.reduce(0, { $0 + config.maximumSquadsFor(player: $1).named })
		
		var countBySquad = CountedSet<String>()
		for evaluations in defenseGuild.values {
			for evaluation in evaluations {
				let squadName = evaluation.squad.nameOrDefault
				if let maximum = config.defenseTeams[squadName] {
					if maximum == 0 || countBySquad[squadName] < maximum {
						countBySquad.add(squadName)
					}
				}
			}
		}
		self.countBySquad = countBySquad
		
		// we need to reset the inventories
		if let initialChromosone = initialChromosone {
			self.initialChromosone = TWGA.reconcile(chromosone: initialChromosone, totalNamedSquadsAllowed: totalNamedSquadsAllowed, countBySquad: countBySquad, config: config)
		} else {
			self.initialChromosone = nil
		}
	}
	
	/// handles a "foreign" chromosone and conforms it to the local state
	static func reconcile(chromosone: TWGAChromosone, totalNamedSquadsAllowed: Int, countBySquad: CountedSet<String>, config: TWGAConfig) -> TWGAChromosone {
		var sectionInventory = CountedSet(keysAndValues: config.placementSections.map({ ($0, config.fullSectionCount) }))
		var squadInventory = countBySquad
		
		var genes = [String:TWGASquadGene]()
		
		// make sure we are not asking for more teams that we have
		for gene in chromosone.genes.values {
			var placements = gene.placements
			if gene.placements.count > squadInventory[gene.name] {
				placements = Array(placements.prefix(squadInventory[gene.name]))
			}
			
			// and we are not using placements that we don't have
			var newPlacements = [String]()
			for section in placements {
				if sectionInventory[section] == 0 {
					let newSection = sectionInventory.anyElement()
					sectionInventory.remove(newSection)
					newPlacements.append(newSection)
				} else {
					sectionInventory.remove(section)
					newPlacements.append(section)
				}
			}
			
			genes[gene.name] = TWGASquadGene(name: gene.name, placements: newPlacements)
			squadInventory.remove(gene.name, count: newPlacements.count)
		}
		
		// finally fill it out if we are missing teams
		let placedSquads = countBySquad.count - squadInventory.count
		if placedSquads < totalNamedSquadsAllowed {
			for _ in 0 ..< totalNamedSquadsAllowed - placedSquads {
				
				if squadInventory.count == 0 {
					// we don't have enough named teams to fill it
					break
				}
				
				let newSquad = squadInventory.anyElement()
				squadInventory.remove(newSquad)
				
				if let gene = genes[newSquad] {
					var placements = gene.placements
					let newSection = sectionInventory.anyElement()
					sectionInventory.remove(newSection)
					placements.append(newSection)
					genes[newSquad] = TWGASquadGene(name: newSquad, placements: placements)
				} else {
					let newSection = sectionInventory.anyElement()
					sectionInventory.remove(newSection)
					genes[newSquad] = TWGASquadGene(name: newSquad, placements: [newSection])
				}
			}
		}
		
		// or if we have too many teams, trim it back
		if placedSquads > totalNamedSquadsAllowed {
			while (countBySquad.count - squadInventory.count) > totalNamedSquadsAllowed {
				let (squadName, gene) = genes.randomElement()!
				
				if gene.placements.count > 0 {
					var placements = gene.placements
					let removedSection = placements.removeLast()
					
					sectionInventory.add(removedSection)
					squadInventory.add(squadName)
					
					genes[squadName] = TWGASquadGene(name: squadName, placements: placements)
				}
			}
		}
		
		return TWGAChromosone(sectionInventory: sectionInventory, squadInventory: squadInventory, genes: genes)
	}
	
	func newGene(name: String, count: Int, inventory: inout CountedSet<String>, preferredSections:[String]) -> TWGASquadGene {
		var placements = [String]()
		
		// use the preferred list first
		var preferredInventory = inventory.subset(preferredSections)
		
		while placements.count < count && !preferredInventory.isEmpty {
			let section = preferredInventory.anyElement()
			preferredInventory.remove(section)
			inventory.remove(section)
			placements.append(section)
		}

		// finish up from the entire inventory
		while placements.count < count {
			let section = inventory.anyElement()
			inventory.remove(section)
			placements.append(section)
		}
		
		return TWGASquadGene(name: name, placements: placements)
	}

	func newGene(name: String, count: Int, inventory: inout CountedSet<String>) -> TWGASquadGene {
		// sometimes we can pick an interesting subset
		if Bool.random() {
			return newGene(name: name, count: count, inventory: &inventory, preferredSections: config.interestingSections.randomElement()!)
			
		} else {
			var placements = [String]()
			
			// finish up from the entire inventory
			while placements.count < count {
				let section = inventory.anyElement()
				inventory.remove(section)
				placements.append(section)
			}
			
			return TWGASquadGene(name: name, placements: placements)
		}
	}
	
	func newChromosone() -> TWGAChromosone {
		var sectionInventory = CountedSet(keysAndValues: config.placementSections.map({ ($0, config.fullSectionCount) }))
		let squadInventory = countBySquad.randomSubset(count: min(countBySquad.count, totalNamedSquadsAllowed))
		
		var genes = [String:TWGASquadGene]()
		
		for (squad, squadCount) in squadInventory.contents {
			let gene = newGene(name: squad, count: squadCount, inventory: &sectionInventory)
			genes[squad] = gene
		}
		
		return TWGAChromosone(sectionInventory: sectionInventory, squadInventory: countBySquad.subtracting(squadInventory), genes: genes)
	}
	
	func newChromosone(instructions: [String:(Int,[String])]) -> TWGAChromosone {
		var sectionInventory = CountedSet(keysAndValues: config.placementSections.map({ ($0, config.fullSectionCount) }))
		var genes = [String:TWGASquadGene]()
		
		for (squadName, (count, preferredSections)) in instructions {
			let gene = newGene(name: squadName, count: count, inventory: &sectionInventory, preferredSections: preferredSections)
			genes[squadName] = gene
		}
		
		let potentialChromosone = TWGAChromosone(sectionInventory: sectionInventory, squadInventory: CountedSet(), genes: genes)
		
		return TWGA.reconcile(chromosone: potentialChromosone, totalNamedSquadsAllowed: totalNamedSquadsAllowed, countBySquad: countBySquad, config: config)
	}
	
	func run(chromosone: TWGAChromosone) throws -> TWCombatSimulator.Result {
		var results = [TWCombatSimulator.Result]()
		
		let scheduler = TWGAScheduler()
		
		// prep the defense
		let defenseTuples = try scheduler.schedule(data: defenseGuild, chromosone: chromosone, config: config)
		var defense = [String:[Squad]]()
		for item in defenseTuples {
			defense[item.2, default: []].append(item.1.squad)
		}

		let simulator = TWCombatSimulator(defense: defense, offense: self.offense)
		
		for _ in 0 ..< 4 {
			results.append(simulator.simulate(plan: ["T1", "T2", "T3", "T4", "B1", "B2", "B3", "B4"]))
			results.append(simulator.simulate(plan: ["B1", "B2", "B3", "B4", "T3", "T4", "T1", "T2"]))
			results.append(simulator.simulate(plan: ["B1", "B2", "T3", "T4", "B3", "B4", "T1", "T2"]))
		}
		
		return results.max()!
	}
	
	func run(generation: [TWGAChromosone]) -> [(TWGAChromosone, TWCombatSimulator.Result)] {
		var result = [(TWGAChromosone, TWCombatSimulator.Result)]()
		
		let queue = DispatchQueue(label: "build")
		func append(_ item: (TWGAChromosone, TWCombatSimulator.Result)) {
			queue.sync {
				result.append(item)
			}
		}

		DispatchQueue.concurrentPerform(iterations: generation.count) {
			let chromosone = generation[$0]
//		for chromosone in generation {
			do {
				let singleResult = try run(chromosone: chromosone)
				append((chromosone, singleResult))
			} catch {
				// simply discard the chromosone if it is not viable (e.g. can't provide placement coverage
			}
		}
		
		return result
	}
	
	let GENERATION_SIZE = 30
	let KEEP_TOP = 6
	
	func mutate(_ chromasone: TWGAChromosone) -> TWGAChromosone {
		var genes = chromasone.genes
		var sectionInventory = chromasone.sectionInventory
		var squadInventory = chromasone.squadInventory

		let geneKey = genes.keys.randomElement()!
		
		switch Int.random(in: 0 ... 9) {
		case 0:
			// sort the placements
			let gene = genes[geneKey]!
			if Bool.random() {
				genes[geneKey] = TWGASquadGene(name: gene.name, placements: gene.placements.sorted(by: >))
			} else {
				genes[geneKey] = TWGASquadGene(name: gene.name, placements: gene.placements.sorted(by: <))
			}
			
		case 1:
			// shuffle the placements
			let gene = genes[geneKey]!
			genes[geneKey] = TWGASquadGene(name: gene.name, placements: gene.placements.shuffled())
			
		case 2 ... 5:
			// concentrate the placements -- this one seems to perform better, so higher weight
			let gene = genes[geneKey]!
			let randomSection = sectionInventory.anyElement()
			let sectionInventoryAvailable = sectionInventory[randomSection]
			let squadPlacementAvailable = gene.placements.count
			
			if squadPlacementAvailable > 0 {
				var placements = gene.placements
				for _ in 1 ..< Int.random(in: 1 ... min(sectionInventoryAvailable, squadPlacementAvailable)) {
					let old = placements.removeFirst()
					sectionInventory.add(old)
					placements.append(randomSection)
					sectionInventory.remove(randomSection)
				}
				genes[geneKey] = TWGASquadGene(name: gene.name, placements: placements)
			}

		case 6:
			// regenerate the placements
			let gene = genes[geneKey]!
			for sectionName in gene.placements {
				sectionInventory.add(sectionName)
			}
			
			genes[geneKey] = newGene(name: gene.name, count: gene.placements.count, inventory: &sectionInventory)

		case 7:
			// change the counts.  first remove from the selected gene
			let gene = genes[geneKey]!
			if gene.placements.count > 1 {
				let removeCount = Int.random(in: 1 ..< gene.placements.count)
				
				squadInventory.add(geneKey, count: removeCount)
				
				var placements = gene.placements
				for _ in 0 ..< removeCount {
					let old = placements.removeFirst()
					sectionInventory.add(old)
				}
				
				genes[geneKey] = TWGASquadGene(name: gene.name, placements: placements)
				
				// now donate those to other squads
				for _ in 0 ..< removeCount {
					let squad = squadInventory.anyElement()
					squadInventory.remove(squad)
					
					if let gene = genes[squad] {
						let newSection = sectionInventory.anyElement()
						sectionInventory.remove(newSection)
						
						var placements = gene.placements
						placements.append(newSection)
						
						genes[squad] = TWGASquadGene(name: gene.name, placements: placements)
					} else {
						genes[squad] = newGene(name: squad, count: 1, inventory: &sectionInventory)
					}
				}
			}
			
		case 8, 9:
			// move some of the units behind of of the teams with shared counters
			let gene = genes[geneKey]!
			var placements = gene.placements

			if placements.count > 0 {
				if let commonCounters = TWCounter.reverseCounterMap[gene.name]?.keys {
					placement: for _ in 0 ..< Int.random(in: 0 ..< gene.placements.count) {
						let commonCounter = commonCounters.randomElement()!
						if let counterGene = genes[commonCounter] {
							for section in counterGene.placements {
								if let behind = config.behind[section] {
									for behindSection in behind.shuffled() {
										if sectionInventory[behindSection] != 0 {
											let oldSection = placements.removeLast()
											sectionInventory.add(oldSection)
											placements.insert(behindSection, at: 0)
											sectionInventory.remove(behindSection)
											continue placement
										}
									}
								}
							}
						}
					}
				}
			}
			
			genes[geneKey] = TWGASquadGene(name: gene.name, placements: placements)
			
		default:
			break
		}
		
		return TWGAChromosone(sectionInventory: sectionInventory, squadInventory: squadInventory, genes: genes)
	}
	
	func combine(_ chromosones: TWGAChromosone...) -> TWGAChromosone {
		var combinedGeneNames = Set<String>()
		
		for chromosone in chromosones {
			combinedGeneNames.formUnion(chromosone.genes.keys)
		}
		
		var newGenes = [String:TWGASquadGene]()
		
		// merge the candidates
		for geneName in combinedGeneNames {
			let source = chromosones.randomElement()!
			if let gene = source.genes[geneName] {
				newGenes[geneName] = gene
			}
		}
		
		// reconcile it (make sure all the numbers make sense)
		let reconciledGene = TWGA.reconcile(chromosone: TWGAChromosone(sectionInventory: CountedSet(), squadInventory: CountedSet(), genes: newGenes), totalNamedSquadsAllowed: totalNamedSquadsAllowed, countBySquad: countBySquad, config: config)
		
		if Bool.random() {
			return mutate(mutate(reconciledGene))
		} else {
			return reconciledGene
		}
	}
	
	func breed(sortedResults: [(TWGAChromosone, TWCombatSimulator.Result)]) -> [TWGAChromosone] {
		var result = [TWGAChromosone]()
		
		result.append(contentsOf: sortedResults.prefix(KEEP_TOP).map({ $0.0 }))
		
		if sortedResults.count > 5 {
			
			// produce some mutations, 2 each for each of the first 5 entries
			var improveOnly = [(TWGAChromosone, Int)]()
			for i in 0 ..< 5 {
				// normal, random mutation
				var chromasone = sortedResults[i].0
				for _ in 0 ..< Int.random(in: 1 ... 3) {
					chromasone = mutate(chromasone)
				}
				result.append(chromasone)
			}
						
			let queue = DispatchQueue(label: "mutate")
			func append(_ item: TWGAChromosone) {
				queue.sync {
					result.append(item)
				}
			}
			
			// this is expensive, so do it concurrently
			DispatchQueue.concurrentPerform(iterations: 5) {
				// mutations (up to a limit) until it gets better
				var chromasone = sortedResults[$0].0
				var i = 0
				while i < 8 {
					chromasone = mutate(chromasone)
					if let result = try? run(chromosone: chromasone) {
						if result.score.score < sortedResults[$0].1.score.score {
							break
						}
						i += 1
					} else {
						// failure in the evaluation, just take what we have
						break
					}
				}
				append(chromasone)
			}
			
			// this is expensive, so do it concurrently
			DispatchQueue.concurrentPerform(iterations: 5) {
				// mutations (up to a limit) until the number of placed squads go up
				var chromasone = sortedResults[$0].0
				let scheduler = TWGAScheduler()
				let initialPlacedCount = (try? scheduler.schedule(data: defenseGuild, chromosone: chromasone, config: config, placeAnySquads: false))?.count ?? 0
				
				var i = 0
				while i < 15 {
					chromasone = mutate(chromasone)
					
					let newPlacedCount = (try? scheduler.schedule(data: defenseGuild, chromosone: chromasone, config: config, placeAnySquads: false))?.count ?? 0
					
					if newPlacedCount > initialPlacedCount {
						break
					} else {
						i = i + 1
					}
				}
				append(chromasone)
			}


			// then some combinations
			for _ in 0 ..< 2 {
				result.append(combine(sortedResults[0].0, sortedResults[1].0))
				result.append(combine(sortedResults[0].0, sortedResults[1].0, sortedResults[2].0))
				result.append(combine(sortedResults[0].0, sortedResults[2].0))
				result.append(combine(sortedResults[1].0, sortedResults[2].0))
				result.append(combine(sortedResults[2].0, sortedResults[3].0))
				result.append(combine(sortedResults[0].0, sortedResults[20].0))
			}
		}
			
		// fill the remainder with new random ones
		while result.count < GENERATION_SIZE {
			result.append(newChromosone())
		}
		
		return result
	}
	
	func announce() {
		print("Generating orders for \(totalNamedSquadsAllowed) out of \(countBySquad.count) squads.")
		
		for squadName in countBySquad.contents.keys.sorted() {
			print("\t\(squadName): \(countBySquad[squadName])")
		}
		print("")
	}
	
	func run(count: Int) -> TWGAChromosone {
		var generation = [TWGAChromosone]()
		
		if let initialChromosone = initialChromosone {
			generation.append(initialChromosone)
			generation.append(initialChromosone)
			generation.append(mutate(initialChromosone))
			generation.append(mutate(mutate(initialChromosone)))
		}
		
		for instructions in config.seedInstructions {
			generation.append(newChromosone(instructions: instructions))
			generation.append(mutate(newChromosone(instructions: instructions)))
			generation.append(mutate(newChromosone(instructions: instructions)))
		}
		
		while generation.count < GENERATION_SIZE {
			generation.append(newChromosone())
		}
		
		var best: TWGAChromosone?
		for i in 0 ..< count {
			let results = run(generation: generation)
			
			// the best score had the lowest enemy score
			let sortedResults = results.sorted { (r1, r2) -> Bool in
				return r1.1.score.score < r2.1.score.score
			}
			
			generation = breed(sortedResults: sortedResults)

			best = sortedResults[0].0
			print("generation \(i): \(sortedResults[0].1.score.score)")
		}
		
		return best!
	}
	
}

struct TWGASchedulerAdaptor : TWSectionScheduler {
	
	let chromosone: TWGAChromosone
	let config: TWGAConfig
	
	func schedule(data: [Player : [SquadEvaluation]], config: TWConfig) -> [(Player, SquadEvaluation, String)] {
		var result = try! TWGAScheduler().schedule(data: data, chromosone: chromosone, config: self.config)
		
		// add fleet placements
		for (player, evaluations) in data {
			for evaluation in evaluations {
				if evaluation.squad.nameOrDefault == "OGMF" {
					result.append((player, evaluation, Bool.random() ? "F1" : "F2"))
				}
			}
		}
		
		return result
	}
}

struct TWGAScheduler {
	
	enum SchedulerError: Error {
		case notViable
	}
	
	func schedule(data: [Player : [SquadEvaluation]], chromosone: TWGAChromosone, config: TWGAConfig, placeAnySquads: Bool = true) throws -> [(Player, SquadEvaluation, String)] {
		var result = [(Player, SquadEvaluation, String)]()
		
		// get a global sort of the squads
		var allSquads = [(Player, SquadEvaluation)]()
		
		for (player, evaluations) in data {
			allSquads.append(contentsOf: evaluations.map({ (player, $0) }))
		}
		
		allSquads.sort { (s1, s2) -> Bool in
			return s1.1.squad.gp > s2.1.squad.gp
		}
		
		// figure out how many squads each player can place
		var allowed = CountedSet<Player>(keysAndValues: data.keys.map { player in (player, config.maximumSquadsFor(player: player).named)})
		
		// figure out how many of each squad type we want
		var desired = CountedSet(contents: chromosone.genes.mapValues { $0.placements.count })
		var placements = chromosone.genes.mapValues { $0.placements }
		
		var sectionNeeds = CountedSet<String>(keysAndValues: config.placementSections.map({ ($0, config.fullSectionCount) }))
		
		// now go through the squads, consuming them in power order while the player they belong to still allows it
		while !allSquads.isEmpty {
			let (player, evaluation) = allSquads.removeFirst()
			
			// see if the player is allowed to place any more
			if allowed[player] == 0 {
				continue
			}
			
			// see if the squad is still wanted
			let squadName = evaluation.squad.nameOrDefault
			if desired[squadName] == 0 {
				continue
			}
			
			// we want it, consume it
			desired.remove(squadName)
			allowed.remove(player)
			
			let sectionName = placements[squadName]!.removeFirst()
			sectionNeeds.remove(sectionName)
			result.append((player, evaluation, sectionName))
		}
		
		if placeAnySquads {
			// now add the fluff squads to the allowed
			for player in data.keys {
				allowed.add(player, count: config.maximumSquadsFor(player: player).fluff)
			}
			
			// finally fill in the blanks with fluff squads
			let sortedPlayers = data.keys.sorted { (p1, p2) -> Bool in
				return p1.stats.characterGP > p2.stats.characterGP
			}
			
			while !sectionNeeds.isEmpty {
				
				if allowed.isEmpty {
					// unable to fill the map
					throw SchedulerError.notViable
				}
				
				for player in sortedPlayers {
					if allowed[player] == 0 {
						continue
					}

					for sectionName in config.placementSections {
						if sectionNeeds[sectionName] > 0 {
							// found a spot
							sectionNeeds.remove(sectionName)
							allowed.remove(player)
							
							// make a fluff squad
							let squad = Squad(Squad.Properties(name: "Any Squad 70k"), units: [Unit]())
							let evaluation = SquadEvaluation(squad: squad, evaluations: [:], squadEvaluations: EvaluationSummary())
							
							result.append((player, evaluation, sectionName))
							break
						}
					}
				}
			}
		}
		
		return result
	}
	
}
