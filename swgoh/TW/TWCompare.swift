//
//  TWCompare.swift
//  swgoh
//
//  Created by David Koski on 5/14/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

struct TWCompare {
	
	let scheduler: TWSectionScheduler

	var data = [(GuildFile, [Player:[SquadEvaluation]])]()
	
	init(scheduler: TWSectionScheduler = defaultTWScheduler()) {
		self.scheduler = scheduler
	}

	mutating func add(guild: GuildFile, evaluations: [Player:[SquadEvaluation]]) {
		data.append((guild, evaluations))
	}
	
	func allSquadNames() -> [String] {
		var names = Set<String>()
		
		for (_, guild) in data {
			for (_, evaluations) in guild {
				for evaluation in evaluations {
					let name = evaluation.squad.nameOrDefault
					names.insert(name)
				}
			}
		}

		return names.sorted()
	}
	
	func allSquads(guild: [Player:[SquadEvaluation]], name: String) -> [SquadEvaluation] {
		var result = [SquadEvaluation]()
		for (_, evaluations) in guild {
			result.append(contentsOf: evaluations.filter() { $0.squad.nameOrDefault == name })
		}
		return result
	}
	
	func summarize(evaluations: [SquadEvaluation]) -> Summary {
		var totalGP = 0
		var zetas = 0
		var g12 = 0
		var count = 0
		
		for evaluation in evaluations {
			count += 1
			totalGP += evaluation.squad.gp
			zetas += evaluation.squad.units.reduce(0) { $0 + $1.zetas.count }
			g12 += evaluation.squad.units.reduce(0) { $0 + ($1.gearLevel >= 12 ? 1 : 0) }
		}
		
		if count == 0 {
			return Summary(count: 0, averageGP: 0, zetas: 0, g12: 0)
		}
		
		func doubleAverage(_ value: Int) -> Double {
			let average = Double(value) / Double(count)
			return Double(Int(average * 100)) / 100.0
		}
		
		return Summary(count: count, averageGP: totalGP / count, zetas: doubleAverage(zetas), g12: doubleAverage(g12))
	}
	
	struct Summary {
		let count: Int
		let averageGP: Int
		let zetas: Double
		let g12: Double
	}
	
	// TODO: total available counters considering gp
	// TODO: running total where I subtract used counters
	
	func report() -> String {
		var result = ""
		
		var summaries = [String:[Summary]]()
		
		for squadName in allSquadNames() {
			for (_, guildData) in data {
				let allSqads = self.allSquads(guild: guildData, name: squadName)
				let summary = summarize(evaluations: allSqads)
				summaries[squadName, default:[]].append(summary)
			}
		}

		for squadName in allSquadNames() {
			result += squadName
			for summary in summaries[squadName, default:[]] {
				result += ","
				result += summary.count.description
			}
			for summary in summaries[squadName, default:[]] {
				result += ","
				result += summary.averageGP.description
			}
			for summary in summaries[squadName, default:[]] {
				result += ","
				result += summary.zetas.description
			}
			for summary in summaries[squadName, default:[]] {
				result += ","
				result += summary.g12.description
			}

			result += "\n"
		}
		
		return result
	}

}
