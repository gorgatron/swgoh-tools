//
//  TWScheduler.swift
//  swgoh
//
//  Created by David Koski on 5/9/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

protocol TWSectionScheduler {
	
	func schedule(data: [Player : [SquadEvaluation]], config: TWConfig) -> [(Player, SquadEvaluation, String)]
	
}

/// assign squads per TW.sectionMap
struct SimpleSectionScheduler : TWSectionScheduler {
	
	/// for static placement, these are where the teams should go
	static let sectionMap = [
		"BH" : "T1",
		"FO" : "B1",
		"GK" : "B1",
		"CLS" : "T2",
		"BS" : "T2",
		"zEP" : "B1",
		"R1" : "T3",
		"NS" : "T3",
		"NestSisters" : "T3",
		"Phoenix" : "B3",
		"Ewoks" : "B3",
		"DR" : "T1",
		"Traya" : "B1",
	]
	
	func schedule(data: [Player : [SquadEvaluation]], config: TWConfig) -> [(Player, SquadEvaluation, String)] {
		var result = [(Player, SquadEvaluation, String)]()
		
		for (player, evaluations) in data {
			for evaluation in evaluations {
				if let name = evaluation.squad.name, let section = SimpleSectionScheduler.sectionMap[name] {
					result.append((player, evaluation, section))
				}
			}
		}
		
		return result
	}
	
}

struct TeamNameScheduler : TWSectionScheduler {
	
	func schedule(data: [Player : [SquadEvaluation]], config: TWConfig) -> [(Player, SquadEvaluation, String)] {
		var result = [(Player, SquadEvaluation, String)]()
		
		for (player, evaluations) in data {
			for evaluation in evaluations {
				let name = evaluation.squad.nameOrDefault
				result.append((player, evaluation, name))
			}
		}
		
		return result
	}
	
}


/// assign squads per TW.sectionMap but coonsider promoting and moving squads back based on power level
struct PromoteFallbackSectionScheduler : TWSectionScheduler {
	
	static let forwardThreshold = [
		"T2" : 100000,
		"B2" : 100000,
		"T3" : 100000,
		"B3" : 100000,
	]
	
	static let sectionPromote = [
		"T2" : "T1",
		"B2" : "B1",
		"T3" : "T2",
		"B3" : "B2",
	]
	
	static let sectionFallback = [
		"T1" : "T2",
		"B1" : "B2",
		"T2" : "T3",
		"B2" : "B3",
		"T3" : "T4",
		"B3" : "B4",
	]
	
	private func resolveSection(section: String, gp: Int, config: TWConfig) -> String? {
		if let threshold = PromoteFallbackSectionScheduler.forwardThreshold[section] {
			if gp >= threshold {
				if let newSection = PromoteFallbackSectionScheduler.sectionPromote[section] {
					return newSection
				}
			}
		}
		if let threshold = config.sectionThreshold[section] {
			if gp < threshold {
				if let newSection = PromoteFallbackSectionScheduler.sectionFallback[section] {
					return resolveSection(section: newSection, gp: gp, config: config)
				}
			} else {
				return section
			}
		}
		return nil
	}
	
	private func resolveSection(squad: Squad, config: TWConfig) -> String? {
		if let name = squad.name {
			let gp = squad.gp
			if let section = SimpleSectionScheduler.sectionMap[name] {
				return resolveSection(section: section, gp: gp, config: config)
			}
		}
		
		return nil
	}
	
	func schedule(data: [Player : [SquadEvaluation]], config: TWConfig) -> [(Player, SquadEvaluation, String)] {
		var result = [(Player, SquadEvaluation, String)]()
		
		for (player, evaluations) in data {
			for evaluation in evaluations {
				if let section = resolveSection(squad: evaluation.squad, config: config) {
					result.append((player, evaluation, section))
				}
			}
		}
		
		return result
	}
	
}

/// Follow the gp thresholds, but randomly place squads for the most part.  This actually has a couple phases:
///
/// - follow the randomPlacementInstructions for specific placements
/// - randomly place the top N squads from each player where N is dependent on gp
/// - fill the remainder with generic "fluff" squads
///
/// This is done in a repeatable way by using a RandomNumberGenerator with a fixed seed and canonical
/// ordering of all the data (via sorts).
struct RandomScheduler3 : TWSectionScheduler {
	
	static var r = MersenneTwister(seed: 8)
	
	// MARK: - comparisons for canonical ordering
	
	func compareSquads(_ t1: (Player, SquadEvaluation), _ t2: (Player, SquadEvaluation)) -> Bool {
		return compareSquads(t1.1, t2.1)
	}
	
	func compareSquads(_ s1: SquadEvaluation, _ s2: SquadEvaluation) -> Bool {
		let df1 = TW.squadDefenseFactor(name: s1.squad.name, gp: s1.squad.gp)
		let df2 = TW.squadDefenseFactor(name: s2.squad.name, gp: s2.squad.gp)
		
		return df1 > df2
	}
	
	// MARK: - result tracking and counters
	
	struct Result {
		/// this is what we are producing
		var placements = [(Player, SquadEvaluation, String)]()
		
		var placedSquadCountByPlayer = [Player:Int]()
		var placedSquadCountBySection = [String: Int]()
		
		let config: TWConfig
		
		init(config: TWConfig) {
			self.config = config
		}
		
		func maximumSquadsFor(player: Player) -> TWMaximumSquads {
			for entry in config.maximumSquadsThresholds {
				if player.stats.gp <= entry.0 {
					return entry.1
				}
			}
			return config.maximumSquadsThresholds.last!.1
		}
		
		mutating func add(player: Player, evalutation: SquadEvaluation, section: String) {
			placements.append((player, evalutation, section))
			if config.fleetSections.contains(section) {
				// we are not counting fleets
			} else {
				placedSquadCountByPlayer[player, default: 0] += 1
			}
			placedSquadCountBySection[section, default: 0] += 1
		}
		
		func allSectionsFull() -> Bool {
			return !config.placementSections.contains { placedSquadCountBySection[$0, default: 0] < config.fullSectionCount }
		}
		
		func isPlayerDone(_ player: Player) -> Bool {
			let allowed = maximumSquadsFor(player: player)
			return placedSquadCountByPlayer[player, default: 0] >= allowed.total
		}
		
		func allPlayersDone() -> Bool {
			for (player, count) in placedSquadCountByPlayer {
				let allowed = maximumSquadsFor(player: player)
				if count < allowed.total {
					return false
				}
			}
			return true
		}
	}
	
	// MARK: - find a random section for a given squad
	
	func randomSection(gp: Int, counts: [String: Int], shuffledSections: [String], overflow: Bool = false, config: TWConfig) -> String? {
		for section in shuffledSections {
			let count = counts[section, default: 0]
			let max = config.sectionMaxCount[section, default: 0]
			
			if overflow && config.backSections.contains(section) {
				// allow placement over the target in the back
				if count >= config.fullSectionCount {
					continue
				}
			} else {
				if count >= max {
					continue
				}
			}
			
			if let threshold = config.sectionThreshold[section], gp >= threshold {
				if let upperLimit = config.sectionMaxThreshold[section] {
					if gp <= upperLimit {
						return section
					}
				} else {
					// no upper limit
					return section
				}
			}
		}
		
		return nil
	}
	
	func randomSection(squadName: String, gp: Int, counts: [String: Int], potentialSections: [String], config: TWConfig) -> String? {
		// pick a preferred section first
		if let preferredSections = config.preferredSections[squadName] {
			if let section = randomSection(gp: gp, counts: counts, shuffledSections: preferredSections.shuffled(using: &RandomScheduler3.r), config:config) {
				return section
			}
		}
		
		// then any section
		let shuffled = potentialSections.shuffled(using: &RandomScheduler3.r)
		if let section = randomSection(gp: gp, counts: counts, shuffledSections: shuffled, config:config) {
			return section
		}
		
		// then try again using overflow rules (placing extras in the back)
		if let section = randomSection(gp: gp, counts: counts, shuffledSections: shuffled, overflow: true, config:config) {
			return section
		}
		
		return nil
	}
	
	// MARK: - Scheduling phases
	
	func buildSquadsByPlayer(data: [Player : [SquadEvaluation]], config: TWConfig) -> [Player:[(Player, SquadEvaluation)]] {
		var result = [Player:[(Player, SquadEvaluation)]]()
		for (player, evaluations) in data {
			// collect all the squads to consider.  we want a full list of them for running through randomPlacementInstructions and lists by player for the remainder of the scheduling
			let playerSquads = evaluations.filter { config.randomPlacementDefenseTeams.contains($0.squad.properties.name ?? "") }.sorted(by: compareSquads).map { (player, $0) }
			result[player] = playerSquads
		}
		return result
	}
	
	func runPlacementInstructions(squadsByPlayer: inout [Player:[(Player, SquadEvaluation)]], into result: inout Result, config: TWConfig) {
		// we want to look at all of the available squads in a canonical sorted order
		var consideredSquads = [(Player, SquadEvaluation)]()
		for player in squadsByPlayer.keys.sorted() {
			consideredSquads.append(contentsOf: squadsByPlayer[player, default: []])
		}
		consideredSquads.sort(by: compareSquads)
		
		// run the instructions
		for instruction in config.randomPlacementInstructions {
			switch instruction {
			case .placeSquad(squad: let squad, section: let section):
				if let index = consideredSquads.firstIndex(where: { item in
					return item.1.squad.properties.name == squad
				}) {
					// remove it from the big list so we don't use it again
					let item = consideredSquads.remove(at: index)
					
					result.add(player: item.0, evalutation: item.1, section: section)
					
					// remove it from the player list so it doesn't get scheduled again later
					squadsByPlayer[item.0, default:[]].removeAll() { return $0.1.squad.properties.name == squad }
				}
				
			case .placeManySquads(squad: let squad, section: let section, count: let count):
				var placed = 0
				while placed < count, let index = consideredSquads.firstIndex(where: { item in
					return item.1.squad.properties.name == squad
				}) {
					placed += 1
					
					// remove it from the big list so we don't use it again
					let item = consideredSquads.remove(at: index)
					
					result.add(player: item.0, evalutation: item.1, section: section)
					
					// remove it from the player list so it doesn't get scheduled again later
					squadsByPlayer[item.0, default:[]].removeAll() { return $0.1.squad.properties.name == squad }
				}

				
			case .removeAll(squad: let squad):
				consideredSquads.removeAll() { item in
					return item.1.squad.properties.name == squad
				}
				
				for player in squadsByPlayer.keys {
					squadsByPlayer[player, default: []].removeAll () { return $0.1.squad.properties.name == squad }
				}
			}
		}
	}
	
	func schedule(data: [Player : [SquadEvaluation]], config: TWConfig) -> [(Player, SquadEvaluation, String)] {
		let potentialSections = config.placementSections
		
		var result = Result(config: config)
		var squadsByPlayer = buildSquadsByPlayer(data: data, config:config)
		var leftoverSquadsByPlayer = [Player:[(Player, SquadEvaluation)]]()
		
		// how many squads do we have total?
		let totalAvailableSquadCount = squadsByPlayer.values.reduce(0) { $0 + $1.count }
		
		runPlacementInstructions(squadsByPlayer: &squadsByPlayer, into: &result, config:config)
		
		// now build the squad list to contain only the squads we intend to place -- honor the player limits
		var consideredSquads = [(Player, SquadEvaluation)]()
		for player in squadsByPlayer.keys.sorted() {
			let alreadyPlaced = result.placedSquadCountByPlayer[player, default: 0]
			let allowed = result.maximumSquadsFor(player: player)
			let playerSquads = squadsByPlayer[player, default: []].prefix(allowed.named - alreadyPlaced)
			leftoverSquadsByPlayer[player] = Array(squadsByPlayer[player, default: []].dropFirst(allowed.named - alreadyPlaced))
			consideredSquads.append(contentsOf: playerSquads)
		}
		
		// again, do a global sort to get the top squads we want to schedule in order.  this time we only have the top N squads from each player
		consideredSquads.sort(by: compareSquads)
		
		// count all the ones we will place + the ones we already did place
		let scheduledSquadCount = consideredSquads.count + result.placedSquadCountBySection.values.reduce(0, +)
		
		// then schedule the rest randomly
		for (player, evaluation) in consideredSquads {
			let gp = evaluation.squad.gp
			if gp > config.minimumGP {
				if let section = randomSection(squadName:evaluation.squad.properties.name ?? "", gp: gp, counts: result.placedSquadCountBySection, potentialSections: potentialSections, config:config) {
					result.add(player: player, evalutation: evaluation, section: section)
				}
			}
		}
		
		print("Schedulable squads: \(scheduledSquadCount), placed: \(result.placedSquadCountBySection.values.reduce(0, +)), available: \(totalAvailableSquadCount)\n")
		
		// finally, fill in the remaining spots with fluff squads
		let playersSortedByRemainingCount = data.keys.filter { player in !result.isPlayerDone(player) }.sorted { p1, p2 in
			let pc1 = result.maximumSquadsFor(player: p1).total - result.placedSquadCountByPlayer[p1, default: 0]
			let pc2 = result.maximumSquadsFor(player: p2).total - result.placedSquadCountByPlayer[p2, default: 0]
			if pc1 == pc2 {
				return p1 > p2
			} else {
				return pc1 > pc2
			}
		}
		
		while !result.allSectionsFull() && !result.allPlayersDone() {
			for player in playersSortedByRemainingCount {
				if result.allSectionsFull() {
					break
				}
				if !result.isPlayerDone(player) {
					// find an section that is not full
					while let section = potentialSections.randomElement(using: &RandomScheduler3.r) {
						if result.placedSquadCountBySection[section, default: 0] < config.fullSectionCount {
							// place a fake squad in it
							let threshold = min(config.sectionThreshold[section, default: 50000], 70000)
							
							// see if we can find a suggested squad for this
							var units = [Unit]()
							if let suggestedSquads = leftoverSquadsByPlayer[player], !suggestedSquads.isEmpty {
								if let suggestedIndex = suggestedSquads.firstIndex(where: { $0.1.squad.gp >= threshold - 5000 && $0.1.squad.gp <= threshold + 10000 }) {
									units = suggestedSquads[suggestedIndex].1.squad.units
									leftoverSquadsByPlayer[player]?.remove(at: suggestedIndex)
								}
							}
							
							let squad = Squad(Squad.Properties(name: "Any Squad \(threshold)+\(units.count > 0 ? ", e.g. ":"")"), units: units)
							let evaluation = SquadEvaluation(squad: squad, evaluations: [:], squadEvaluations: EvaluationSummary())
							result.add(player: player, evalutation: evaluation, section: section)
							break
						}
					}
				}
			}
		}
		
		return result.placements
	}
	
}

