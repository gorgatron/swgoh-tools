//
//  TWConfig.swift
//  swgoh
//
//  Created by David Koski on 5/9/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

struct TWMaximumSquads {
	let named: Int
	let fluff: Int
	var total: Int {
		return named + fluff
	}
}

protocol TWConfig {
	/// for random placement, these are the teams we consider
	var randomPlacementDefenseTeams: Set<String> { get }
	
	var backSections: [String] { get }
	var fleetSections: [String] { get }
	var anyPlacedFleet: String { get }
	
	var minimumGP: Int { get }
	var placementSections: [String] { get }
	var preferredSections: [String:[String]] { get }
	var sectionThreshold: [String:Int] { get }
	var sectionMaxThreshold: [String:Int] { get }
	
	var fullSectionCount: Int { get }
	var sectionMaxCount: [String:Int] { get }
	
	var squadDefenseFactor: [String:Double] { get }
	
	var randomPlacementInstructions: [PlacementInstruction] { get }
	
	var maximumSquadsThresholds: [(Int, TWMaximumSquads)] { get }
}

protocol TWForwardingConfig : TWConfig {
	
	var base: TWConfig { get }
	
}

extension TWForwardingConfig {
	
	var randomPlacementDefenseTeams: Set<String> {
		return base.randomPlacementDefenseTeams
	}

	var backSections: [String] {
		return base.backSections
	}
	
	var fleetSections: [String] {
		return base.fleetSections
	}
	
	var anyPlacedFleet: String {
		return base.anyPlacedFleet
	}
	
	var placementSections: [String] {
		return base.placementSections
	}
	
	var preferredSections: [String:[String]] {
		return base.preferredSections
	}
	
	var minimumGP: Int {
		return base.minimumGP
	}
	
	var sectionThreshold: [String:Int] {
		return base.sectionThreshold
	}
	
	var sectionMaxThreshold: [String:Int] {
		return base.sectionMaxThreshold
	}
	
	var fullSectionCount: Int {
		return base.fullSectionCount
	}
	
	var sectionMaxCount: [String:Int] {
		return base.sectionMaxCount
	}
	
	var squadDefenseFactor: [String:Double] {
		return base.squadDefenseFactor
	}
	
	var randomPlacementInstructions: [PlacementInstruction] {
		return base.randomPlacementInstructions
	}
	
	var maximumSquadsThresholds: [(Int, TWMaximumSquads)] {
		return base.maximumSquadsThresholds
	}

}

struct BaseTWConfig : TWConfig {
	
	let backSections = [ "T3", "T4", "B3", "B4" ]
	let fleetSections = [ "F1", "F2" ]
	
	let anyPlacedFleet = "Fleet: 300k+ defense fleet"
	
	let placementSections = [ "T1", "T2", "T3", "T4", "B1", "B2", "B3", "B4", "T1", "B1", "T1", "B1" ]
	
	/// if a squad is preferred to be placed, e.g. in the front or back
	let preferredSections = [
		"Phoenix" : [ "T3", "T4", "B3", "B4" ],
		"Wiggs-D" : [ "T3", "T4", "B3", "B4" ],
	]
	
	let minimumGP = 60000
	
	let sectionThreshold = [
		"T1" : 90000,
		"B1" : 90000,
		"T2" : 80000,
		"B2" : 80000,
		"T3" : 75000,
		"B3" : 70000,
		"T4" : 60000,
		"B4" : 60000,
	]
	
	let sectionMaxThreshold = [
		"T2" : 100000,
		"B2" : 100000,
		"T3" : 84000,
		"B3" : 84000,
		"T4" : 75000,
		"B4" : 75000,
	]
	
	let fullSectionCount = 24
	let sectionMaxCount = [
		"T1" : 24,
		"T2" : 24,
		"T3" : 6,
		"T4" : 4,
		"B1" : 24,
		"B2" : 24,
		"B3" : 4,
		"B4" : 4,
	]
	
	/// multiplier for how good squads are at defense (just sort order)
	let squadDefenseFactor = [
		"BH" : 2.2,
		"FO" : 1.6,
		"GK" : 1.6,
		"BS" : 1.6,
		"CLS" : 1.6,
		
		// special teams
		"All Hans" : 2.0,
	]
	
	let maximumSquadsThresholds: [(Int, TWMaximumSquads)] = [
		( 1_500_000, TWMaximumSquads(named: 1, fluff: 1) ),
		( 2_200_000, TWMaximumSquads(named: 2, fluff: 1) ),
		( 3_000_000, TWMaximumSquads(named: 3, fluff: 1) ),
		( 4_000_000, TWMaximumSquads(named: 4, fluff: 1) ),
		( 5_000_000, TWMaximumSquads(named: 8, fluff: 1) ),
		( 10_000_000, TWMaximumSquads(named: 8, fluff: 2) ),
	]
	
	let randomPlacementDefenseTeams = Set<String>()
	
	let randomPlacementInstructions = [PlacementInstruction]()

}

// strong in the front
struct StandardConfig : TWForwardingConfig {
	
	let base: TWConfig = BaseTWConfig()
	
	let randomPlacementDefenseTeams: Set = [
		"BH", "FO", "GK",
		"CLS",
		"BS",
		"zEP",
		"Wiggs-D",
		"NS", "NestSisters",
		"Phoenix",
		"Ewoks",
		"padme",
		"carth",
		"Qira",

		// Revan is on defense but gets removed via the special instructions
		"Revan",
		
		// these are kept for offense
//		 "DR", "Traya",
		
		// fleets
		"OGMF",
	]
	
	let randomPlacementInstructions: [PlacementInstruction] = [
		
		.placeSquad(squad: "NestSisters", section: "T4"),
		.placeSquad(squad: "NestSisters", section: "B4"),
		.placeSquad(squad: "NS", section: "T4"),
		.placeSquad(squad: "NS", section: "B4"),
		.placeSquad(squad: "CLS", section: "B4"),
		.placeSquad(squad: "Revan", section: "B4"),
		.placeSquad(squad: "Revan", section: "T4"),

		.placeSquad(squad: "CLS", section: "T3"),
		.placeSquad(squad: "CLS", section: "B3"),
		.placeSquad(squad: "BH", section: "T3"),
		.placeSquad(squad: "BH", section: "B3"),
		.placeSquad(squad: "zEP", section: "T3"),
		.placeSquad(squad: "zEP", section: "B3"),
		
		// some stronger fleets
		.placeSquad(squad: "OGMF", section: "F1"),
		.placeSquad(squad: "OGMF", section: "F2"),
		.placeSquad(squad: "OGMF", section: "F1"),
		.placeSquad(squad: "OGMF", section: "F2"),
		.removeAll(squad: "OGMF"),

		// don't place any more Revan squads on defense
		.removeAll(squad: "Revan"),
	]
}

// strong T3, fleets
struct OptimizedConfig : TWForwardingConfig {
	
	let base: TWConfig = BaseTWConfig()
	
	let anyPlacedFleet = "Fleet: **BEST** defensive fleet, fill F1 first"
	
	// extra T3 and T4 because these are the areas of concentration.  extra B1 because we want it to look tougher
	let placementSections = [ "T1", "T2", "T3", "T4", "B1", "B2", "B3", "B4", "T3", "T4", "T3", "T4", "B1" ]

	// these allow over the placement quota
	let backSections = [ "B1", "B2", "B3", "B4" ]

	/// if a squad is preferred to be placed, e.g. in the front or back
	let preferredSections = [
		"Phoenix" : [ "T1", "T2", "B1", "B2" ],
		"Wiggs-D" : [ "T1", "T2", "B1", "B2" ],
		"BH" : [ "T3" ],
		"zEP" : [ "T4" ],
	]
	
	let minimumGP = 60000
	
	let sectionThreshold = [
		"T1" : 85000,
		"B1" : 87000,
		"T2" : 80000,
		"B2" : 75000,
		"T3" : 95000,
		"B3" : 70000,
		"T4" : 95000,
		"B4" : 60000,
	]
	
	let sectionMaxThreshold = [
		// we don't want *the best* in T1
		"T1" : 100500,
		"B1" : 100500,

		"T2" : 95000,
		"B2" : 95000,
		"B3" : 84000,
		"B4" : 75000,
	]
	
	let fullSectionCount = 25
	let sectionMaxCount = [
		"T1" : 16,
		"T2" : 12,
		"T3" : 23,
		"T4" : 23,
		"B1" : 16,
		"B2" : 8,
		"B3" : 3,
		"B4" : 3,
	]

	let randomPlacementDefenseTeams: Set = [
		"BH", "FO", "GK",
		"CLS",
		"BS",
		"zEP",
		"Wiggs-D",
		"NS", "NestSisters",
		"Phoenix",
		"Ewoks",
		"padme",
		"carth",
		"Qira",
		"Geonosians",
		"Rogue1",
		"Carth",

		// Revan is on defense but gets removed via the special instructions
		"Revan",
		
		// these are kept for offense
		 "DR", "Traya",
		
		// fleets
		"OGMF",
	]
	
	let randomPlacementInstructions: [PlacementInstruction] = [
		
		.placeSquad(squad: "NestSisters", section: "T4"),
		.placeSquad(squad: "NestSisters", section: "B4"),
		.placeSquad(squad: "NS", section: "T4"),
		.placeSquad(squad: "NS", section: "B4"),
		.placeSquad(squad: "Revan", section: "B4"),
		.placeManySquads(squad: "Revan", section: "T4", count: 3),
		.placeSquad(squad: "DR", section: "T4"),
		.placeSquad(squad: "padme", section: "T4"),

		.placeSquad(squad: "CLS", section: "T4"),
		.placeSquad(squad: "CLS", section: "B4"),
		.placeSquad(squad: "CLS", section: "T3"),
		
		.placeManySquads(squad: "BH", section: "T4", count: 6),
		.placeManySquads(squad: "zEP", section: "T4", count: 10),
		
		// some stronger fleets
		.placeManySquads(squad: "OGMF", section: "F2", count: 4),
		.placeSquad(squad: "OGMF", section: "F1"),
		.placeSquad(squad: "OGMF", section: "F1"),
		.placeManySquads(squad: "OGMF", section: "F2", count: 17),

		// don't place any more Revan squads on defense
		.removeAll(squad: "Revan"),
		.removeAll(squad: "DR"),
		.removeAll(squad: "Traya"),
	]
}


struct TW3v3Config : TWForwardingConfig {
	
	let base: TWConfig = BaseTWConfig()
	
	let randomPlacementDefenseTeams: Set = [
		"BH", "BS", "CLS", "Carth", "Empire", "FO", "GK", "Lando", "NS", "QGJ", "Qira", "Wiggs-D", "Rebels", "Resistance", "Revan", "Sith", "Wiggs", "Ewoks"
		]
	
	let randomPlacementInstructions: [PlacementInstruction] = [
		
	]
	
	let minimumGP = 20000
	
	let sectionThreshold = [
		"T1" : 50000,
		"B1" : 50000,
		"T2" : 40000,
		"B2" : 40000,
		"T3" : 35000,
		"B3" : 35000,
		"T4" : 30000,
		"B4" : 30000,
	]
	
	let sectionMaxThreshold = [
		"T2" : 60000,
		"B2" : 60000,
		"T3" : 50000,
		"B3" : 50000,
		"T4" : 40000,
		"B4" : 40000,
	]
	
	let fullSectionCount = 35
	let sectionMaxCount = [
		"T1" : 35,
		"T2" : 35,
		"T3" : 10,
		"T4" : 8,
		"B1" : 35,
		"B2" : 35,
		"B3" : 8,
		"B4" : 8,
	]
	
	/// multiplier for how good squads are at defense (just sort order)
	let squadDefenseFactor = [
		"BH" : 2.2,
		"FO" : 1.6,
		"GK" : 1.6,
		"BS" : 1.6,
		"CLS" : 1.6,
	]
	
	let maximumSquadsThresholds: [(Int, TWMaximumSquads)] = [
		( 1_500_000, TWMaximumSquads(named: 2, fluff: 1) ),
		( 2_200_000, TWMaximumSquads(named: 4, fluff: 1) ),
		( 3_000_000, TWMaximumSquads(named: 6, fluff: 1) ),
		( 4_000_000, TWMaximumSquads(named: 8, fluff: 2) ),
		( 10_000_000, TWMaximumSquads(named: 10, fluff: 2) ),
	]

}

struct TurtleConfig : TWForwardingConfig {
	
	let base: TWConfig = BaseTWConfig()
	
	let randomPlacementDefenseTeams: Set = [
		"BH", "FO", "GK",
		"CLS",
		"BS",
		"zEP",
		"Wiggs-D",
		"NS", "NestSisters",
		"Phoenix",
		"Ewoks",
		
		// Revan is on defense but gets removed via the special instructions
		"Revan",
		
		// these are kept for offense
		"DR", "Traya",
	]
	
	let sectionThreshold = [
		"T1" : 90000,
		"B1" : 90000,
		"T2" : 80000,
		"B2" : 80000,
		"T3" : 75000,
		"B3" : 75000,
		"T4" : 60000,
		"B4" : 60000,
	]
	
	let sectionMaxThreshold = [
		"T2" : 110000,
		"B2" : 110000,
		"T3" : 90000,
		"B3" : 90000,
		"T4" : 80000,
		"B4" : 80000,
	]
	
	let fullSectionCount = 25
	let sectionMaxCount = [
		"T1" : 25,
		"T2" : 25,
		"T3" : 25,
		"T4" : 25,
		"B1" : 25,
		"B2" : 25,
		"B3" : 25,
		"B4" : 25,
	]
	
	let randomPlacementInstructions: [PlacementInstruction] = [
		
		.placeSquad(squad: "NestSisters", section: "T4"),
		.placeSquad(squad: "NestSisters", section: "B4"),
		.placeSquad(squad: "CLS", section: "B4"),
		.placeSquad(squad: "Revan", section: "B4"),
		.placeSquad(squad: "Revan", section: "T4"),
	]
	
	let maximumSquadsThresholds: [(Int, TWMaximumSquads)] = [
		( 10_000_000, TWMaximumSquads(named: 20, fluff: 2) ),
	]
}


enum PlacementInstruction {
	case placeSquad(squad: String, section: String)
	case placeManySquads(squad: String, section: String, count: Int)
	case removeAll(squad: String)
}

/// a filter that can remove collections of teams based on rules, e.g. we want high power NS on offense so remove all those teams from consideration.
/// We do it this way for NS because we don't want them to tie up Nest if they are not being used on defense.  Compare to how we do Revan:  his
/// teams are generated but not scheduled because we want to tie up his buddies.
struct TWSquadFilter : Filter {
	
	func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
		let containsHighPowerNS = squadEvaluations.contains { evaluation in
			if evaluation.squad.properties.name == "NS" {
				return evaluation.squad.gp >  101000
			}
			if evaluation.squad.properties.name == "NestSisters" {
				return evaluation.squad.gp >  104000
			}
			return false
		}
		if containsHighPowerNS {
			return squadEvaluations.filter { evaluation in
				if evaluation.squad.properties.name == "NS" || evaluation.squad.properties.name == "NestSisters" {
					return false
				}
				return true
			}
		} else {
			return squadEvaluations
		}
	}
	
}

/// similiar to TWSquadFilter, but produce NS if they have them
struct TWPlayerSquadFilter : Filter {
	
	func filter(_ squadEvaluations: [SquadEvaluation]) -> [SquadEvaluation] {
		let containsHighPowerNS = squadEvaluations.contains { evaluation in
			if evaluation.squad.properties.name == "NS" {
				return evaluation.squad.gp >  101000
			}
			return false
		}
		if containsHighPowerNS {
			return squadEvaluations.filter { evaluation in
				if evaluation.squad.properties.name == "NestSisters" {
					return false
				}
				return true
			}
		} else {
			return squadEvaluations
		}
	}
	
}

