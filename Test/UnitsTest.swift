//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation
import XCTest

class UnitsTest: XCTestCase {
    
    func testSingleSelect() {
        let singleSelect = u("REY")
        let result = singleSelect.select(units: TestResources.unitProvider)
        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(result[0].id, "REY")
    }
    
    func testSquadBuilderSimple() {
        let squadBuilder = SelectUnitSquadBuilder(unitSelectors: SingleUnitSelect.build("REYJEDITRAINING", "REY", "BB8", "R2D2_LEGENDARY", "RESISTANCETROOPER"))
        let result = squadBuilder.generateSquads(units: TestResources.unitProvider)
        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(result[0].units.count, 5)
        XCTAssertEqual(result[0].units[0].id, "REYJEDITRAINING")
    }

    func testSquadBuilderComplex() {
        // there should be 1 * 2 * 2 = 4 results
        let squadBuilder = SelectUnitSquadBuilder(u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("DEATHTROOPER"), u("PAO", "EMPERORPALPATINE"), u("CHIRRUTIMWE", "CT7567"))
        
        let result = squadBuilder.generateSquads(units: TestResources.unitProvider)
        XCTAssertEqual(result.count, 4)
    }

}
