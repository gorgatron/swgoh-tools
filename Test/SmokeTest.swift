//
//  Copyright © 2019 David Koski. All rights reserved.
//

import XCTest

class SmokeTest: XCTestCase {

    func testGuildFile() {
        let guildFile = TestResources.testGuildFile
        XCTAssertNotNil(guildFile)        
        XCTAssertFalse(guildFile.players.isEmpty)
    }

}
