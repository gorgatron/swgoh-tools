//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

class TestResources {
    
    static let testGuildFile: GuildFile = {
        do {
            let guildURL = Bundle(for: TestResources.self)
                .url(forResource: "gorgatron", withExtension: "json")!
            let jsonData = try Data(contentsOf: guildURL)
            let jsonDecoder = JSONDecoder()
            
            let guildFile = try jsonDecoder.decode(GuildFile.self, from: jsonData)
            return guildFile
        } catch {
            fatalError("loading testGuildFile: \(error)")
        }
    }()
    
    static let player: Player = {
        return testGuildFile.players.first!
    }()
    
    static let unitProvider: UnitProvider = {
        return testGuildFile.unitProviderFor(player: player)
    }()
    
    static let jtr: Unit = {
       return player.units["REYJEDITRAINING"]!
    }()
    
    static let pao: Unit = {
        return player.units["PAO"]!
    }()

    static let shoreTrooper: Unit = {
        return player.units["SHORETROOPER"]!
    }()

}

