//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation
import XCTest

class EvaluateTests: XCTestCase {
    
    func testStatRuleLessThanAtLeast() {
        let unit = TestResources.pao
        
        // actual is 2515
        let rule = StatRule(.physicalDamage, atLeast: 2600, target: 3000)
        let message = rule.evaluate(unit: unit)[0]
        
        XCTAssertEqual(message.status, .needsGear)
        XCTAssertEqual(message.score, 0)
        XCTAssertEqual(message.max, 100)
    }

    func testStatRuleAtLeast() {
        let unit = TestResources.pao
        
        // actual is 2515
        let rule = StatRule(.physicalDamage, atLeast: 2500, target: 2600)
        let message = rule.evaluate(unit: unit)[0]
        
        XCTAssertEqual(message.status, .needsTuning)
        
        // we want > 2500 and the target is 2600.  We have 15 over so we score 15/100 of the points
        XCTAssertEqual(message.score, 15)
        XCTAssertEqual(message.max, 100)
    }

    func testStatRuleTarget() {
        let unit = TestResources.pao
        
        // actual is 2515
        let rule = StatRule(.physicalDamage, atLeast: 2400, target: 2500)
        let message = rule.evaluate(unit: unit)[0]
        
        XCTAssertEqual(message.status, .ok)
        XCTAssertEqual(message.score, 100)
        XCTAssertEqual(message.max, 100)
    }

    func testUnitRule() {
        // the unit rule reads unit properties rather than Unit.Stats properties, but otherwise has the same implementation
        let unit = TestResources.shoreTrooper
        
        let rule = UnitRule(.gearLevel, atLeast: 10, target: 12)
        let message = rule.evaluate(unit: unit)[0]
        
        XCTAssertEqual(message.status, .needsFarming)
        XCTAssertEqual(message.score, 0)
        XCTAssertEqual(message.max, 100)
    }

    func testAllOmegasRule() {
        let unit = TestResources.shoreTrooper
        
        let rule = AllOmegasRule()
        let messages = rule.evaluate(unit: unit)
        
        XCTAssertEqual(messages.count, 1)
        
        XCTAssertEqual(messages[0].status, .needsGear)
        XCTAssertEqual(messages[0].score, 100)
        XCTAssertEqual(messages[0].max, 300)
    }
    
    func testUnitEvaluation() {
        var evaluation = EvaluationSummary()
        
        evaluation.add(message: Message(message: "test", status: .needsTuning, score: 50, max: 100))
        
        XCTAssertEqual(evaluation.status, .needsTuning)
        XCTAssertEqual(evaluation.score, 50)
        XCTAssertEqual(evaluation.preference, 0)

        evaluation.add(message: Message(message: "test", status: .ok, score: 200, max: 200))

        XCTAssertEqual(evaluation.status, .needsTuning)
        XCTAssertEqual(evaluation.score, 250)
        XCTAssertEqual(evaluation.preference, 0)

        evaluation.add(message: Message(message: "test", status: .ok, score: 100, max: 100, preference: 1))
        
        XCTAssertEqual(evaluation.status, .needsTuning)
        XCTAssertEqual(evaluation.score, 350)
        XCTAssertEqual(evaluation.preference, 1)
        
        evaluation.add(message: Message(message: "test", status: .needsFarming, score: 0, max: 100))
        
        XCTAssertEqual(evaluation.status, .needsFarming)
        XCTAssertEqual(evaluation.score, 350)
        XCTAssertEqual(evaluation.preference, 1)
    }
    
    func testSquadEvaluation() {
        let squad = Squad(units: [ TestResources.jtr, TestResources.pao ])
        
        var unitEvaluation1 = EvaluationSummary()
        var unitEvaluation2 = EvaluationSummary()

        unitEvaluation1.add(message: Message(message: "test", status: .ok, score: 100, max: 100, preference: 1))
        unitEvaluation2.add(message: Message(message: "test", status: .needsFarming, score: 10, max: 100))
        
        let squadEvaluation = SquadEvaluation(squad: squad, evaluations: [ TestResources.pao.id : unitEvaluation1, TestResources.jtr.id : unitEvaluation2 ], squadEvaluations: EvaluationSummary())
        
        XCTAssertEqual(squadEvaluation.status, .needsFarming)
        XCTAssertEqual(squadEvaluation.status.isViable, false)
        XCTAssertEqual(squadEvaluation.score, 110)
        XCTAssertEqual(squadEvaluation.preference, 1)
        
        // 100 (OK) + 25 (farming)
        XCTAssertEqual(squadEvaluation.viabilityScore, 125)
    }
    
    func testUnitEvaluator() {
        let unit = TestResources.shoreTrooper
        let evaluator = UnitEvaluator(
                // this first one fails
                UnitRule.sevenStarRule,
                UnitRule.gear10PlusRule,
                AllOmegasRule()
        )
        
        let result = evaluator.evaluate(unit: unit)
        
        XCTAssertEqual(result.status, .needsFarming)
        
        // we score 100 out of 900 points
        XCTAssertEqual(result.score, 100)
        XCTAssertEqual(result.max, 900)
    }

}
