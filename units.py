import json
import locale
import sys
import codecs

with codecs.open("characters.json", "r", encoding="utf-8") as read_file:
	characters = json.load(read_file)

print("let characters: [String:Character] = [")

for character in characters:
	categoriesString = ", ".join(map(lambda s: "\"" + s + "\"",character["categories"]))
	abilityClassesString = ", ".join(map(lambda s: "\"" + s + "\"",character["ability_classes"]))
	nameString = character["name"].replace("\"", "\\\"")
	shipString = ", ship: true"
	print(u"\t\"{base_id}\": Character(\"{base_id}\", \"{nameString}\", role:\"{role}\", abilityClasses:[{abilityClassesString}], categories:[{categoriesString}]{shipString}),".format(nameString = nameString, categoriesString = categoriesString, abilityClassesString = abilityClassesString, shipString = shipString, **character))

print("]")

# print(json.dumps(characters, indent=4))

